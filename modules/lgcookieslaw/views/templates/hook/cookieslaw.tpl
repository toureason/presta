{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<style>
	{literal}
	.lgcookieslaw_banner {
		display: table;
		width:100%;
		position:fixed;
		left:0;
		repeat-x scroll left top;
		background: {/literal}{$bgcolor|escape:'html':'UTF-8'}{literal};
		border-color: {/literal}{$bgcolor|escape:'html':'UTF-8'}{literal};
		border-left: 1px solid {/literal}{$bgcolor|escape:'html':'UTF-8'}{literal};
		border-radius: 3px 3px 3px 3px;
		border-right: 1px solid {/literal}{$bgcolor|escape:'html':'UTF-8'}{literal};
		color: {/literal}{$fontcolor|escape:'html':'UTF-8'}{literal} !important;
		z-index: 99999;
		border-style: solid;
		border-width: 1px;
		margin: 0;
		outline: medium none;
		text-align: center;
		vertical-align: middle;
		text-shadow: 0 0 0 0;
		-webkit-box-shadow: 0px 1px 5px 0px {/literal}{$shadowcolor|escape:'html':'UTF-8'}{literal};
		-moz-box-shadow:    0px 1px 5px 0px {/literal}{$shadowcolor|escape:'html':'UTF-8'}{literal};
		box-shadow:         0px 1px 5px 0px {/literal}{$shadowcolor|escape:'html':'UTF-8'}{literal};
	{/literal}
	{$position|escape:'html':'UTF-8'};
	{$opacity|escape:'html':'UTF-8'};
	{literal}
	}

	.lgcookieslaw_banner > form
	{
		position:relative;
	}

	.lgcookieslaw_banner > form input.lgcookieslaw_btn
	{
		border-color: {/literal}{$btn1_bgcolor|escape:'html':'UTF-8'}{literal} !important;
		background: {/literal}{$btn1_bgcolor|escape:'html':'UTF-8'}{literal} !important;
		color: {/literal}{$btn1_fontcolor|escape:'html':'UTF-8'}{literal};
		text-align: center;
		margin: 0px 0px 8px 0px;
        padding: 5px 7px;
        display: inline-block;
        border: 0;
        font-weight: bold;
	}

	.lgcookieslaw_banner > form input:hover.lgcookieslaw_btn
	{
		moz-opacity:0.85;
		opacity: 0.85;
		filter: alpha(opacity=85);
	}

	.lgcookieslaw_banner > form a.lgcookieslaw_btn
	{
		border-color: {/literal}{$btn2_bgcolor|escape:'html':'UTF-8'}{literal};
		background: {/literal}{$btn2_bgcolor|escape:'html':'UTF-8'}{literal};
		color: {/literal}{$btn2_fontcolor|escape:'html':'UTF-8'}{literal};
		margin: 0px 0px 8px 0px;
		text-align: center;
        padding: 5px 7px;
        display: inline-block;
        border: 0;
        font-weight: bold;
	}

	.lgcookieslaw_banner > form a:hover.lgcookieslaw_btn
	{
		moz-opacity:0.85;
		opacity: 0.85;
		filter: alpha(opacity=85);
	}

	.lgcookieslaw_close_banner_btn
	{
		cursor:pointer;
		width:21px;
		height:21px;
		max-width:21px;
	}

</style>
	<script type="text/javascript">
		function closeinfo()
		{
			$('.lgcookieslaw_banner').hide();
		}

	</script>
{/literal}

<div class="lgcookieslaw_banner">
	<form method="post" action="{$smarty.server.REQUEST_URI|escape:'html':'UTF-8'}" name="">
		{if $buttons_position == 2 }
			<input name="aceptocookies" class="button lgcookieslaw_btn" type="submit" href="{$cms_link|escape:'quotes':'UTF-8'}" value="{stripslashes($button1|escape:'quotes':'UTF-8')}" >
			<a class="button lgcookieslaw_btn" {if isset($cms_target) && $cms_target} target="_blank" {/if} href="{$cms_link|escape:'quotes':'UTF-8'}" >{stripslashes($button2|escape:'quotes':'UTF-8')}
			</a>
		{/if}
		<div class="" style="display:table; margin:0 auto;">
			{if $buttons_position == 4}
				<div style="display:table-cell; vertical-align: middle; padding:5px">
					<input name="aceptocookies" class="button lgcookieslaw_btn" type="submit" href="{$cms_link|escape:'quotes':'UTF-8'}" value="{stripslashes($button1|escape:'quotes':'UTF-8')}" >
					<a class="button lgcookieslaw_btn" {if isset($cms_target) && $cms_target} target="_blank" {/if} href="{$cms_link|escape:'quotes':'UTF-8'}" >{stripslashes($button2|escape:'quotes':'UTF-8')}
					</a>
				</div>
			{/if}
			<div style="display:table-cell; vertical-align: middle; padding:5px 25px 5px 5px;" >{stripslashes($content|escape:'quotes':'UTF-8')}</div>
			{if $buttons_position == 5}
				<div style="display:table-cell; vertical-align: middle; padding:5px">
					<input name="aceptocookies" class="button lgcookieslaw_btn" type="submit" href="{$cms_link|escape:'quotes':'UTF-8'}" value="{stripslashes($button1|escape:'quotes':'UTF-8')}" >
					<a class="button lgcookieslaw_btn" {if isset($cms_target) && $cms_target} target="_blank" {/if} href="{$cms_link|escape:'quotes':'UTF-8'}" >{stripslashes($button2|escape:'quotes':'UTF-8')}
					</a>
				</div>
			{/if}

		</div>
		{if $buttons_position == 3 }
			<input name="aceptocookies" class="button lgcookieslaw_btn" type="submit" href="{$cms_link|escape:'quotes':'UTF-8'}" value="{stripslashes($button1|escape:'quotes':'UTF-8')}" >
			<a class="button lgcookieslaw_btn" {if isset($cms_target) && $cms_target} target="_blank" {/if} href="{$cms_link|escape:'quotes':'UTF-8'}" >{stripslashes($button2|escape:'quotes':'UTF-8')}
			</a>
		{/if}
		{if $show_close}
			<div style="position:absolute;top:5px;right:5px;">
				<img src="{$path_module|escape:'html':'UTF-8'}/views/img/close.png" alt="close" class="lgcookieslaw_close_banner_btn" onclick="closeinfo();">
			</div>
		{/if}
	</form>

</div>
