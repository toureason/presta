<?php

if (!defined('_PS_VERSION_'))
	exit;

define('_MEGAMENU_MODULE_DIR_', _PS_MODULE_DIR_.'megamenu/');

define('_MEGAMENU_ADMIN_CONTROLLER_DIR', _MEGAMENU_MODULE_DIR_.'controllers/admin/');
define('_MEGAMENU_FRONT_CONTROLLER_DIR', _MEGAMENU_MODULE_DIR_.'controllers/front/');

define('_MEGAMENU_ADMIN_MODEL_DIR', _MEGAMENU_MODULE_DIR_.'model/');

define('_MEGAMENU_ADMIN_VIEWS_DIR', 'views/templates/admin/');
define('_MEGAMENU_HOOK_VIEWS_DIR', 'views/templates/hook/');

define('_MEGAMENU_CLASSES_DIR_', _MEGAMENU_MODULE_DIR_.'classes/');

class MegaMenu extends Module{
	public function __construct(){
		global $context;
		$this->name = 'megamenu';
		$this->tab = 'front_office_features';
		$this->version = '1.3.2';
		$this->author = 'Transvelo';
		$this->need_instance = 0;
		$this->langId = $context->language->id;
		$this->bootstrap = true;
		parent::__construct();
		$this->link = new Link();
		$this->allowedRowSize = 3;
        $this->oPath = $this->_path;
        $this->oSmarty = $this->smarty;
        $this->oContext = $this->context;
        $this->displayName = $this->l('Madhai - Responsive Mega Menu');
        $this->description = $this->l('A Top Horizontal menu which has complex menu items');
    }

    public function install(){
        if(parent::install() AND $this->installSQL() AND $this->registerHook('displayTop')){
            return true;
        }
        return false;
    }

    public function uninstall(){
        return ($this->uninstallSQL() && parent::uninstall());
    }

    public function installSQL(){
        $sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'megamenu_content` ( `id_megamenu_content` int(11) NOT NULL AUTO_INCREMENT, `id_megamenu_menu` int(11) NOT NULL, `type` varchar(10) NOT NULL, `row_count` smallint(6) NOT NULL, `json_data` text, `id_product` int(11) DEFAULT NULL, `custom_class` varchar(50) DEFAULT NULL, `grid` smallint(6) NOT NULL, `date_add` datetime NOT NULL, `date_upd` datetime NOT NULL, PRIMARY KEY (`id_megamenu_content`) ) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'megamenu_content_lang` ( `id_megamenu_content` int(11) NOT NULL, `id_lang` int(11) NOT NULL, `title` varchar(100) CHARACTER SET utf8 NOT NULL, `custom_html` text, PRIMARY KEY (`id_megamenu_content`,`id_lang`) ) DEFAULT CHARSET=utf8;CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'megamenu_link` ( `id_megamenu_link` int(11) NOT NULL AUTO_INCREMENT, `url` varchar(255) DEFAULT NULL, `date_add` datetime DEFAULT NULL, `date_upd` datetime DEFAULT NULL, PRIMARY KEY (`id_megamenu_link`) ) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'megamenu_link_lang` ( `id_megamenu_link` int(11) NOT NULL, `id_lang` int(11) NOT NULL, `label` varchar(255) DEFAULT NULL, PRIMARY KEY (`id_megamenu_link`,`id_lang`) ) DEFAULT CHARSET=utf8;CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'megamenu_menu` ( `id_megamenu_menu` int(11) NOT NULL AUTO_INCREMENT, `is_customlink` tinyint(1) NOT NULL DEFAULT "0", `custom_link` varchar(255) DEFAULT NULL, `icon_type` enum("icon","image") DEFAULT NULL, `icon` varchar(100) DEFAULT NULL, `is_new_tab` tinyint(1) NOT NULL DEFAULT "0", `is_megamenu` tinyint(1) DEFAULT "0", `link_object_id` varchar(10) DEFAULT NULL, `is_fullwidth` tinyint(1) NOT NULL DEFAULT "0", `position` int(11) NOT NULL, `status` tinyint(1) NOT NULL DEFAULT "0", `date_add` datetime NOT NULL, `date_upd` datetime NOT NULL, PRIMARY KEY (`id_megamenu_menu`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'megamenu_menu_lang` ( `id_megamenu_menu` int(11) NOT NULL, `id_lang` int(11) NOT NULL, `label` varchar(100) DEFAULT NULL ) DEFAULT CHARSET=utf8;';
        $configure = '{"is_bootstrap":"1","is_hover":"1","effect":"","menu_bg":"","menu_active_bg":"","menu_active_font":"","menu_font":"","drop_bg":"","drop_font":"","custom_css":""}';
        Configuration::updateValue('PS_MEGAMENU_SETTINGS', $configure);
        Configuration::updateValue('PS_MEGAMENU_UPDATE',1);
        return Db::getInstance()->execute($sql);
    }

    public function uninstallSQL(){
        $sql = 'DROP TABLE  `'._DB_PREFIX_.'megamenu_menu`;DROP TABLE  `'._DB_PREFIX_.'megamenu_menu_lang`;DROP TABLE  `'._DB_PREFIX_.'megamenu_content`;DROP TABLE  `'._DB_PREFIX_.'megamenu_content_lang`;DROP TABLE  `'._DB_PREFIX_.'megamenu_link`;DROP TABLE  `'._DB_PREFIX_.'megamenu_link_lang`;';;
        Configuration::deleteByName('PS_MEGAMENU_SETTINGS');
        return Db::getInstance()->execute($sql);
    }

    public function hookTop($params){
        $this->_setHookAssets();
        if (!$this->_prepareHook())
            return false;
        return $this->display(__FILE__, 'topmenu.tpl', $this->getCacheId());
    }

    public function hookDisplayMenu(){
        return $this->hookTop();
    }

    private function _setHookAssets(){
        $this->context->controller->addCSS($this->_path.'css/megamenu.css', 'all');
        $this->context->controller->addCSS($this->_path.'css/animate.css', 'all');
        $this->context->controller->addCSS($this->_path.'css/custom.css', 'all');
        $this->context->controller->addCSS($this->_path.'css/font-awesome.min.css', 'all');

        $settings = json_decode(Configuration::get('PS_MEGAMENU_SETTINGS'), 1 );
        
        if(isset($settings['is_bootstrap']) && !$settings['is_bootstrap']){
            $this->context->controller->addCSS($this->_path.'css/bootstrap.css', 'all');
            $this->context->controller->addJs($this->_path.'js/bootstrap.js');
        }

        $this->context->controller->addJs($this->_path.'js/bootstrap-hover-dropdown.min.js');
    }


    private function _prepareHook(){

        if (!$this->isCached('topmenu.tpl', $this->getCacheId())){
            global $smarty;
            $settings = json_decode(Configuration::get('PS_MEGAMENU_SETTINGS'), 1 );
           
            require_once(_MEGAMENU_ADMIN_MODEL_DIR.'MegamenuMenu.php');

            $menuObject = new MegamenuMenu();

            $menus = $menuObject->getTopMenuToDisplay();

            $smarty->assign(array(
                    'menus'     => $menus,
                    'settings'  => $settings,
                    'imagePath' => $this->_path.'icons',
                )
            );
        }
        return true;
    }

    public function getContent(){

        $this->checkUpdate();
        $this->context->controller->addCSS($this->_path.'css/admin.css', 'all');
        $this->context->controller->addCSS($this->_path.'css/animate.css', 'all');
        $this->context->controller->addCSS($this->_path.'css/font-awesome.min.css', 'all');
        $this->context->controller->addJs(_PS_JS_DIR_.'tiny_mce/tiny_mce.js');
        $this->context->controller->addJs(_PS_JS_DIR_.'tinymce.inc.js');
        $this->context->smarty->assign(array(
                'baseURL'		=> $this->getModuleLink(),
                'template' 		=> $this->getTemplateContent(),
                'languages' 	=> Language::getLanguages(false),
                'module_path' 	=> $this->_path,

            )
        ); 
        return $this->display(__FILE__, _MEGAMENU_ADMIN_VIEWS_DIR.'layouts/layout-default.tpl');
    }

    public function checkUpdate(){
        if(!Configuration::get('PS_MEGAMENU_UPDATE')){
            $sql = 'ALTER TABLE  `'._DB_PREFIX_.'megamenu_menu` ADD `icon_type` enum("icon","image") DEFAULT NULL AFTER  `status`,ADD `icon` varchar(100) DEFAULT NULL AFTER  `icon_type`,ADD `is_new_tab` tinyint(1) NOT NULL DEFAULT "0" AFTER  `icon`';
            if(Db::getInstance()->execute($sql)){
                Configuration::updateValue('PS_MEGAMENU_UPDATE',1);
            }
        }
    }


    public function getTemplateContent(){
        $controller = Tools::getValue('mm-controller');
        $controller = !empty($controller) ? $controller : 'Menus';
        require_once(_MEGAMENU_ADMIN_CONTROLLER_DIR.$controller.'Controller.php');
        $classname = $controller.'Controller';
        $contollerObject = new $classname($this);
        $filename = $contollerObject->getcontent();
        $this->clearCache();
        return $this->display(__FILE__, _MEGAMENU_ADMIN_VIEWS_DIR.$filename.'.tpl');
    }

    public function getFolderAdmin() {
        $folders = array('cache', 'classes', 'config', 'controllers', 'css', 'docs', 'download', 'img', 'js', 'localization', 'log', 'mails',
         'modules', 'override', 'themes', 'tools', 'translations', 'upload', 'webservice', '.', '..');
            $handle = opendir(_PS_ROOT_DIR_);
        if (!$handle) {
            return false;
        }
        while (false !== ($folder = readdir($handle))) {
            if (is_dir(_PS_ROOT_DIR_ . '/' . $folder)) {
                if (!in_array($folder, $folders)) {
                    $folderadmin = opendir(_PS_ROOT_DIR_ . '/' . $folder);
                    if (!$folderadmin)
                        return $folder;
                    while (false !== ($file = readdir($folderadmin))) {
                        if (is_file(_PS_ROOT_DIR_ . '/' . $folder . '/' . $file) && ($file == 'header.inc.php')) {
                            return $folder;
                        }
                    }
                }
            }
        }
        return $false;
    }


    public function clearCache()
    {
        $this->_clearCache('topmenu.tpl');
    }


    public function getModuleLink(){
        $linkAdminModules = $this->context->link->getAdminLink('AdminModules', true);
        return $linkAdminModules.'&configure='.urlencode($this->name).'&tab_module='.$this->tab.'&module_name='.urlencode($this->name);
    }

    public function getIcons(){
        return array(
           'fa fa-adjust',
           'fa fa-anchor',
           'fa fa-archive',
           'fa fa-area-chart',
           'fa fa-arrows',
           'fa fa-arrows-h',
           'fa fa-arrows-v',
           'fa fa-asterisk',
           'fa fa-at',
           'fa fa-automobile',
           'fa fa-ban',
           'fa fa-bank',
           'fa fa-bar-chart',
           'fa fa-bar-chart-o',
           'fa fa-barcode',
           'fa fa-bars',
           'fa fa-beer',
           'fa fa-bell',
           'fa fa-bell-o',
           'fa fa-bell-slash',
           'fa fa-bell-slash-o',
           'fa fa-bicycle',
           'fa fa-binoculars',
           'fa fa-birthday-cake',
           'fa fa-bolt',
           'fa fa-bomb',
           'fa fa-book',
           'fa fa-bookmark',
           'fa fa-bookmark-o',
           'fa fa-briefcase',
           'fa fa-bug',
           'fa fa-building',
           'fa fa-building-o',
           'fa fa-bullhorn',
           'fa fa-bullseye',
           'fa fa-bus',
           'fa fa-cab',
           'fa fa-calculator',
           'fa fa-calendar',
           'fa fa-calendar-o',
           'fa fa-camera',
           'fa fa-camera-retro',
           'fa fa-car',
           'fa fa-caret-square-o-down',
           'fa fa-caret-square-o-left',
           'fa fa-caret-square-o-right',
           'fa fa-caret-square-o-up',
           'fa fa-cc',
           'fa fa-certificate',
           'fa fa-check',
           'fa fa-check-circle',
           'fa fa-check-circle-o',
           'fa fa-check-square',
           'fa fa-check-square-o',
           'fa fa-child',
           'fa fa-circle',
           'fa fa-circle-o',
           'fa fa-circle-o-notch',
           'fa fa-circle-thin',
           'fa fa-clock-o',
           'fa fa-close',
           'fa fa-cloud',
           'fa fa-cloud-download',
           'fa fa-cloud-upload',
           'fa fa-code',
           'fa fa-code-fork',
           'fa fa-coffee',
           'fa fa-cog',
           'fa fa-cogs',
           'fa fa-comment',
           'fa fa-comment-o',
           'fa fa-comments',
           'fa fa-comments-o',
           'fa fa-compass',
           'fa fa-copyright',
           'fa fa-credit-card',
           'fa fa-crop',
           'fa fa-crosshairs',
           'fa fa-cube',
           'fa fa-cubes',
           'fa fa-cutlery',
           'fa fa-dashboard',
           'fa fa-database',
           'fa fa-desktop',
           'fa fa-dot-circle-o',
           'fa fa-download',
           'fa fa-edit',
           'fa fa-ellipsis-h',
           'fa fa-ellipsis-v',
           'fa fa-envelope',
           'fa fa-envelope-o',
           'fa fa-envelope-square',
           'fa fa-eraser',
           'fa fa-exchange',
           'fa fa-exclamation',
           'fa fa-exclamation-circle',
           'fa fa-exclamation-triangle',
           'fa fa-external-link',
           'fa fa-external-link-square',
           'fa fa-eye',
           'fa fa-eye-slash',
           'fa fa-eyedropper',
           'fa fa-fax',
           'fa fa-female',
           'fa fa-fighter-jet',
           'fa fa-file-archive-o',
           'fa fa-file-audio-o',
           'fa fa-file-code-o',
           'fa fa-file-excel-o',
           'fa fa-file-image-o',
           'fa fa-file-movie-o',
           'fa fa-file-pdf-o',
           'fa fa-file-photo-o',
           'fa fa-file-picture-o',
           'fa fa-file-powerpoint-o',
           'fa fa-file-sound-o',
           'fa fa-file-video-o',
           'fa fa-file-word-o',
           'fa fa-file-zip-o',
           'fa fa-film',
           'fa fa-filter',
           'fa fa-fire',
           'fa fa-fire-extinguisher',
           'fa fa-flag',
           'fa fa-flag-checkered',
           'fa fa-flag-o',
           'fa fa-flash',
           'fa fa-flask',
           'fa fa-folder',
           'fa fa-folder-o',
           'fa fa-folder-open',
           'fa fa-folder-open-o',
           'fa fa-frown-o',
           'fa fa-futbol-o',
           'fa fa-gamepad',
           'fa fa-gavel',
           'fa fa-gear',
           'fa fa-gears',
           'fa fa-gift',
           'fa fa-glass',
           'fa fa-globe',
           'fa fa-graduation-cap',
           'fa fa-group',
           'fa fa-hdd-o',
           'fa fa-headphones',
           'fa fa-heart',
           'fa fa-heart-o',
           'fa fa-history',
           'fa fa-home',
           'fa fa-image',
           'fa fa-inbox',
           'fa fa-info',
           'fa fa-info-circle',
           'fa fa-institution',
           'fa fa-key',
           'fa fa-keyboard-o',
           'fa fa-language',
           'fa fa-laptop',
           'fa fa-leaf',
           'fa fa-legal',
           'fa fa-lemon-o',
           'fa fa-level-down',
           'fa fa-level-up',
           'fa fa-life-bouy',
           'fa fa-life-buoy',
           'fa fa-life-ring',
           'fa fa-life-saver',
           'fa fa-lightbulb-o',
           'fa fa-line-chart',
           'fa fa-location-arrow',
           'fa fa-lock',
           'fa fa-magic',
           'fa fa-magnet',
           'fa fa-mail-forward',
           'fa fa-mail-reply',
           'fa fa-mail-reply-all',
           'fa fa-male',
           'fa fa-map-marker',
           'fa fa-meh-o',
           'fa fa-microphone',
           'fa fa-microphone-slash',
           'fa fa-minus',
           'fa fa-minus-circle',
           'fa fa-minus-square',
           'fa fa-minus-square-o',
           'fa fa-mobile',
           'fa fa-mobile-phone',
           'fa fa-money',
           'fa fa-moon-o',
           'fa fa-mortar-board',
           'fa fa-music',
           'fa fa-navicon',
           'fa fa-newspaper-o',
           'fa fa-paint-brush',
           'fa fa-paper-plane',
           'fa fa-paper-plane-o',
           'fa fa-paw',
           'fa fa-pencil',
           'fa fa-pencil-square',
           'fa fa-pencil-square-o',
           'fa fa-phone',
           'fa fa-phone-square',
           'fa fa-photo',
           'fa fa-picture-o',
           'fa fa-pie-chart',
           'fa fa-plane',
           'fa fa-plug',
           'fa fa-plus',
           'fa fa-plus-circle',
           'fa fa-plus-square',
           'fa fa-plus-square-o',
           'fa fa-power-off',
           'fa fa-print',
           'fa fa-puzzle-piece',
           'fa fa-qrcode',
           'fa fa-question',
           'fa fa-question-circle',
           'fa fa-quote-left',
           'fa fa-quote-right',
           'fa fa-random',
           'fa fa-recycle',
           'fa fa-refresh',
           'fa fa-remove',
           'fa fa-reorder',
           'fa fa-reply',
           'fa fa-reply-all',
           'fa fa-retweet',
           'fa fa-road',
           'fa fa-rocket',
           'fa fa-rss',
           'fa fa-rss-square',
           'fa fa-search',
           'fa fa-search-minus',
           'fa fa-search-plus',
           'fa fa-send',
           'fa fa-send-o',
           'fa fa-share',
           'fa fa-share-alt',
           'fa fa-share-alt-square',
           'fa fa-share-square',
           'fa fa-share-square-o',
           'fa fa-shield',
           'fa fa-shopping-cart',
           'fa fa-sign-in',
           'fa fa-sign-out',
           'fa fa-signal',
           'fa fa-sitemap',
           'fa fa-sliders',
           'fa fa-smile-o',
           'fa fa-soccer-ball-o',
           'fa fa-sort',
           'fa fa-sort-alpha-asc',
           'fa fa-sort-alpha-desc',
           'fa fa-sort-amount-asc',
           'fa fa-sort-amount-desc',
           'fa fa-sort-asc',
           'fa fa-sort-desc',
           'fa fa-sort-down',
           'fa fa-sort-numeric-asc',
           'fa fa-sort-numeric-desc',
           'fa fa-sort-up',
           'fa fa-space-shuttle',
           'fa fa-spinner',
           'fa fa-spoon',
           'fa fa-square',
           'fa fa-square-o',
           'fa fa-star',
           'fa fa-star-half',
           'fa fa-star-half-empty',
           'fa fa-star-half-full',
           'fa fa-star-half-o',
           'fa fa-star-o',
           'fa fa-suitcase',
           'fa fa-sun-o',
           'fa fa-support',
           'fa fa-tablet',
           'fa fa-tachometer',
           'fa fa-tag',
           'fa fa-tags',
           'fa fa-tasks',
           'fa fa-taxi',
           'fa fa-terminal',
           'fa fa-thumb-tack',
           'fa fa-thumbs-down',
           'fa fa-thumbs-o-down',
           'fa fa-thumbs-o-up',
           'fa fa-thumbs-up',
           'fa fa-ticket',
           'fa fa-times',
           'fa fa-times-circle',
           'fa fa-times-circle-o',
           'fa fa-tint',
           'fa fa-toggle-down',
           'fa fa-toggle-left',
           'fa fa-toggle-off',
           'fa fa-toggle-on',
           'fa fa-toggle-right',
           'fa fa-toggle-up',
           'fa fa-trash',
           'fa fa-trash-o',
           'fa fa-tree',
           'fa fa-trophy',
           'fa fa-truck',
           'fa fa-tty',
           'fa fa-umbrella',
           'fa fa-university',
           'fa fa-unlock',
           'fa fa-unlock-alt',
           'fa fa-unsorted',
           'fa fa-upload',
           'fa fa-user',
           'fa fa-users',
           'fa fa-video-camera',
           'fa fa-volume-down',
           'fa fa-volume-off',
           'fa fa-volume-up',
           'fa fa-warning',
           'fa fa-wheelchair',
           'fa fa-wifi',
           'fa fa-wrench',
           );
    }
}