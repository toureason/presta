<?php
require_once(_MEGAMENU_ADMIN_MODEL_DIR.'MegamenuContent.php');
require_once(_MEGAMENU_ADMIN_MODEL_DIR.'MegamenuLink.php');

class MegamenuMenu extends ObjectModel{

	public $id_megamenu_menu;

	public $is_customlink;

	public $custom_link;

	public $link_object_id;

	public $is_megamenu;

	public $position;

	public $status;

	public $is_fullwidth;

	public $date_add;

	public $date_upd;

	public $label;

	public $langId;

	public $icon_type;

	public $icon;

	public $is_new_tab;

	public function __construct($id_megamenu_menu = NULL,$id_lang = null){
		global $context;
		if($id_megamenu_menu){
			$this->id_megamenu_menu = $id_megamenu_menu;
		}
		$this->langId = $context->language->id;
		parent::__construct($id_megamenu_menu,$id_lang);
	}

	public static $definition = array(
		'table' => 'megamenu_menu',
		'primary' => 'id_megamenu_menu',
		'multilang' => true,
		'fields' => array(
			'is_customlink'		=> 	array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
			'is_new_tab'		=> 	array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
			'custom_link'		=>	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'icon_type'			=>	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'icon'				=>	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'link_object_id'	=>	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'is_megamenu'		=> 	array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
			'is_fullwidth'		=> 	array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
			'position'			=>	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'status' 			=>	array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'label'				=>	array('type' => self::TYPE_STRING,'lang'=>true, 'validate' => 'isString'),
			'date_add'       	=>  array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'date_upd'      	=>	array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
		),
	);

	public function findByMenuId($id_megamenu_menu){
		$sql = 'SELECT * FROM '._DB_PREFIX_.'megamenu_menu WHERE id_megamenu_menu = '.$id_megamenu_menu;
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
	}

	public function getMenuDetailsbyId($id_megamenu_menu){
		$definition = MegamenuMenu::$definition;
		$menuObject = new MegamenuMenu($id_megamenu_menu);
		$menu = array();
		foreach ($definition['fields'] as $key => $field) {
			$menu[$key] = $menuObject->$key;
		}
		$megamenuContentObject = new MegamenuContent();
		$menu['content'] = $megamenuContentObject->getMenuContentByMenuId($id_megamenu_menu);
		return $menu;
	}

	public function doAdd($data){
		foreach($data as $key => $value){
			$this->$key = $value;
		}
		return parent::add();
	}

	public function doUpdate($data){
		foreach($data as $key => $value){
			$this->$key = $value;
		}
		return parent::update();
	}

	public function doDelete(){
		return parent::delete();
	}

	public function getNextPosition(){
		$postion = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT MAX(position) FROM '._DB_PREFIX_.'megamenu_menu');
		return $postion+1;
	}

	public function getMenusByPosition($id_lang = false,$status = false){
		$sql = 'SELECT * FROM '._DB_PREFIX_.'megamenu_menu m';
		if($id_lang){
			$sql .= ' LEFT JOIN '._DB_PREFIX_.'megamenu_menu_lang ml ON(ml.id_megamenu_menu = m.id_megamenu_menu AND ml.id_lang='.$id_lang.')';
		}
		if($status){
			$sql .= ' WHERE status = 1';
		}
		$sql .= ' ORDER BY m.position ASC';
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	}

	
	public function getDefaultLinks($addspace = true){
		
		$categories = MegamenuMenu::getCategoriesList();
		if(isset($categories[0])){
			unset($categories[0]);
		}

		$manufacturer = new Manufacturer();
		$manufacturers = $manufacturer->getManufacturers();

		$supplier = new Supplier();
		$suppliers = $supplier->getSuppliers();

		$cms = new CMS();
		$cmsPages = $cms->getCMSPages($this->langId);

		$customLinks = new MegamenuLink();
		$customLinksDetails = $customLinks->getLinks($this->langId);

		$links = array();
		
		$links = array(
			'Categories' 	=> MegamenuMenu::buildLinkArray($categories,'id_category','name','CAT','level_depth'),
			'Manufacturers' => MegamenuMenu::buildLinkArray($manufacturers,'id_manufacturer','name','MAN'),
			'Suppliers' 	=> MegamenuMenu::buildLinkArray($suppliers,'id_supplier','name','SUP'),
			'CMS'			=> MegamenuMenu::buildLinkArray($cmsPages,'id_cms','meta_title','CMS'),
			'CustomLinks'	=> MegamenuMenu::buildLinkArray($customLinksDetails,'id_megamenu_link','label','CSL'),
		);
		return $links;
	}

	private function buildLinkArray($links,$idKey,$nameKey,$prefix,$spaceKey = ''){ 
		$bulidedValues = array();
		foreach($links as $link) {
			if(isset($link[$idKey]) && isset($link[$nameKey])){ 
				$preSpace = '';
				if($spaceKey && isset($link[$spaceKey])){
					$preSpace = str_repeat("&nbsp;&nbsp;", $link[$spaceKey]);
				}
				$bulidedValues[$prefix.$link[$idKey]] = $preSpace.$link[$nameKey];
			}
		}
		return $bulidedValues;
	}

	private function getCategoriesList(){
		return Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'category` AS c LEFT JOIN '._DB_PREFIX_.'category_lang AS cl ON ( cl.id_lang = '.$this->langId.' AND cl.id_category = c.id_category ) ORDER BY c.`nleft`;');
	}

	public function changePosition($id_megamenu_menu,$position){
		if($position){
			$menuObject = new MegamenuMenu($id_megamenu_menu);
			$existPosition = $menuObject->position;
			$alternamePosition = MegamenuMenu::getAlternatePosition($existPosition,$position);
			if($alternamePosition){
				$alternameObject = new MegamenuMenu($alternamePosition);
				$menuObject->position = $alternameObject->position;
				$alternameObject->position = $existPosition;
				if($menuObject->doUpdate() && $alternameObject->doUpdate()){
					return true;
				}
			}
		}
		return false;
	}

	public function getAlternatePosition($currentPosition,$position){
		$sql = '';
		if($currentPosition){
			if($position == 'up'){
			$sql = 'SELECT id_megamenu_menu FROM `'._DB_PREFIX_.'megamenu_menu` where position < '.$currentPosition.' ORDER BY `position` DESC';
			}elseif ($position == 'down') {
				$sql = 'SELECT id_megamenu_menu FROM `'._DB_PREFIX_.'megamenu_menu` where position > '.$currentPosition.' ORDER BY `position` ASC';
			}

			if($sql){
				return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);
				
			}
		}
		
		return false;
	}


	public function getTopMenuToDisplay(){
		$menus = $this->getMenusByPosition($this->langId,true);
		$menuContentObject = new MegamenuContent();
		//$men
		if(!empty($menus)){
			foreach ($menus as $key => $menu) {
				$menusCount = count($menuContentObject->getContentByid($menu['id_megamenu_menu']));
				$menus[$key]['content'] = $menuContentObject->getMenuContentByMenuId($menu['id_megamenu_menu'],$this->langId);
				$menus[$key]['is_dropdown'] = 0;
				if($menu['id_megamenu_menu'] == 2){
					//echo '<pre>'.print_r($menu,1); exit;
				}
				if($menusCount == 1 && isset($menus[$key]['content'][1][1]['type']) && $menus[$key]['content'][1][1]['type'] == 'LNK'){
					$menus[$key]['is_dropdown'] = 1;
				}
				
			}
		}
		return MegamenuMenu::buildMenuHtml($menus);
	}

	public function buildMenuHtml($menus){
		$html = '';
		$settings = json_decode(Configuration::get('PS_MEGAMENU_SETTINGS'),1);
		foreach ($menus as $menu) {
			$dropDownClass = '';
			$is_dropdown = false;
			$menuLinkAdds = '';
			$menuLink = $this->getMenuLink($menu);
			$menuMobileTrigger = '';
			if($menu['is_megamenu'] || !empty($menu['content'])){
				$is_dropdown = true;

				$menuLinkAdds = '';
				
				if($settings['is_hover']){
					$menuMobileTrigger = '<span class="megamenu-toggle"></span>';
					$menuLinkAdds .= 'data-hover="dropdown"';
				}else{
					$menuLinkAdds .= 'class="dropdown-toggle" data-toggle="dropdown"';
				}
				$dropDownClass = 'dropdown';
			}
			if($menu['is_new_tab']){
				$menuLinkAdds .= ' target="_blank"';
			}
			if($menu['is_fullwidth']){
				$dropDownClass .= ' yamm-fw';
			}
			$icon = '';
			if($menu['icon_type']){
				if($menu['icon_type'] == 'icon'){
					$icon = '<i class="'.$menu['icon'].'"></i>';
				}elseif ($menu['icon_type'] == 'image') {
					$mm = new Megamenu();
					$icon = '<img src="'.$mm->oPath.'/icons/'.$menu['id_megamenu_menu'].'.png">';
				}
			}
			$html .= '<li class="'.$dropDownClass.'">';

			$html .= $menuMobileTrigger.'<a href="'.$menuLink.'" '.$menuLinkAdds.'>'.$icon.' '.$menu['label'].'</a>';

			if($is_dropdown){

				if($menu['is_dropdown'] && isset($menu['content'][1][1]['type']) && $menu['content'][1][1]['type'] == 'LNK' && !empty($menu['content'][1][1]['LNK'])){
					$html .= '<ul class="dropdown-menu animated" role="menu">'.$this->buildLinkHtml($menu['content'][1][1]['LNK']).'</ul>';
				}else{
					$html .= $this->buildMenuContentHtml($menu);
				}
			}
			
			$html .= '</li>';
		}
		return $html;
	}

	public function buildMenuContentHtml($menu){
		$html = '';
		if(!empty($menu['content'])){
			$html .= '<ul class="dropdown-menu animated" role="menu">';
			$html .= '<li>';
			foreach ($menu['content'] as $rowCount => $row) {
				if(!empty($row)){
					$html .='<div class="yamm-content"><div class="row">';

						foreach($row as $columnCount => $column){
							$html .= '<div class="col-md-'.$column['grid'].'">';
							$html .= $this->buildMenuColumnHtml($menu,$column);
							$html .= '</div>';
						}

					$html .='</div></div>';					
				}

			}
			$html .= '</li>';
			$html .= '</ul>';
		}
		return $html;
	}

	public function buildMenuColumnHtml($menu,$column){
		$html = '';
		$type = $column['type'];
		if($type){
			$html .= '<div class="section '.$column['custom_class'].'">';
			if($column['title']){
				$html .='<h3 class="title">'.$column['title'].'</h3>';
			}
			switch ($type) {
				case 'LNK':
					$html .= '<ul class="links list-unstyled">'.$this->buildLinkHtml($column['LNK']).'</ul>';
					break;
				case 'PRO':
					$html .= $this->buildProductHtml($column['id_product']);
					break;
				case 'CUS':
					$html .= $column['custom_html'];
					break;
			}
			$html .= '</div>';
		}
		return $html;
	}

	public function buildProductHtml($id_product){
		$html = '';
		global $smarty;	
		if($id_product){
			$product = new Product($id_product,false,$this->langId);
			$linkObj = new Link();
			$link = $linkObj->getProductLink($product);
			$html .= '<div class="product-details">';
			$img = new Image($product->getCoverWs());
			$price = $product->getPrice();
			$actualPrice = $product->getPriceWithoutReduct();
			$html .= '<img alt="" class="img-responsive" draggable="false" src="'._PS_BASE_URL_._THEME_PROD_DIR_.$img->getExistingImgPath().'-home_default.jpg" >';
			$html .= '<a href="'.$link.'"><h4 class="name">'.$product->name.'</h4></a>';
			if($price == $actualPrice){
				$html .= '<span class="price" >'.$product->convertPrice(array('price' => $price),$smarty).'</span>';
			}else{
				$html .= '<span class="price" >'.$product->convertPrice(array('price' => $actualPrice),$smarty).'</span>';
				$html .= '<span class="price old-price pull-right" >'.$product->convertPrice(array('price' => $price),$smarty).'</span>';
			}
			$html .= '</div>';
		}
		return $html;
	}

	public function buildLinkHtml($links){
		$html = '';
		if(!empty($links)){
			foreach ($links as $link) {
				$linkDetails = $this->getLinkDetailsbyId($link);
				if($linkDetails['label'] && $linkDetails['url']){
					$html .= '<li><a href="'.$linkDetails['url'].'">'.$linkDetails['label'].'</a></li>';
				}
			}
		}
		return $html;
	}


	private function getMenuLink($menu){
		$link = '';
		if($menu['is_customlink']){
			return $menu['custom_link'];
		}elseif ($menu['link_object_id']) {
			$linkDetails = $this->getLinkDetailsbyId($menu['link_object_id']);
			return $linkDetails['url'];
		}
	}

	public function getLinkDetailsbyId($id){
		$type = substr($id,0,3);
		$objectId = substr($id,3);
		$link = array();

		switch ($type) {
			case 'CAT':
				$link = $this->getObjectLinkById($objectId,'Category');		
				break;
			case 'MAN':
				$link = $this->getObjectLinkById($objectId,'Manufacturer');		
				break;
			case 'SUP':
				$link = $this->getObjectLinkById($objectId,'Supplier');		
				break;
			case 'CMS':
				$link = $this->getObjectLinkById($objectId,'CMS','meta_title');		
				break;
			case 'CSL':
				$link = $this->getCustomLinksById($objectId,'');		
				break;
		}

		return $link;
	}

	protected function getObjectLinkById($objectId,$type,$nameKey = 'name'){
		$link = new Link();
		$linkfunction = 'get'.$type.'Link';
		$object = new $type($objectId,$this->langId);
		$linkUrl = $link->$linkfunction($object);
		$label = $object->$nameKey;
		return array('label'=>$label,'url'=>$linkUrl);
	}
	
	protected function getCustomLinksById($id){
		$link = new MegamenuLink($id,$this->langId);
		return array('label'=>$link->label,'url'=>$link->url);
	}
}