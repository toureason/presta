<div id="megamenu-admin">
	<div class="container1">
		{if !empty($flashMessage) && !empty($flashMessageType)}
			<div class="alert alert-{$flashMessageType}">
				{$flashMessage}
			</div>
		{/if}
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<a class="navbar-brand" href="{$baseURL}">{l s='Megamenu' mod='megamenu'}</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a data-toggle="dropdown" href="#">
								{l s='Menus' mod='megamenu'}
								<span class="caret"></span>
							</a>
							
							<ul aria-labelledby="drop1" role="menu" class="dropdown-menu">
								<li><a href="{$baseURL}&mm-controller=Menus&mm-action=edit">{l s='Add Menu' mod='megamenu'}</a></li>
								<li><a href="{$baseURL}&mm-controller=Menus&mm-action=index">{l s='List Menus' mod='megamenu'}</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a data-toggle="dropdown" href="#">
								{l s='Custom Links' mod='megamenu'}
								<span class="caret"></span>
							</a>
							<ul role="menu" class="dropdown-menu">
								<li><a href="{$baseURL}&mm-controller=Links&mm-action=edit">{l s='Add Link' mod='megamenu'}</a></li>
								<li><a href="{$baseURL}&mm-controller=Links&mm-action=index">{l s='List Links' mod='megamenu'}</a></li>
							</ul>
						</li>
						<li><a href="{$baseURL}&mm-controller=Pages&mm-action=settings" >{l s='Settings' mod='megamenu'}</a></li>
						<li><a href="mailto:transvelo@support.assembla.com?Subject=Support:%20Megamenu">{l s='Support' mod='megamenu'}</a></li>
					</ul>
					<p class="navbar-text navbar-right">{l s='Built by' mod='megamenu'} <a href="http://transvelo.com/">{l s='Transvelo' mod='megamenu'}</a></p>
				</div>
			</div>
		</nav>
		<div class="content">
			{$template}
		</div>
	</div>
</div>
<script type="text/javascript">
	$('.confirm-delete').click(function(){
		var didConfirm = confirm("Are you sure you want to delete this?");
		return didConfirm;
	});
</script>