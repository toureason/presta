<div>
	<form class="form-horizontal" method="POST">
		<div class="form-group">
			<label class="control-label col-lg-3">
				{l s='label' mod='megamenu'}
			</label>
			{foreach from=$languages item=language}
    			{if $languages|count > 1}
					<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $defaultFormLanguage}style="display:none"{/if}>
				{/if}
					<div class="col-lg-3">
						<input value="{if isset($menuLink->label[$language.id_lang])}{$menuLink->label[$language.id_lang]}{/if}" type="text" name="menuLink[label][{$language.id_lang}]" id="label_{$language.id_lang}-name">
					</div>
				{if $languages|count > 1}
					<div class="col-lg-2">
						<button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
							{$language.iso_code}
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							{foreach from=$languages item=lang}
								<li><a href="javascript:hideOtherLanguage({$lang.id_lang});" tabindex="-1">{$lang.name}</a></li>
							{/foreach}
						</ul>
					</div>
				{/if}
				{if $languages|count > 1}
					</div>
				{/if}
			{/foreach}
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3">
				{l s='Url' mod='megamenu'}
			</label>
			<div class="col-md-3"> 
				<input type="text" value="{if isset($menuLink->url)}{$menuLink->url}{/if}" name="menuLink[url]">
			</div>
			
		</div>
		<div class="form-group">
			<label class="control-label col-lg-3">
			</label>
			{if $id_megamenu_link}
				<input type="hidden" name="menuLink[id_megamenu_link]" vale="{$id_megamenu_link}">
			{/if}
			<input type="submit" class="btn btn-success" value="save" name="submit-link">
		</div>
	</form>
</div>