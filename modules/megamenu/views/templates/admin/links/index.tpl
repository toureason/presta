<div>
	{if !empty($links)}
		<table class="table table-border table-hover">
			<thead>
				<tr>
					<th>{l s='ID' mod='megamenu'}</th>
					<th>{l s='Label' mod='megamenu'}</th>
					<th>{l s='URL' mod='megamenu'}</th>
					<th>{l s='Action' mod='megamenu'}</th>
				</tr>
			</thead>
			<tbody>

				{foreach from=$links item=link}
				 	<tr>
					    <td>{$link.id_megamenu_link}</td>
					    <td>{$link.label}</td>
					    <td>{$link.url}</td>
					    <td>
					    	<a href="{$baseURL}&mm-controller=Links&mm-action=edit&id_megamenu_link={$link.id_megamenu_link}" class="btn btn-default">{l s='Edit' mod='megamenu'} <i class="icon-pencil"></i></a>
					    	<a href="{$baseURL}&mm-controller=Links&mm-action=delete&id_megamenu_link={$link.id_megamenu_link}" class="btn btn-default confirm-delete">{l s='Delete' mod='megamenu'} <i class="icon-trash"></i></a>
					    <tr/>
				{/foreach}
				
			</tbody>
		</table>
	{else}
		<div class="alert alert-warning">
			{l s='There is no more link' mod='megamenu'} <a href="#">{l s='click here' mod='megamenu'}</a> {l s='to add link' mod='megamenu'}
		</div>		
	{/if}

</div>