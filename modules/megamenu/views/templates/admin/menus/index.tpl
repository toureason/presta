<div>
	{if !empty($menus)}
	<table class="table table-border table-hover">
		<thead>
			<tr>
				<th>{l s='ID' mod='megamenu'}</th>
				<th>{l s='Label' mod='megamenu'}</th>
				<th>{l s='Position' mod='megamenu'}</th>
				<th>{l s='Status' mod='megamenu'}</th>
				<th>{l s='Action' mod='megamenu'}</th>
			</tr>
		</thead>
		<tbody>
			{foreach from=$menus item=menu}
				<tr>
				    <td>{$menu.id_megamenu_menu}</td>
				    <td>{$menu.label}</td>
				    <td>{$menu@iteration}</td>
				    <td>
				    	{if $menu.status}
				    		<a href="{$baseURL}&mm-controller=Menus&mm-action=edit&id_megamenu_menu={$menu.id_megamenu_menu}" class="list-action-enable action-enabled"><i class="icon-check"></i></a>
						{else}
				    		<a href="{$baseURL}&mm-controller=Menus&mm-action=edit&id_megamenu_menu={$menu.id_megamenu_menu}" class="list-action-enable action-disabled"><i class="icon-remove"></i></a>
				    	{/if}
				    </td>
				    <td>
				    	<a href="{$baseURL}&mm-controller=Menus&mm-action=edit&id_megamenu_menu={$menu.id_megamenu_menu}" class="btn btn-default">{l s='Edit' mod='megamenu'} <i class="icon-pencil"></i></a>
				    	<a href="{$baseURL}&mm-controller=Menus&mm-action=delete&id_megamenu_menu={$menu.id_megamenu_menu}" class="btn btn-default confirm-delete">{l s='Delete' mod='megamenu'} <i class="icon-trash"></i></a>
				    	<a href="{$baseURL}&mm-controller=Menus&mm-action=changePosition&mm-position=up&id_megamenu_menu={$menu.id_megamenu_menu}" class="btn btn-default">{l s='Up' mod='megamenu'} <i class="icon-arrow-up"></i></a>
				    	<a href="{$baseURL}&mm-controller=Menus&mm-action=changePosition&mm-position=down&id_megamenu_menu={$menu.id_megamenu_menu}" class="btn btn-default">{l s='Down' mod='megamenu'} <i class="icon-arrow-down"></i></a>
				    </td>
			    <tr/>
			{/foreach}
		</tbody>
	</table>
	{else}
		<div class="alert alert-warning">
			{l s='There is no more menu' mod='megamenu'} <a href="{$baseURL}&mm-controller=Menus&mm-action=edit">{l s='click here' mod='megamenu'}</a> {l s='to add menu' mod='megamenu'}
		</div>
	{/if}

</div>