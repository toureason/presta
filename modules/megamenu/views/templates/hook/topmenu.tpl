{if $menus}
<div class="col-lg-12" id="megamenu">
	<div class="navbar yamm navbar-default {if isset($settings['is_top']) && $settings['is_top']}navbar-fixed-top{/if}" role="navigation">
		<div class="navbar-header">
          <button class="navbar-toggle collapsed" data-target="#navbar-megamenu" data-toggle="collapse" type="button"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
		<div class="navbar-collapse collapse" id="navbar-megamenu">
			<ul class="nav navbar-nav">
				{$menus}
			</ul>
		</div>
	</div>
</div>
{/if}

<script>
	{if !empty($settings['effect'])}
		$('.dropdown').on('show.bs.dropdown', function () {
			$(this).find('.dropdown-menu').addClass('{$settings["effect"]}');
		});
		$('.dropdown').on('hide.bs.dropdown', function () {
			$(this).find('.dropdown-menu').removeClass('{$settings["effect"]}');
		});
	{/if}

	$('.megamenu-toggle').click(function(){
		$(this).parent().toggleClass('open');
	});

	$(document).on('click', '.yamm .dropdown-menu', function(e) {
	  e.stopPropagation();
	});

	$('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { 
		e.stopPropagation(); 
	});
	
</script>

