<?php

class CartAddress extends AddressCore {

    /** @var string e-mail address */
    public $email;

    public function getFields() {
        $ret = parent::getFields();
        $ret['id_cart'] = $this->id_cart;
        $ret['email'] = $this->email;
        return $ret;
    }
}
