<?php

class ToureasonDb {

    public static function logAPIAction($service, $ts, $session) {

        $sql = 'INSERT INTO ' . _DB_PREFIX_ . 'toureason_api_log (service, ts, session) '.
            "VALUES ('$service', '$ts', '$session')";

        if(Db::getInstance()->execute($sql)) {
            return Db::getInstance()->Insert_ID();
        }

        return null;
    }

    public static function logAPIError($id, $error, $request, $response) {

        $sql = 'UPDATE ' . _DB_PREFIX_ . 'toureason_api_log SET '.
            'errmsg = "' . $error .'", '.
            'request = "' . addslashes(json_encode($request)) . '", '.
            'response = "' . addslashes(json_encode($response)) . '" ' .
            "WHERE id = $id";

        return Db::getInstance()->execute($sql);
    }

    public static function loadCartTValues($cartId) {

        $sql = 'SELECT id_hotel, hotel_price, id_flight, flight_price, id_pnr FROM ' . _DB_PREFIX_ . 'cart '.
            'WHERE id_cart = ' . $cartId;
        $ret = Db::getInstance()->executeS($sql);
        if(is_array($ret) && isset($ret[0])) {
            return $ret[0];
        }

        return false;
    }

    public static function loadPNR($cartId) {

        $sql = 'SELECT json_pnr FROM ' . _DB_PREFIX_ . 'toureason_pnr ' .
            'WHERE id_cart = ' . $cartId . ' ORDER BY id_pnr DESC';
        $ret = Db::getInstance()->executeS($sql);

        if(is_array($ret) && isset($ret[0])) {
            return json_decode($ret[0]['json_pnr'], true);
        }

        return false;
    }

    public static function savePNR($pnr, $cartId) {
        $sql = 'UPDATE ' . _DB_PREFIX_ . 'toureason_pnr SET json_pnr = "'.addslashes(json_encode($pnr)).'" ' .
            'WHERE id_pnr = (SELECT id_pnr FROM '._DB_PREFIX_."cart WHERE id_cart = '$cartId') ";
        return Db::getInstance()->execute($sql);
    }

}
