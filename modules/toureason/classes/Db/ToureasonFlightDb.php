<?php

class ToureasonFlightDb {

    public static function saveFlightSearch($lastSearch, $soap, $result, $cartId) {

        $sql = 'REPLACE INTO ' . _DB_PREFIX_ . 'toureason_flight_search_result (id_cart, json_search, json_result, soap_response) '.
            'VALUES (' . $cartId . ', "' . addslashes(json_encode($lastSearch)) . '", "' . addslashes(json_encode($result)) . '", "' . addslashes(json_encode($soap)) . '")';

        return Db::getInstance()->execute($sql);
    }

    public static function loadFlightSearchResult($cartId) {

        $sql = 'SELECT json_result FROM ' . _DB_PREFIX_ . 'toureason_flight_search_result '.
            'WHERE id_cart = ' . $cartId;
        $ret = Db::getInstance()->executeS($sql);
        if(is_array($ret)) {
            return json_decode($ret[0]['json_result'], true);
        }

        return false;
    }

    public static function loadFlightSearch($cartId) {

        $sql = 'SELECT json_search FROM ' . _DB_PREFIX_ . 'toureason_flight_search_result '.
            'WHERE id_cart = ' . $cartId;
        $ret = Db::getInstance()->executeS($sql);
        if(is_array($ret) && isset($ret[0])) {
            return json_decode($ret[0]['json_search'], true);
        }

        return false;
    }

    public static function saveFlightSelect($flight, $pax, $cartId, $price) {

        $sql = 'INSERT INTO ' . _DB_PREFIX_ . 'toureason_flight_select (id_cart, json_flight, json_passengers) '.
            'VALUES (' . $cartId . ', "' . addslashes(json_encode($flight)) . '", "' . addslashes(json_encode($pax)) . '")';

        if(Db::getInstance()->execute($sql)) {
            $flightId = Db::getInstance()->Insert_ID();
            $sql = 'UPDATE ' . _DB_PREFIX_ . "cart SET id_flight = '$flightId', flight_price = '$price', date_upd = NOW() " .
                "WHERE id_cart = '$cartId' ";
            if(Db::getInstance()->execute($sql)) return $flightId;
        }

        return false;
    }

    public static function deleteFlightSelect($cartId) {
        $sql = 'UPDATE ' . _DB_PREFIX_ . 'cart SET id_flight = NULL, flight_price = NULL WHERE id_cart = ' . $cartId;

        if(Db::getInstance()->execute($sql)) {
            $sql = 'UPDATE ' . _DB_PREFIX_ . 'toureason_flight_select SET json_flight = "" WHERE id_cart = ' . $cartId;
            return Db::getInstance()->execute($sql);
        }
    }

    public static function loadFlightSelect($cartId) {

        $sql = 'SELECT json_flight, json_passengers FROM ' . _DB_PREFIX_ . 'toureason_flight_select ' .
            'WHERE id_cart = ' . $cartId . ' ORDER BY id_flight DESC';
        $ret = Db::getInstance()->executeS($sql);

        if(is_array($ret) && isset($ret[0])) return $ret[0];

        return false;
    }

    public static function saveCreatedPNR($pnr, $pax, $cartId) {

        $sql = 'INSERT INTO ' . _DB_PREFIX_ . 'toureason_pnr (id_cart, json_pnr) '.
            'VALUES (' . $cartId . ', "' . addslashes(json_encode($pnr)) . '")';

        if(Db::getInstance()->execute($sql)) {
            $pnrId = Db::getInstance()->Insert_ID();
            $sql = 'UPDATE ' . _DB_PREFIX_ . 'toureason_flight_select SET json_passengers = "'.addslashes(json_encode($pax)).'" ' .
                'WHERE id_flight = (SELECT id_flight FROM '._DB_PREFIX_."cart WHERE id_cart = '$cartId') ";
            if(Db::getInstance()->execute($sql)) {
                $sql = 'UPDATE ' . _DB_PREFIX_ . "cart SET id_pnr = '$pnrId', date_upd = NOW() " .
                "WHERE id_cart = '$cartId' ";
                if(Db::getInstance()->execute($sql)) return $pnrId;
            }
        }

        return false;
    }

    public static function savePax($names, $cartId) {
        $sql = 'SELECT id_flight FROM '._DB_PREFIX_."cart WHERE id_cart = '$cartId'";
        $ret = Db::getInstance()->executeS($sql);

        if(is_array($ret) && isset($ret[0])) {
            $flightId = $ret[0]['id_flight'];
            $sql = 'UPDATE ' . _DB_PREFIX_ . 'toureason_flight_select SET json_passengers = "'.addslashes(json_encode($names)).'" ' .
                " WHERE id_flight = $flightId";
            if(Db::getInstance()->execute($sql)) {
                $sql = 'DELETE FROM ' . _DB_PREFIX_ . "toureason_flight_select WHERE id_cart = $cartId AND id_flight != $flightId";
                Db::getInstance()->execute($sql);
                return true;
            }
        }

        return false;
    }

    public static function airportsLoad($term) {
        $sql =
            'SELECT iata_code, name, country FROM ' . _DB_PREFIX_ . 'toureason_airports '.
            'WHERE name LIKE "%' . $term . '%" OR iata_code = "' . $term . '" ORDER BY name';
        return Db::getInstance()->executeS($sql);
    }

    public static function loadCompanies($ids) {
        $ret = array();

        $sql =
            'SELECT * FROM ' . _DB_PREFIX_ . 'toureason_airlines '.
            'WHERE iata_code IN ("' . implode('","', $ids) . '") AND active = "Y"';
        $companies = Db::getInstance()->executeS($sql);

        foreach($companies as $company) {
            $ret[$company['iata_code']] = $company;
        }

        return $ret;
    }

    public static function loadLocations($ids) {
        $ret = array();

        $sql =
            'SELECT * FROM ' . _DB_PREFIX_ . 'toureason_airports '.
            'WHERE iata_code IN ("' . implode('","', $ids) . '")';
        $locations = Db::getInstance()->executeS($sql);

        foreach($locations as $location) {
            $ret[$location['iata_code']] = $location;
        }

        return $ret;
    }

}
