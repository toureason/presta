<?php


class ToureasonHotelDb {

    public static function saveHotelSearch($lastSearch, $result, $cartId) {

        $sql = 'REPLACE INTO ' . _DB_PREFIX_ . 'toureason_hotel_search_result (id_cart, json_search, json_result) '.
            'VALUES (' . $cartId . ', "' . addslashes(json_encode($lastSearch)) . '", "' . addslashes(json_encode($result)) . '")';

        return Db::getInstance()->execute($sql);
    }

    public static function updateHotelSearchResult($result, $cartId) {

        $sql = 'UPDATE ' . _DB_PREFIX_ . 'toureason_hotel_search_result '.
            "SET json_result = '" . addslashes(json_encode($result)).
            "' WHERE id_cart = $cartId";

        return Db::getInstance()->execute($sql);
    }

    public static function loadHotelSearchResult($cartId) {

        $sql = 'SELECT json_result FROM ' . _DB_PREFIX_ . 'toureason_hotel_search_result '.
            'WHERE id_cart = ' . $cartId;
        $ret = Db::getInstance()->executeS($sql);
        if(is_array($ret) && isset($ret[0])) {
            return json_decode($ret[0]['json_result'], true);
        }

        return false;
    }

    public static function loadHotelSearch($cartId) {

        $sql = 'SELECT json_search FROM ' . _DB_PREFIX_ . 'toureason_hotel_search_result '.
            'WHERE id_cart = ' . $cartId;
        $ret = Db::getInstance()->executeS($sql);
        if(is_array($ret) && isset($ret[0])) {
            return json_decode($ret[0]['json_search'], true);
        }

        return false;
    }

    public static function saveHotelSelect($hotel, $pax, $cartId, $price) {

        $sql = 'INSERT INTO ' . _DB_PREFIX_ . 'toureason_hotel_select (id_cart, json_hotel, json_passengers) '.
            'VALUES (' . $cartId . ', "' . addslashes(json_encode($hotel)) . '", "' . addslashes(json_encode($pax)) . '")';

        if(Db::getInstance()->execute($sql)) {
            $hotelId = Db::getInstance()->Insert_ID();
            $sql = 'UPDATE ' . _DB_PREFIX_ . "cart SET id_hotel = '$hotelId', hotel_price = '$price', date_upd = NOW() " .
                "WHERE id_cart = '$cartId' ";
            if(Db::getInstance()->execute($sql)) return $hotelId;
        }

        return false;
    }

    public static function deleteHotelSelect($cartId) {
        $sql = 'UPDATE ' . _DB_PREFIX_ . 'cart SET id_hotel = NULL, hotel_price = NULL WHERE id_cart = ' . $cartId;

        if(Db::getInstance()->execute($sql)) {
            $sql = 'UPDATE ' . _DB_PREFIX_ . 'toureason_hotel_select SET json_hotel = "" WHERE id_cart = ' . $cartId;
            return Db::getInstance()->execute($sql);
        }
    }

    public static function loadHotelSelect($cartId) {

        $sql = 'SELECT json_hotel, json_passengers FROM ' . _DB_PREFIX_ . 'toureason_hotel_select ' .
            'WHERE id_cart = ' . $cartId . ' ORDER BY id_hotel DESC';
        $ret = Db::getInstance()->executeS($sql);

        if(is_array($ret) && isset($ret[0])) return $ret[0];

        return false;
    }

    public static function savePax($names, $cartId) {
        $sql = 'SELECT id_hotel FROM '._DB_PREFIX_."cart WHERE id_cart = '$cartId'";
        $ret = Db::getInstance()->executeS($sql);

        if(is_array($ret) && isset($ret[0])) {
            $hotelId = $ret[0]['id_hotel'];
            $sql = 'UPDATE ' . _DB_PREFIX_ . 'toureason_hotel_select SET json_passengers = "'.addslashes(json_encode($names)).'" ' .
                " WHERE id_hotel = $hotelId";
            if(Db::getInstance()->execute($sql)) {
                $sql = 'DELETE FROM ' . _DB_PREFIX_ . "toureason_hotel_select WHERE id_cart = $cartId AND id_hotel != $hotelId";
                Db::getInstance()->execute($sql);
                return true;
            }
        }

        return false;
    }

    public static function citiesLoad($term) {
        $sql =
            'SELECT iata_code, name, country FROM ' . _DB_PREFIX_ . 'toureason_airports '.
            'WHERE (name LIKE "%' . $term . '%" OR iata_code = "' . $term . '") AND iscity = "yes" ORDER BY name';
        return Db::getInstance()->executeS($sql);
    }
}
