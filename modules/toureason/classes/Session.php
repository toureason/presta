<?php

class Session {

    public $id = null;
    public $sequence = null;
    public $token = null;

    public function __construct($id, $sequence, $token) {
        $this->id = $id;
        $this->sequence = intval($sequence);
        $this->token = $token;
    }

    public function nextStep() {
        if(isset($this->sequence) && ($this->sequence)) $this->sequence++;
    }
}
