<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/Session.php');

class SoapClientToureason extends SoapClient {

    protected $debug = true;

    protected $soapEndPoint = "https://nodeD1.test.webservices.amadeus.com/1ASIWPRGELU";
    protected $login = 'WSELUPRG';
    protected $passwd = 'AMADEUS';
    protected $soapAction = '';
    protected $soapURI = '';
    protected $method = '';
    protected $methodReply = '';
    protected $session = null;
    protected $headers = [];

    protected $request = null;
    protected $logId = null;

    public static $ts = null;

    public function __construct() {
        parent::__construct(null, array(
            'location' => $this->soapEndPoint,
            'uri' => $this->soapURI,
            'soap_version' => SOAP_1_1,
            'trace' => 1)
        );
        if(! $this->methodReply) $this->methodReply = $this->method.'Reply';
    }


    public function process() {
        $this->setHeaders();
        try {
            return $this->__soapCall($this->method, $this->prepareArgs());
        }
        catch (SoapFault $ex) {
            $this->logError(addslashes(substr($ex->getMessage(), 0, 250)));
            throw new Exception(Tools::displayError('Error calling external service.'));
        }
    }

    protected function prepareArgs() {
        return array();
    }

    protected function createSVar($name, $val) {
        return new SoapVar($val, XSD_STRING, null, null, $name);
    }

    protected function createAVar($name, $val) {
        return new SoapVar($val, SOAP_ENC_OBJECT, null, null, $name);
    }

    protected function setHeaders() {
        $this->headers[] = new AddressingHeader('MessageID', uniqid('', true));
        $this->headers[] = new AddressingHeader('Action', $this->soapAction);
        $this->headers[] = new AddressingHeader('To', $this->soapEndPoint);
        $this->headers[] = new AMASecurityHeader(isset($this->session));
        $this->headers[] = new LinkHeader();
        $this->__setSoapHeaders($this->headers);
    }

	/**
	 * if SearchCacheLevel="LessRecent" select from Amadeus (cache)
	 * if SearchCacheLevel="Live" select from real data
	 * if SearchCacheLevel="VeryRecent" select from cache and real
	 *
	 * if AvailRatesOnly="true" - filtering via available properties
	 *
	 * @param type $request
	 * @param type $location
	 * @param type $action
	 * @param type $version
	 * @param type $one_way
	 * @return type
	 */
    public function __doRequest($request, $location, $action, $version, $one_way = 0) {
        $request = preg_replace('/<ns([0-9]+):Password>/', '<ns$1:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">', $request);
        $request = preg_replace('/<ns([0-9]+):Session([a-zA-Z]{3,})/', '<ns$1:Session TransactionStatusCode="$2"', $request);
        $request = preg_replace('/<\/ns([0-9]+):Session([a-zA-Z]{3,})>/', '</ns$1:Session>', $request);
        if($this->debug){
			dump($request);
		}

        $this->request = $request;
        $this->logId = ToureasonDb::logAPIAction($this->method, SoapClientToureason::$ts, isset($this->session) ? $this->session->id : null);

        $response = parent::__doRequest($request, $location, $this->soapAction, $version, $one_way);
        $this->logCommunication($response);

        return $response;
    }

    public function getSession() {
        $response = $this->__getLastResponse();
        if(preg_match('/SessionId>([^<]+)<.*SequenceNumber>([^<]+)<.*SecurityToken>([^<]+)</', $response, $matches)) {
            return new Session($matches[1], $matches[2], $matches[3]);
        }
        return null;
    }

    public function setSession($session) {
        $this->session = $session;
    }

    protected function getResponse() {
        $response = $this->__getLastResponse();
        if(preg_match('/<' . $this->methodReply . '[\s\S]*<\/' . $this->methodReply . '>/i', $response, $bodyXml) !== 1) {
            throw new Exception(Tools::displayError('Bad response.'));
        }
		return simplexml_load_string($bodyXml[0]);
    }

    public static function obj2array($obj) {
        $out = array();
        foreach ($obj as $key => $val) {
            switch(true) {
                case is_object($val):
                    $out[$key] = self::obj2array($val);
                    break;
                case is_array($val):
                    $out[$key] = self::obj2array($val);
                    break;
                default:
                    $out[$key] = $val;
            }
        }
        return $out;
    }

    protected function getDateNormal($date) {
        $phelpVar = explode('.', $date);
        return $phelpVar[2] . '-' . $phelpVar[1] . '-' . $phelpVar[0];
    }

    protected function logError($msg) {
        ToureasonDb::logAPIError($this->logId, $msg, $this->request, $this->__getLastResponse());
    }

    protected function logCommunication($response) {
        ToureasonDb::logAPIError($this->logId, '', $this->request, $response);
    }
}
