<?php

class WsseAuthHeader extends SoapHeader {

    private $wss_ns = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
    private $wsu_ns = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';

    public function __construct($user, $password) {

        $prefix = gethostname();
        $nonce = substr( md5( uniqid( $prefix.'_', true)), 0, 16);
        date_default_timezone_set("UTC");
        $t = microtime(true);
        $micro = sprintf("%03d",($t - floor($t)) * 1000);
        $date = new DateTime( date('Y-m-d H:i:s.'.$micro) );
        $timestamp = $date->format("Y-m-d\TH:i:s:").$micro . 'Z';
        $encodedNonce = base64_encode($nonce);
        $passSHA = base64_encode(sha1($nonce . $timestamp . sha1($password, true), true));

        $auth = new stdClass();
        $auth->Username = new SoapVar($user, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);
        $auth->Password = new SoapVar($passSHA, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);
        $auth->Nonce = new SoapVar($encodedNonce, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);
        $auth->Created = new SoapVar($timestamp, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wsu_ns);

        $username_token = new stdClass();
        $username_token->UsernameToken = new SoapVar($auth, SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns);

        $security_sv = new SoapVar(
            new SoapVar($username_token, SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns),
            SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'Security', $this->wss_ns);

        SoapClientToureason::$ts = $timestamp;

        parent::__construct($this->wss_ns, 'Security', $security_sv);
    }
}

class AMASecurityHeader extends SoapHeader {

    private $ns = 'http://xml.amadeus.com/2010/06/Security_v1';

    public function __construct($session=false) {

        $UserId = '<AMA_SecurityHostedUser xmlns="' . $this->ns . '"';
        if($session) $UserId .= ' />';
        else $UserId .= '><UserID POS_Type="1" PseudoCityCode="PRGCK28ER" AgentDutyCode="SU" RequestorType="U" /></AMA_SecurityHostedUser>';
        $sv = new SoapVar($UserId, XSD_ANYXML);

        parent::__construct($this->ns, 'AMA_SecurityHostedUser', $sv);
    }
}

class AddressingHeader extends SoapHeader {

    private $ns = 'http://www.w3.org/2005/08/addressing';

    public function __construct($name, $value) {
        parent::__construct($this->ns, $name, $value);
    }
}

class LinkHeader extends SoapHeader {

    private $ns = 'http://wsdl.amadeus.com/2010/06/ws/Link_v1';

    function __construct() {
        parent::__construct($this->ns, 'TransactionFlowLink');
    }
}

class SessionStartHeader extends SoapHeader {

    private $ns = 'http://xml.amadeus.com/2010/06/Session_v3';

    public function __construct() {
        parent::__construct($this->ns, 'SessionStart');
    }
}

class SessionSequenceHeader extends SoapHeader {

    private $ns = 'http://xml.amadeus.com/2010/06/Session_v3';

    public function __construct($session, $status='InSeries') {
        $session->nextStep();

        $sess = new stdClass();
        $sess->SessionId = new SoapVar($session->id, XSD_STRING, NULL, $this->ns, NULL, $this->ns);
        $sess->SequenceNumber = new SoapVar($session->sequence, XSD_STRING, NULL, $this->ns, NULL, $this->ns);
        $sess->SecurityToken = new SoapVar($session->token, XSD_STRING, NULL, $this->ns, NULL, $this->ns);
        $sv = new SoapVar($sess, SOAP_ENC_OBJECT, NULL, $this->ns, 'Session', $this->ns);

        parent::__construct($this->ns, 'Session' . $status, $sv);
    }
}
