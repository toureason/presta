<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/SoapClientToureason.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');

class ToureasonFlightCreateTST extends SoapClientToureason {

    protected $soapURI = 'http://xml.amadeus.com/TAUTCQ_04_1_1A';
    protected $soapAction = 'http://webservices.amadeus.com/TAUTCQ_04_1_1A';
    protected $method = 'Ticket_CreateTSTFromPricing';
    protected $debug = false;

    public $passengers = array();


    public function parseResponse($response) {
        $respObject = $this->getResponse();
        //dump($respObject);

        if(isset($respObject->applicationError)) {
            $this->logError($respObject->applicationError->errorText->errorFreeText);
            throw new Exception(Tools::displayError('Unable to book flight.'));
        }

        return true;
    }

    protected function setHeaders() {
        $this->headers[] = new SessionSequenceHeader($this->session);
        parent::setHeaders();
    }

    protected function prepareArgs() {

        $args = array();

        foreach($this->passengers as $key => $pass) {
            if($pass['cnt'] > 0) {
                $args[] =
                    $this->createAVar('psaList', array(
                        $this->createAVar('itemReference', array(
                            $this->createSVar('referenceType', $pass['type']),
                            $this->createSVar('uniqueReference', $pass['unique'])
                        ))
                    ));
            }
        }

        return $args;
    }


}
