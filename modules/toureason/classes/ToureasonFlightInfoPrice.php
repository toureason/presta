<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/SoapClientToureason.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');

class ToureasonFlightInfoPrice extends SoapClientToureason {

    protected $soapURI = 'http://xml.amadeus.com/TIPNRQ_13_2_1A';
    protected $soapAction = 'http://webservices.amadeus.com/TIPNRQ_13_2_1A';
    protected $method = 'Fare_InformativePricingWithoutPNR';
    protected $debug = false;

    public $fgroup = array();
    public $passengers = array();

    public function parseResponse($response) {
        //dump($response);

        if(isset($response['errorGroup'])) $ret['error'] = $response['errorGroup']->errorWarningDescription->freeText;
        else {
            $priceGroups = $response['mainGroup']->pricingGroupLevelGroup;
            if(! is_array($priceGroups)) $priceGroups = array($priceGroups);
            $retPrice = 0;
            $index = 0;
            foreach($priceGroups[$index]->fareInfoGroup->textData as $warning) {
                if($warning->freeTextQualification->informationType == "710") {
                    $this->logError('Class availability may not be sufficient on certain flights.');
                    throw new Exception(Tools::displayError('Not available enough seats.'));
                }
            }
            foreach($this->passengers as $key => $parr) {
                if($parr['cnt'] > 0) {
                    $omd = $priceGroups[$index]->fareInfoGroup->fareAmount->otherMonetaryDetails;
                    if(! is_array($omd)) $omd = array($omd);
                    foreach($omd as $monetary) {
                        if($monetary->typeQualifier == '712') {
                            $this->passengers[$key]['price'] = $monetary->amount;
                            $retPrice += $monetary->amount * $parr['cnt'];
                        }
                    }
                    $index++;
                }
            }
            if($retPrice != $this->fgroup[1][0]['priceOrig']) {
                $this->logError('Price check not passed. MP: ' . $this->fgroup[1][0]['priceOrig'] . ' Info: ' . $retPrice);
                throw new Exception(Tools::displayError('Price check not passed.'));
            }
        }

        return true;
    }

    protected function setHeaders() {
        $this->headers[] = new WsseAuthHeader($this->login, $this->passwd);
        parent::setHeaders();
    }

    protected function prepareArgs() {
        $args = $this->addPax();

        $itemNum = 1;
        $carrier = '';
        foreach($this->fgroup as $seg => $fgroup) {
            foreach($fgroup as $flid => $flight) {
                $carrier = $flight['carrier'];
                $args[] =
                    $this->createAVar('segmentGroup', array(
                        $this->createAVar('segmentInformation', array(
                            $this->createAVar('flightDate', array(
                                $this->createSVar('departureDate', $flight['depDateOrig'])
                            )),
                            $this->createAVar('boardPointDetails', array(
                                $this->createSVar('trueLocationId', $flight['loc'][0]['id'])
                            )),
                            $this->createAVar('offpointDetails', array(
                                $this->createSVar('trueLocationId', $flight['loc'][1]['id'])
                            )),
                            $this->createAVar('companyDetails', array(
                                $this->createSVar('marketingCompany', $flight['company'])
                            )),
                            $this->createAVar('flightIdentification', array(
                                $this->createSVar('flightNumber', $flight['number']),
                                $this->createSVar('bookingClass', $flight['class'])
                            )),
                            $this->createAVar('flightTypeDetails', array(
                                $this->createSVar('flightIndicator', $seg)
                            )),
                            $this->createSVar('itemNumber', ($itemNum))
                        ))
                    ));
                $itemNum++;
            }
        }

        $args[] =
            $this->createAVar('pricingOptionGroup', array(
                $this->createAVar('pricingOptionKey', array(
                    $this->createSVar('pricingOptionKey', 'RP')
                ))
            ));

        $args[] =
            $this->createAVar('pricingOptionGroup', array(
                $this->createAVar('pricingOptionKey', array(
                    $this->createSVar('pricingOptionKey', 'RU')
                ))
            ));

        if($carrier && (strlen($carrier) == 2)) {
            $args[] =
                $this->createAVar('pricingOptionGroup', array(
                    $this->createAVar('pricingOptionKey', array(
                        $this->createSVar('pricingOptionKey', 'VC')
                    )),
                    $this->createAVar('carrierInformation', array(
                        $this->createAVar('companyIdentification', array(
                            $this->createSVar('otherCompany', $carrier)
                        ))
                    ))
                ));

        $args[] =
            $this->createAVar('pricingOptionGroup', array(
                $this->createAVar('pricingOptionKey', array(
                    $this->createSVar('pricingOptionKey', 'FOP')
                )),
                $this->createAVar('formOfPaymentInformation', array(
                    $this->createAVar('formOfPayment', array(
                        $this->createSVar('type', 'CC'),
                        $this->createSVar('creditCardNumber', substr(Configuration::get('TOUREASON_CC_NUMBER'), 0, 6))
                    ))
                ))
            ));
        }

        return $args;
    }


    private function addPax() {
        $ret = array();
        $sequence = 1;
        foreach($this->passengers as $key => $parr) {
            if($parr['cnt'] > 0) {
                $travellers = array();
                for($i = 1; $i <= $parr['cnt']; $i++) {
                    $travellers[] = $this->createAVar('travellerDetails', array(
                        $this->createSVar('measurementValue', ($key == 'CH') ? ($i + $this->passengers['ADT']['cnt']) : $i),
                    ));
                }

                $dptc = array($this->createSVar('valueQualifier', $key));
                if($key == 'INF') $dptc[] = $this->createAVar('fareDetails', array($this->createSVar('qualifier', 766)));

                $ret[] =
                    $this->createAVar('passengersGroup', array(
                        $this->createAVar('segmentRepetitionControl', array(
                            $this->createAVar('segmentControlDetails', array(
                                $this->createSVar('quantity', $sequence),
                                $this->createSVar('numberOfUnits', $parr['cnt']),
                            ))
                        )),
                        $this->createAVar('travellersID', $travellers),
                        $this->createAVar('discountPtc', $dptc)
                    ));
            }
            $sequence++;
        }

        return $ret;
    }

}
