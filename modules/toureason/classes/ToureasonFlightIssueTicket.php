<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/SoapClientToureason.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');

class ToureasonFlightIssueTicket extends SoapClientToureason {

    protected $soapURI = 'http://xml.amadeus.com/TTKTIQ_09_1_1A';
    protected $soapAction = 'http://webservices.amadeus.com/TTKTIQ_09_1_1A';
    protected $method = 'DocIssuance_IssueTicket';
    protected $debug = false;

    public $indicator = '';


    public function parseResponse($response) {
        $respObject = $this->getResponse();
        //dump($respObject);

        if($respObject->processingStatus->statusCode == 'O') {
            return true;
        }

        $this->logError($respObject->errorGroup->errorWarningDescription->freeText);
        throw new Exception(Tools::displayError('Unable to issue tickets.'));
    }

    protected function setHeaders() {
        $this->headers[] = new SessionSequenceHeader($this->session);
        parent::setHeaders();
    }

    protected function prepareArgs() {

        $args[] =
            $this->createAVar('optionGroup', array(
                $this->createAVar('switches', array(
                    $this->createAVar('statusDetails', array(
                        $this->createSVar('indicator', $this->indicator)
                    ))
                ))
            ));

        return $args;
    }

}
