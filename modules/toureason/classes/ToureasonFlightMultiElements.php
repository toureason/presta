<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/SoapClientToureason.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');

class ToureasonFlightMultiElements extends SoapClientToureason {

    protected $soapURI = 'http://xml.amadeus.com/PNRADD_14_1_1A';
    protected $soapAction = 'http://webservices.amadeus.com/PNRADD_14_1_1A';
    protected $method = 'PNR_AddMultiElements';
    protected $debug = false;

    public $passengers = array();
    public $marketingCarrier = '';

    public function parseResponse($response) {
        //dump($response);
        if(is_array($response) && ! isset($response['generalErrorInfo'])) {
            $ti = $response['travellerInfo'];
            if(! is_array($ti)) $ti = array($ti);

            foreach($ti as $traveller) {
                $pids = $traveller->passengerData;
                if(! is_array($pids)) $pids = array($pids);
                foreach($pids as $pid) {
                    $pids2 = $pid->travellerInformation->passenger;
                    if(! is_array($pids2)) $pids2 = array($pids2);
                    foreach($pids2 as $pass) {
                        $type = $pass->type;
                        if($type == 'CHD') $type = 'CH';
                        $this->passengers[$type]['ids'][] = $traveller->elementManagementPassenger->reference->number;
                    }
                }
            }

            return true;
        }

        $this->logError($response->generalErrorInfo->messageErrorText->text);
        throw new Exception(Tools::displayError('Unable to book flight.'));
    }

    protected function setHeaders() {
        $this->headers[] = new SessionSequenceHeader($this->session);
        parent::setHeaders();
    }

    protected function prepareArgs() {

        $args[] =
            $this->createAVar('pnrActions', array(
                $this->createSVar('optionCode', 0)
            ));

        $seat = 1;
        $travellers = array();
        $ffcards = array();
        for($index = 1; $index <= $this->passengers['ADT']['cnt']; $index++) {
            $pass = $this->passengers['ADT']['list'][$index];
            if($pass['ffcard'] && $pass['ffcardcode']) $ffcards[$seat] = $pass;

            $passenger = array(
                $this->createSVar('firstName', $pass['first'] . (isset($pass['sal']) ? ' ' . $pass['sal'] : '')),
                $this->createSVar('type', 'ADT')
            );
            if(isset($this->passengers['INF']['list'][$index])) $passenger[] = $this->createSVar('infantIndicator', 3);

            $traveller = array(
                $this->createAVar('elementManagementPassenger', array(
                    $this->createAVar('reference', array(
                        $this->createSVar('qualifier', 'PR'),
                        $this->createSVar('number', $seat++)
                    )),
                    $this->createSVar('segmentName', 'NM')
                )),
                $this->createAVar('passengerData', array(
                    $this->createAVar('travellerInformation', array(
                        $this->createAVar('traveller', array(
                            $this->createSVar('surname', $pass['last']),
                            $this->createSVar('quantity', isset($this->passengers['INF']['list'][$index]) ? 2 : 1)
                        )),
                        $this->createAVar('passenger', $passenger)
                    ))
                ))
            );

            if($this->passengers['INF']['cnt'] >= $index) {
                $dbinf = strtoupper(date('dMy', mktime(0, 0, 0,
                    substr($this->passengers['INF']['list'][$index]['birth'], 3, 2),
                    substr($this->passengers['INF']['list'][$index]['birth'], 0, 2),
                    substr($this->passengers['INF']['list'][$index]['birth'], 6, 4)
                )));
                $traveller[] =
                    $this->createAVar('passengerData', array(
                        $this->createAVar('travellerInformation', array(
                            $this->createAVar('traveller', array(
                                $this->createSVar('surname', $this->passengers['INF']['list'][$index]['last'])
                            )),
                            $this->createAVar('passenger', array(
                                $this->createSVar('firstName', $this->passengers['INF']['list'][$index]['first'] . (isset($this->passengers['INF']['list'][$index]['sal']) ? ' ' . $this->passengers['INF']['list'][$index]['sal'] : '')),
                                $this->createSVar('type', 'INF')
                            ))
                        )),
                        $this->createAVar('dateOfBirth', array(
                            $this->createAVar('dateAndTimeDetails', array(
                                $this->createSVar('date', $dbinf)
                            ))
                        ))
                    ));
            }

            $travellers[] = $this->createAVar('travellerInfo', $traveller);
        }

        if(isset($this->passengers['CH']['list'])) {
            for($index = 1; $index <= $this->passengers['CH']['cnt']; $index++) {
                $pass = $this->passengers['CH']['list'][$index];
                if($pass['ffcard'] && $pass['ffcardcode']) $ffcards[$seat] = $pass;

                $travellers[] = $this->createAVar('travellerInfo', array(
                    $this->createAVar('elementManagementPassenger', array(
                        $this->createAVar('reference', array(
                            $this->createSVar('qualifier', 'PR'),
                            $this->createSVar('number', $seat++)
                        )),
                        $this->createSVar('segmentName', 'NM')
                    )),
                    $this->createAVar('passengerData', array(
                        $this->createAVar('travellerInformation', array(
                            $this->createAVar('traveller', array(
                                $this->createSVar('surname', $pass['last']),
                                $this->createSVar('quantity', 1)
                            )),
                            $this->createAVar('passenger', array(
                                $this->createSVar('firstName', $pass['first'] . (isset($pass['sal']) ? ' ' . $pass['sal'] : '')),
                                $this->createSVar('type', 'CHD')
                            ))
                        ))
                    ))
                ));
            }
        }

        $args = array_merge($args, $travellers);

        $dataElements[] = $this->createSVar('marker1', '');

        $dataElements[] =
            $this->createAVar('dataElementsIndiv', array(
                $this->createAVar('elementManagementData', array(
                    $this->createSVar('segmentName', 'AP')
                )),
                $this->createAVar('freetextData', array(
                    $this->createAVar('freetextDetail', array(
                        $this->createSVar('subjectQualifier', 3),
                        $this->createSVar('type', 'P21')
                    )),
                    $this->createSVar('longFreetext', '')
                ))
            ));

        $dataElements[] =
            $this->createAVar('dataElementsIndiv', array(
                $this->createAVar('elementManagementData', array(
                    $this->createSVar('segmentName', 'TK')
                )),
                $this->createAVar('ticketElement', array(
                    $this->createAVar('ticket', array(
                        $this->createSVar('indicator', 'OK'),
                    ))
                ))
            ));

        $dataElements[] =
            $this->createAVar('dataElementsIndiv', array(
                $this->createAVar('elementManagementData', array(
                    $this->createSVar('segmentName', 'RF')
                )),
                $this->createAVar('freetextData', array(
                    $this->createAVar('freetextDetail', array(
                        $this->createSVar('subjectQualifier', 3),
                        $this->createSVar('type', 'P22'),
                    )),
                    $this->createSVar('longFreetext', 'AWSUI')
                ))
            ));

        foreach($ffcards as $index => $pass) {
            $dataElements[] =
                $this->createAVar('dataElementsIndiv', array(
                    $this->createAVar('elementManagementData', array(
                        $this->createSVar('segmentName', 'SSR')
                    )),
                    $this->createAVar('serviceRequest', array(
                        $this->createAVar('ssr', array(
                            $this->createSVar('type', 'FQTV'),
                            $this->createSVar('companyId', $this->marketingCarrier),
                            $this->createSVar('indicator', 'P01')
                        ))
                    )),
                    $this->createAVar('frequentTravellerData', array(
                        $this->createAVar('frequentTraveller', array(
                            $this->createSVar('companyId', $pass['ffcardcode']),
                            $this->createSVar('membershipNumber', $pass['ffcard'])
                        ))
                    )),
                    $this->createAVar('referenceForDataElement', array(
                        $this->createAVar('reference', array(
                            $this->createSVar('qualifier', 'PR'),
                            $this->createSVar('number', $index)
                        ))
                    ))
                ));
        }

        $args[] = $this->createAVar('dataElementsMaster', $dataElements);

        return $args;
    }


}
