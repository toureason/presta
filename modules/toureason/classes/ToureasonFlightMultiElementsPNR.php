<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/ToureasonFlightMultiElements.php');


class ToureasonFlightMultiElementsPNR extends ToureasonFlightMultiElements {

    protected $methodReply = 'PNR_Reply';

    protected $debug = false;

    public function parseResponse($response) {
        $respObject = $this->getResponse();
        //dump($respObject);

        if(isset($respObject->generalErrorInfo)) {
            $this->logError(trim($respObject->generalErrorInfo->messageErrorText->text));
            throw new Exception(Tools::displayError('Unable to book flight.'));
        }

        $ret = array(
            'companyId' => strval($respObject->pnrHeader->reservationInfo->reservation->companyId),
            'controlNumber' => strval($respObject->pnrHeader->reservationInfo->reservation->controlNumber)
        );

        return $ret;
    }

    protected function prepareArgs() {

        $args[] =
            $this->createAVar('pnrActions', array(
                $this->createSVar('optionCode', 10),
                $this->createSVar('optionCode', 30)
            ));

        return $args;
    }

}
