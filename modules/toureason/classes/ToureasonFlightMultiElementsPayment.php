<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/ToureasonFlightMultiElements.php');


class ToureasonFlightMultiElementsPayment extends ToureasonFlightMultiElements {

    //const CC_TYPE = 'VI';
    //const CC_NUMBER = '4444339999991118';
    //const CC_NUMBER = '4000000000000002';
    //const CC_NUMBER = '4444330322221117';
    //const CC_TYPE = 'AX';
    //const CC_NUMBER = '371449635311004';
    //const CC_TYPE = 'CA';
    //const CC_NUMBER = '5499830000000015';
    //const CC_EXPIRATION = '0616';
    //const CC_CVC = '999';

    protected $debug = false;

    protected $methodReply = 'PNR_Reply';

    public function parseResponse($response) {
        $respObject = $this->getResponse();
        //dump($respObject);

        if(isset($respObject->generalErrorInfo)) {
            $this->logError(trim($respObject->generalErrorInfo->messageErrorText->text));
            throw new Exception(Tools::displayError('Unable to issue tickets.'));
        }

        return true;
    }

    protected function prepareArgs() {

        $args[] =
            $this->createAVar('pnrActions', array(
                $this->createSVar('optionCode', 0)
            ));

        $args[] =
            $this->createAVar('dataElementsMaster', array(
                $this->createSVar('marker1', ''),
                // form of payment
                $this->createAVar('dataElementsIndiv', array(
                    $this->createAVar('elementManagementData', array(
                        $this->createSVar('segmentName', 'FP')
                    )),
                    $this->createAVar('formOfPayment', array(
                        $this->createAVar('fop', array(
                            $this->createSVar('identification', 'CC'),
                            $this->createSVar('creditCardCode', Configuration::get('TOUREASON_CC_TYPE')),
                            $this->createSVar('accountNumber', Configuration::get('TOUREASON_CC_NUMBER')),
                            $this->createSVar('expiryDate', Configuration::get('TOUREASON_CC_EXPIRE'))
                        ))
                    ))
                )),
                // agency commission
                $this->createAVar('dataElementsIndiv', array(
                    $this->createAVar('elementManagementData', array(
                        $this->createAVar('reference', array(
                            $this->createSVar('qualifier', 'OT'),
                            $this->createSVar('number', 1)
                        )),
                        $this->createSVar('segmentName', 'FM')
                    )),
                    $this->createAVar('commission', array(
                        $this->createAVar('commissionInfo', array(
                            $this->createSVar('percentage', 0)
                        ))
                    ))
                ))
            ));

        return $args;
    }

}
