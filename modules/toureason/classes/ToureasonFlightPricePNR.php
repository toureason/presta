<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/SoapClientToureason.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');

class ToureasonFlightPricePNR extends SoapClientToureason {

    protected $soapURI = 'http://xml.amadeus.com/TPCBRQ_13_2_1A';
    protected $soapAction = 'http://webservices.amadeus.com/TPCBRQ_13_2_1A';
    protected $method = 'Fare_PricePNRWithBookingClass';
    protected $debug = false;

    public $fgroup = array();
    public $passengers = array();


    public function parseResponse($response) {
        $respObject = $this->getResponse();
        //dump($respObject);

        if(isset($respObject->applicationError)) {
            $this->logError($respObject->applicationError->errorWarningDescription->freeText);
            throw new Exception(Tools::displayError('Unable to book flight.'));
        }

        $retPrice = 0;
        foreach($respObject->fareList as $fdet) {
            $key = 'ADT';
            if($fdet->paxSegReference->refDetails->refQualifier == 'PI') $key = 'INF';
            else if(isset($this->passengers['CH']['ids']) && in_array($fdet->paxSegReference->refDetails->refNumber, $this->passengers['CH']['ids'])) $key = 'CH';

            foreach($fdet->fareDataInformation->fareDataSupInformation as $monetary) {
                if($monetary->fareDataQualifier == '712') {
                    if($monetary->fareAmount == $this->passengers[$key]['price']) {
                        $this->passengers[$key]['type'] = $fdet->fareReference->referenceType;
                        $this->passengers[$key]['unique'] = $fdet->fareReference->uniqueReference;
                        $retPrice += ($monetary->fareAmount * $this->passengers[$key]['cnt']);
                    }
                }
            }
        }

        if($retPrice != $this->fgroup[1][0]['priceOrig']) {
            $this->logError('Price check not passed. MP: ' . $this->fgroup[1][0]['priceOrig'] . ' Info: ' . $retPrice);
            throw new Exception(Tools::displayError('Price check not passed.'));
        }

        foreach($this->passengers as $pass) {
            if(($pass['cnt'] > 0) && ! isset($pass['unique'])) {
                $this->logError('Ticket is not available.');
                throw new Exception(Tools::displayError('Ticket is not available.'));
            }
        }

        return true;
    }

    protected function setHeaders() {
        $this->headers[] = new SessionSequenceHeader($this->session);
        parent::setHeaders();
    }

    protected function prepareArgs() {

        $args[] =
            $this->createAVar('pricingOptionGroup', array(
                $this->createAVar('pricingOptionKey', array(
                    $this->createSVar('pricingOptionKey', 'RP')
                ))
            ));

        $args[] =
            $this->createAVar('pricingOptionGroup', array(
                $this->createAVar('pricingOptionKey', array(
                    $this->createSVar('pricingOptionKey', 'RU')
                ))
            ));

        if(isset($this->fgroup[1][0]['carrier']) && (strlen($this->fgroup[1][0]['carrier']) == 2)) {
            $args[] =
                $this->createAVar('pricingOptionGroup', array(
                    $this->createAVar('pricingOptionKey', array(
                        $this->createSVar('pricingOptionKey', 'VC')
                    )),
                    $this->createAVar('carrierInformation', array(
                        $this->createAVar('companyIdentification', array(
                            $this->createSVar('otherCompany', $this->fgroup[1][0]['carrier'])
                        ))
                    ))
                ));
        }

        $args[] =
            $this->createAVar('pricingOptionGroup', array(
                $this->createAVar('pricingOptionKey', array(
                    $this->createSVar('pricingOptionKey', 'FOP')
                )),
                $this->createAVar('formOfPaymentInformation', array(
                    $this->createAVar('formOfPayment', array(
                        $this->createSVar('type', 'CC'),
                        $this->createSVar('creditCardNumber', substr(Configuration::get('TOUREASON_CC_NUMBER'), 0, 6))
                    ))
                ))
            ));

        return $args;
    }


}
