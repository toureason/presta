<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/SoapClientToureason.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/Db/ToureasonFlightDb.php');

class ToureasonFlightSearch extends SoapClientToureason {

    protected $soapURI = 'http://xml.amadeus.com/FMPTBQ_14_3_1A';
    protected $soapAction = 'http://webservices.amadeus.com/FMPTBQ_14_3_1A';
    protected $method = 'Fare_MasterPricerTravelBoardSearch';
    protected $debug = false;

    public $from = null;
    public $to = null;
    public $adults = 0;
    public $children = 0;
    public $infants = 0;
    public $cabin = '';
    public $direct = false;
    public $date = '';
    public $timeDA = '';
    public $timeInterval = '';
    public $returnDate = '';
    public $returnTimeDA = '';
    public $returnTimeInterval = '';

    private $confTime = array(
        'M' => '0900',
        'A' => '1500',
        'E' => '2000',
        'N' => '0200',
    );
    private $confTimeWindow = array(
        'M' => '3',
        'A' => '3',
        'E' => '2',
        'N' => '4',
    );

    public function parseResponse($response) {
        //dump($response);
        $ret = array();
        if(is_array($response) && isset($response['flightIndex'])) {
            $ret['currency'] = $response['conversionRate']->conversionRateDetail->currency;
            $ret['seats'] = $this->getPassengerSeats();
            $ret['passengers'] = array(
                'ADT' => array('cnt' => $this->adults),
                'CH' => array('cnt' => $this->children),
                'INF' => array('cnt' => $this->infants),
            );
            $fba = self::parseFBA($response['serviceFeesGrp']);
            if(is_array($response['flightIndex'])) {
                //$fgroups = $response['flightIndex'];
                $ret['seg'][$response['flightIndex'][0]->requestedSegmentRef->segRef] = self::parseSegment($response['flightIndex'][0], $response['recommendation'], $fba[1]);
                $ret['seg'][$response['flightIndex'][1]->requestedSegmentRef->segRef] = self::parseSegment($response['flightIndex'][1], $response['recommendation'], $fba[2]);
                // if only one segment is returned in return flight we remove the only one segment
                foreach($ret['seg'] as $index => $seg) {
                    if($index == $response['flightIndex'][0]->requestedSegmentRef->segRef) $index2 = $response['flightIndex'][1]->requestedSegmentRef->segRef;
                    else $index2 = $response['flightIndex'][1]->requestedSegmentRef->segRef;
                    foreach($ret['seg'][$index] as $vari => $variant) {
                        if(! isset($ret['seg'][$index2][$vari])) unset($ret['seg'][$index][$vari]);
                    }
                }
                $ret['seg'][1]['locations'] = array_merge($ret['seg'][1]['locations'], $ret['seg'][2]['locations']);
                unset($ret['seg'][2]['locations']);
                $ret['seg'][1]['companies'] = array_merge($ret['seg'][1]['companies'], $ret['seg'][2]['companies']);
                unset($ret['seg'][2]['companies']);
            }
            else $ret['seg'][1] = self::parseSegment($response['flightIndex'], $response['recommendation'], $fba[1]);

            if(isset($ret['seg'][1]['locations'])) {
                $lObjs = ToureasonFlightDb::loadLocations($ret['seg'][1]['locations']);
                foreach($lObjs as $lObj) {
                    $ret['locations'][$lObj['iata_code']] = $lObj['name'];
                }
                unset($ret['seg'][1]['locations']);
            }

            if(isset($ret['seg'][1]['companies'])) {
                $cObjs = ToureasonFlightDb::loadCompanies($ret['seg'][1]['companies']);
                foreach($cObjs as $cObj) {
                    $ret['companies'][$cObj['iata_code']] = $cObj['name'];
                }
                unset($ret['seg'][1]['companies']);
            }

            if(isset($ret['seg'][2])) {
                if(count($ret['seg'][1]) > count($ret['seg'][2])) $ret['seg'][1] = array_slice($ret['seg'][1], 0, count($ret['seg'][2]), true);
                else if(count($ret['seg'][1]) < count($ret['seg'][2])) $ret['seg'][2] = array_slice($ret['seg'][2], 0, count($ret['seg'][1]), true);
            }

            // add fields for sorting
            foreach($ret['seg'][1] as $findex => $fgroup) {
                $ret['seg'][1][$findex][0]['durationTotal'] = self::getDurationTotal($ret['seg'][1][$findex][0]['duration']);
                $ret['seg'][1][$findex][0]['movesTotal'] = count($ret['seg'][1][$findex]);
                if(isset($ret['seg'][2])) {
                    $ret['seg'][1][$findex][0]['durationTotal'] = self::getDurationTotal($ret['seg'][1][$findex][0]['duration'], $ret['seg'][2][$findex][0]['duration']);
                    $ret['seg'][2][$findex][0]['durationTotal'] = $ret['seg'][1][$findex][0]['durationTotal'];
                    $ret['seg'][1][$findex][0]['movesTotal'] += count($ret['seg'][2][$findex]);
                    $ret['seg'][2][$findex][0]['movesTotal'] = $ret['seg'][1][$findex][0]['movesTotal'];
                }
                foreach($fgroup as $findex2 => $flight) {
                    if($findex2 > 0) {
                        $ret['seg'][1][$findex][$findex2]['durationTotal'] = $ret['seg'][1][$findex][0]['durationTotal'];
                        $ret['seg'][1][$findex][$findex2]['movesTotal'] = $ret['seg'][1][$findex][0]['movesTotal'];
                    }
                }
                if(isset($ret['seg'][2])) {
                    foreach($ret['seg'][2][$findex] as $findex2 => $flight) {
                        if($findex2 > 0) {
                            $ret['seg'][2][$findex][$findex2]['durationTotal'] = $ret['seg'][2][$findex][0]['durationTotal'];
                            $ret['seg'][2][$findex][$findex2]['movesTotal'] = $ret['seg'][2][$findex][0]['movesTotal'];
                        }
                    }
                }
            }
        }

        return $ret;
    }

    private static function getDurationTotal($d1, $d2='00:00') {
        return self::getTimeFromAirTime($d1) + self::getTimeFromAirTime($d2);
    }

    private static function getTimeFromAirTime($airTime) {
        list($hours, $minutes) = explode(':', $airTime);
        return intval($hours)*60 + intval($minutes);
    }

    private static function parseFBA($xml) {
        $fbagrp = array();
        if(! is_array($xml->freeBagAllowanceGrp)) $xml->freeBagAllowanceGrp = array($xml->freeBagAllowanceGrp);
        foreach($xml->freeBagAllowanceGrp as $fgrp) {
            $fbagrp[$fgrp->itemNumberInfo->itemNumberDetails->number] = self::obj2array($fgrp->freeBagAllownceInfo->baggageDetails);
        }

        $ret = array();
        if(! is_array($xml->serviceCoverageInfoGrp)) {
            $xml->serviceCoverageInfoGrp = array($xml->serviceCoverageInfoGrp);
        }
        foreach($xml->serviceCoverageInfoGrp as $fgrp) {
            if(! is_array($fgrp->serviceCovInfoGrp)) $fgrp->serviceCovInfoGrp = array($fgrp->serviceCovInfoGrp);
            foreach($fgrp->serviceCovInfoGrp as $segindex => $scig) {
                if(! is_array($scig->coveragePerFlightsInfo)) $scig->coveragePerFlightsInfo = array($segindex => $scig->coveragePerFlightsInfo);
                $ret[$scig->coveragePerFlightsInfo[$segindex]->numberOfItemsDetails->refNum][$fgrp->itemNumberInfo->itemNumber->number] = $fbagrp[$scig->refInfo->referencingDetail->refNumber];
            }
            // segment 2 is same if is not set earlier
            if(! isset($ret[2][$fgrp->itemNumberInfo->itemNumber->number])) $ret[2][$fgrp->itemNumberInfo->itemNumber->number] = $ret[1][$fgrp->itemNumberInfo->itemNumber->number];
        }

        return $ret;
    }

    private static function parseSegment($fsegment, $recommendation, $fba) {
        if(! is_array($recommendation)) $recommendation = array($recommendation);
        //dump($fsegment);
        //dump($recommendation);
        $ret = array();

        if(is_array($fsegment->groupOfFlights)) $fgarr = $fsegment->groupOfFlights;
        else $fgarr = array($fsegment->groupOfFlights);

        $prices = array();
        $cabins = array();
        $classes = array();
        $carriers = array();
        $baggages = array();
        foreach($recommendation as $rec1) {
            $recIndexes = array();
            $frefs = $rec1->segmentFlightRef;
            if(! is_array($frefs)) $frefs = array($frefs);
            foreach($frefs as $fref) {
                $rd = $fref->referencingDetail;
                if(! is_array($rd)) $rd = array($rd);
                foreach($rd as $rdet) {
                    if($rdet->refQualifier == 'S') $recIndex = $rdet->refNumber;
                    else if($rdet->refQualifier == 'B') $bagIndex = $rdet->refNumber;
                }
                $recIndexes[] = $recIndex;
                $baggages[$recIndex] = $bagIndex;
            }
            // cabins
            if(is_array($rec1->paxFareProduct)) $pfp = $rec1->paxFareProduct;
            else $pfp = array($rec1->paxFareProduct);
            if(is_array($pfp[0]->fareDetails)) $fDets = $pfp[0]->fareDetails;
            else $fDets = array($pfp[0]->fareDetails);
            if(is_array($fDets[0]->groupOfFares)) $gofs = $fDets[0]->groupOfFares;
            else $gofs = array($fDets[0]->groupOfFares);

            foreach($recIndexes as $recIndex) {
                foreach($gofs as $retIndex => $gof) {
                    $cabins[$recIndex][$retIndex] = $gof->productInformation->cabinProduct->cabin;
                    $classes[$recIndex][$retIndex] = $gof->productInformation->cabinProduct->rbd;
                }
                // prices
                $prices[$recIndex] = $rec1->recPriceInfo->monetaryDetail[0]->amount;
                $pfp = $rec1->paxFareProduct;
                if(! is_array($pfp)) $pfp = array($pfp);
                $csd = $pfp[0]->paxFareDetail->codeShareDetails;
                if(! is_array($csd)) $csd = array($csd);
                $carriers[$recIndex] = $csd[0]->company;
            }
        }

        $companies = array();
        foreach($fgarr as $flight) {
            $fi = $flight->propFlightGrDetail->flightProposal[0]->ref;
            if(is_array($flight->flightDetails)) $fdetails = $flight->flightDetails;
            else $fdetails = array($flight->flightDetails);

            foreach($fdetails as $fi2 => $fdetail) {

                $ret[$fi][$fi2]['flightId'] = $fi;
                $ret[$fi][$fi2]['duration'] = self::formatAirTime($flight->propFlightGrDetail->flightProposal[1]->ref);
                $ret[$fi][$fi2]['duration_simple'] = self::formatAirTime($fdetail->flightInformation->attributeDetails->attributeDescription);
                $ret[$fi][$fi2]['number'] = $fdetail->flightInformation->flightOrtrainNumber;
                $ret[$fi][$fi2]['company'] = $fdetail->flightInformation->companyId->marketingCarrier;
                $ret['companies'][] = $ret[$fi][$fi2]['company'];
                if(isset($baggages[$fi])) {
                    $ret[$fi][$fi2]['carrier'] = $carriers[$fi];
                    $ret[$fi][$fi2]['priceOrig'] = isset($prices[$fi]) ? $prices[$fi] : $ret[$fi][0]['priceOrig'];
                    $ret[$fi][$fi2]['price'] = self::formatAirPrice(self::getToureasonPrice($ret[$fi][$fi2]['priceOrig']));
                    $ret[$fi][$fi2]['cabin'] = isset($cabins[$fi][$fi2]) ? $cabins[$fi][$fi2] : $ret[$fi][0]['cabin'];
                    $ret[$fi][$fi2]['class'] = isset($classes[$fi][$fi2]) ? $classes[$fi][$fi2] : $ret[$fi][0]['class'];
                    $ret[$fi][$fi2]['fba'] = $fba[$baggages[$fi]];
                }
                $ret[$fi][$fi2]['depDateOrig'] = $fdetail->flightInformation->productDateTime->dateOfDeparture;
                $ret[$fi][$fi2]['depDate'] = self::formatAirDate($fdetail->flightInformation->productDateTime->dateOfDeparture);
                $ret[$fi][$fi2]['depTime'] = self::formatAirTime($fdetail->flightInformation->productDateTime->timeOfDeparture);
                $ret[$fi][$fi2]['arrDate'] = self::formatAirDate($fdetail->flightInformation->productDateTime->dateOfArrival);
                $ret[$fi][$fi2]['arrTime'] = self::formatAirTime($fdetail->flightInformation->productDateTime->timeOfArrival);
                foreach($fdetail->flightInformation->location as $i => $loc) {
                    $ret[$fi][$fi2]['loc'][$i]['id'] = $loc->locationId;
                    $ret['locations'][] = $loc->locationId;
                    $ret[$fi][$fi2]['loc'][$i]['terminal'] = (isset($loc->terminal) ? $loc->terminal : '');
                }
            }
        }

        return $ret;
    }

    private static function formatAirDate($airDate) {
        return substr($airDate, 0, 2) . '.' . substr($airDate, 2, 2) . '.20' . substr($airDate, 4, 2);
    }

    private static function formatAirTime($airTime) {
        return substr($airTime, 0, 2) . ':' . substr($airTime, 2, 2);
    }

    private static function formatAirPrice($airPrice) {
        return number_format($airPrice, 0, ',', ' ');
    }

    // add percentage and convert CZK to EUR
    public static function getToureasonPrice($airPrice) {
        return ceil($airPrice / floatval(Configuration::get('TOUREASON_EXCHANGE_RATE_EUR2CZK')) * (100 + floatval(Configuration::get('TOUREASON_FLIGHT_PERC'))) / 100);
    }

    protected function setHeaders() {
        $this->headers[] = new WsseAuthHeader($this->login, $this->passwd);
        parent::setHeaders();
    }

    protected function prepareArgs() {

        $args[] =
            $this->createAVar('numberOfUnit', array(
                $this->addUnitNumberDetail($this->getPassengerSeats(), 'PX'),
                $this->addUnitNumberDetail(Configuration::get('TOUREASON_FLIGHT_SEARCH_CNT'), 'RC'),
            ));

        if($this->adults > 0) $args[] = $this->addPax('ADT', $this->adults);
        if($this->children > 0) $args[] = $this->addPax('CH', $this->children, $this->adults);
        if($this->infants > 0) $args[] = $this->addPax('INF', $this->infants);

        $args[] =
            $this->createAVar('fareOptions', array(
                $this->createAVar('pricingTickInfo', array(
                    $this->createAVar('pricingTicketing', array(
                        $this->createSVar('priceType', 'RU'),
                        $this->createSVar('priceType', 'RP'),
                        $this->createSVar('priceType', 'TAC'),
                    ))
                )),
                $this->createAVar('formOfPayment', array(
                    $this->createAVar('formOfPaymentDetails', array(
                        $this->createSVar('type', 'CC'),
                        $this->createSVar('creditCardNumber', substr(Configuration::get('TOUREASON_CC_NUMBER'), 0, 6))
                    ))
                ))
            ));

        $tfinfo = array();
        if($this->cabin) $tfinfo[] = $this->createAVar('cabinId', array($this->createSVar('cabin', $this->cabin)));
        $flightTypes = array(
                $this->createSVar('flightType', 'D'),
                $this->createSVar('flightType', 'N'),
        );
        if(! $this->direct) $flightTypes[] = $this->createSVar('flightType', 'C');
        $tfinfo[] = $this->createAVar('flightDetail', $flightTypes);
        $args[] = $this->createAVar('travelFlightInfo', $tfinfo);

        $args[] = $this->addItinerary(1);

        if($this->returnDate) $args[] = $this->addItinerary(2);

        return $args;
    }

    private function getPassengerSeats() {
        return $this->adults + $this->children;
    }

    private function addUnitNumberDetail($num, $type) {
        return $this->createAVar('unitNumberDetail', array(
            $this->createSVar('numberOfUnits', $num),
            $this->createSVar('typeOfUnit', $type)
        ));
    }

    private function addPax($type, $cnt, $offset = 0) {
        $paxs = array($this->createSVar('ptc', $type));
        for($i = $offset; $i < ($cnt + $offset); $i++) {
            $traveller = array($this->createSVar('ref', $i + 1));
            if($type == 'INF') $traveller[] = $this->createSVar('infantIndicator', 1);
            $paxs[] = $this->createAVar('traveller', $traveller);
        }

        return $this->createAVar('paxReference', $paxs);
    }

    private function addItinerary($ref) {
        return
            $this->createAVar('itinerary', array(
                $this->createAVar('requestedSegmentRef', array(
                    $this->createSVar('segRef', $ref)
                )),
                $this->createAVar('departureLocalization', array(
                    $this->createAVar('departurePoint', array(
                        $this->createSVar('locationId', ($ref == 1 ? $this->from : $this->to))
                    ))
                )),
                $this->createAVar('arrivalLocalization', array(
                    $this->createAVar('arrivalPointDetails', array(
                        $this->createSVar('locationId', ($ref == 1 ? $this->to : $this->from))
                    ))
                )),
                $this->createAVar('timeDetails', array(
                    $this->createAVar('firstDateTimeDetail', $this->getFlightDt($ref))
                ))
            ));
    }

    private function getFlightDt($ref) {
        $ret = array();

        if(($ref == 1) && ($this->timeDA)) $ret[] = $this->createSVar('timeQualifier', $this->timeDA);
        if(($ref == 2) && ($this->returnTimeDA)) $ret[] = $this->createSVar('timeQualifier', $this->timeDA);

        $ret[] = $this->createSVar('date', ($ref == 1 ? $this->date : $this->returnDate));

        if(($ref == 1) && ($this->timeInterval)) {
            $ret[] = $this->createSVar('time', $this->confTime[$this->timeInterval]);
            $ret[] = $this->createSVar('timeWindow', $this->confTimeWindow[$this->timeInterval]);
        }
        if(($ref == 2) && ($this->returnTimeInterval)) {
            $ret[] = $this->createSVar('time', $this->confTime[$this->returnTimeInterval]);
            $ret[] = $this->createSVar('timeWindow', $this->confTimeWindow[$this->returnTimeInterval]);
        }

        return $ret;
    }

}
