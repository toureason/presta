<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/SoapClientToureason.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');

class ToureasonFlightSell extends SoapClientToureason {

    protected $soapURI = 'http://xml.amadeus.com/ITAREQ_05_2_IA';
    protected $soapAction = 'http://webservices.amadeus.com/ITAREQ_05_2_IA';
    protected $method = 'Air_SellFromRecommendation';
    protected $debug = false;

    public $fgroup = array();
    public $passengers = array();


    public function parseResponse($response) {
        //dump($response);

        $idet = $response['itineraryDetails'];
        if(! is_array($idet)) $idet = array($idet);
        foreach($idet as $seg) {
            $si = $seg->segmentInformation;
            if(! is_array($si)) $si = array($si);
            foreach($si as $flight) {
                if($flight->actionDetails->statusCode != 'OK') {
                    $this->logError($flight->actionDetails->statusCode);
                    return false;
                }
            }
        }
        if(isset($response['errorAtMessageLevel'])) {
            $this->logError($response['errorAtMessageLevel']['errorSegment']['errorDetails']['errorCode']);
            return false;
        }

        return true;
    }

    protected function setHeaders() {
        if(isset($this->session)) {
            $this->headers[] = new SessionSequenceHeader($this->session);
        }
        else {
            $this->headers[] = new WsseAuthHeader($this->login, $this->passwd);
            $this->headers[] = new SessionStartHeader();
        }
        parent::setHeaders();
    }

    protected function prepareArgs() {
        $seats = $this->passengers['ADT']['cnt'] + $this->passengers['CH']['cnt'];

        $args[] =
            $this->createAVar('messageActionDetails', array(
                $this->createAVar('messageFunctionDetails', array(
                    $this->createSVar('messageFunction', 183),
                    $this->createSVar('additionalMessageFunction', 'M1')
                ))
            ));

        foreach($this->fgroup as $seg => $fgroup) {
            $seg = array();
            foreach($fgroup as $flid => $flight) {
                $seg[] =
                    $this->createAVar('segmentInformation', array(
                        $this->createAVar('travelProductInformation', array(
                            $this->createAVar('flightDate', array(
                                $this->createSVar('departureDate', $flight['depDateOrig'])
                            )),
                            $this->createAVar('boardPointDetails', array(
                                $this->createSVar('trueLocationId', $flight['loc'][0]['id'])
                            )),
                            $this->createAVar('offpointDetails', array(
                                $this->createSVar('trueLocationId', $flight['loc'][1]['id'])
                            )),
                            $this->createAVar('companyDetails', array(
                                $this->createSVar('marketingCompany', $flight['company'])
                            )),
                            $this->createAVar('flightIdentification', array(
                                $this->createSVar('flightNumber', $flight['number']),
                                $this->createSVar('bookingClass', $flight['class'])
                            ))
                        )),
                        $this->createAVar('relatedproductInformation', array(
                            $this->createSVar('quantity', $seats),
                            $this->createSVar('statusCode', 'NN')
                        ))
                    ));
            }
            $args[] =
                $this->createAVar('itineraryDetails', array_merge(
                    array(
                        $this->createAVar('originDestinationDetails', array(
                            $this->createSVar('origin', $fgroup[0]['loc'][0]['id']),
                            $this->createSVar('destination', $fgroup[(count($fgroup)-1)]['loc'][1]['id'])
                        )),
                        $this->createAVar('message', array(
                            $this->createAVar('messageFunctionDetails', array(
                                $this->createSVar('messageFunction', 183)
                            )),
                        ))
                    ),
                    $seg
                ));
        }

        return $args;
    }


}
