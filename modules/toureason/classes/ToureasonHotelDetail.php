<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/SoapClientToureason.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');

class ToureasonHotelDetail extends SoapClientToureason {

    protected $soapURI = 'http://www.opentravel.org/OTA/2003/05';
    protected $soapAction = 'http://webservices.amadeus.com/OTA_HotelDescriptiveInfoRQ_07.1_1A2007A';
    protected $method = 'OTA_HotelDescriptiveInfoRQ';
    protected $methodReply = 'OTA_HotelDescriptiveInfoRS';
    protected $debug = false;

    public $id = '';


    public function __doRequest($request, $location, $action, $version, $one_way = 0) {
        $request = str_replace('<ns1:OTA_HotelDescriptiveInfoRQ', '<ns1:OTA_HotelDescriptiveInfoRQ xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.opentravel.org/OTA/2003/05" EchoToken="PartialWithParsing" PrimaryLangID="en" Version="6.001"', $request);
        $request = str_replace('ns1:OTA_HotelDescriptiveInfoRQ', 'OTA_HotelDescriptiveInfoRQ', $request);
        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }

    protected function setHeaders() {
        $this->headers[] = new WsseAuthHeader($this->login, $this->passwd);
        parent::setHeaders();
    }

    public function parseResponse() {
        $respObject = $this->getResponse();
        $pomd = [];
        $ret['img'] = '';
        $ret['thumb'] = '';
        $ret['award'] = [];
        $ret['stars'] = '0';

        if(isset($respObject->HotelDescriptiveContents->HotelDescriptiveContent)) {
            $pomhdc = $respObject->HotelDescriptiveContents->HotelDescriptiveContent;
            if(isset($pomhdc->AffiliationInfo->Awards)) {
                foreach($pomhdc->AffiliationInfo->Awards as $award) {
                    $ret['award'][] = array(
                        'provider' => strval($award->Award['Provider']),
                        'rating' => strval($award->Award['Rating']),
                        'symbol' => isset($award->Award['RatingSymbol']) ? strval($award->Award['RatingSymbol']) : ''
                    );
                    if($award->Award['Provider'] == 'Local Star Rating') {
                        $ret['stars'] = strval($award->Award['Rating']);
                    }
                }
            }
            if(isset($pomhdc->HotelInfo->Descriptions->MultimediaDescriptions->MultimediaDescription)) {
                $md = $pomhdc->HotelInfo->Descriptions->MultimediaDescriptions->MultimediaDescription;
                foreach($md as $desc) {
                    if(isset($desc->TextItems->TextItem->Description))
                        $pomd[] = strval($desc->TextItems->TextItem->Description);
                    if(isset($desc->ImageItems->ImageItem->ImageFormat)) {
                        $ret['img'] = strval($desc->ImageItems->ImageItem->ImageFormat[0]->URL);
                        $ret['thumb'] = strval($desc->ImageItems->ImageItem->ImageFormat[2]->URL);
                    }
                }
            }
        }
        $ret['desc'] = implode(' ', $pomd);
        //var_dump($respObject);

        return $ret;
    }

    protected function prepareArgs() {
        $arg[] = new SoapVar('
            <HotelDescriptiveInfos>
                <HotelDescriptiveInfo HotelCode="'.$this->id.'">
                    <HotelInfo SendData="true"/>
                    <FacilityInfo SendMeetingRooms="true" SendGuestRooms="true" SendRestaurants="true"/>
                    <Policies SendPolicies="true"/>
                    <AreaInfo SendRefPoints="false" SendAttractions="false" SendRecreations="true"/>
                    <AffiliationInfo SendLoyalPrograms="false" SendAwards="true"/>
                    <ContactInfo SendData="false"/>
                    <MultimediaObjects SendData="true"/>
                    <ContentInfos>
                        <ContentInfo Name="SecureMultimediaURLs"/>
                    </ContentInfos>
                </HotelDescriptiveInfo>
            </HotelDescriptiveInfos>',
            XSD_ANYXML);

        return $arg;
    }

}
