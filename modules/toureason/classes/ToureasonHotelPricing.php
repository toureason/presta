<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/SoapClientToureason.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');

class ToureasonHotelPricing extends SoapClientToureason {

    protected $soapURI = 'http://www.opentravel.org/OTA/2003/05';
    protected $soapAction = 'http://webservices.amadeus.com/Hotel_EnhancedPricing_2.0';
    protected $method = 'OTA_HotelAvailRQ';
    protected $methodReply = 'OTA_HotelAvailRS';
    protected $debug = false;


    public function __doRequest($request, $location, $action, $version, $one_way = 0) {
        $request = str_replace('<ns1:OTA_HotelAvailRQ', '<ns1:OTA_HotelAvailRQ xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.opentravel.org/OTA/2003/05" EchoToken="Pricing" Version="4.000" AvailRatesOnly="true" PrimaryLangID="EN" RequestedCurrency="EUR" SummaryOnly="false" RateRangeOnly="false"', $request);
        $request = str_replace('ns1:OTA_HotelAvailRQ', 'OTA_HotelAvailRQ', $request);
        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }

    protected function setHeaders() {
        $this->headers[] = new SessionSequenceHeader($this->session);
        parent::setHeaders();
    }

    public function parseResponse() {
        $respObject = $this->getResponse();
        //dump($respObject);

        if (isset($respObject->Errors)) {
            if(isset($respObject->Errors[0]->Error[0])) {
                $this->logError('Error type: ' . $xmlErrors[0]->Error[0]['Type'] . ' Code: ' . $xmlErrors[0]->Error[0]['Code'] . ' Status: ' . $xmlErrors[0]->Error[0]['Status']);
            }
            throw new Exception(Tools::displayError('Check price error.'));
        }

        $rooms = [];
        if(isset($respObject->RoomStays)) {
            $rateConversion = (isset($respObject->CurrencyConversions) ? $respObject->CurrencyConversions[0]->CurrencyConversion[0]['RateConversion'] : 1);
            foreach($respObject->RoomStays->children() as $xmlRoom) {
                $rooms = array_merge($rooms, ToureasonHotelSearch::addRoom($xmlRoom, $rateConversion));
            }
        }

        $trusted = 0;
        foreach($rooms as $room) {
            if(isset($this->selection[$room['BookingCode']])) {
                if($this->selection[$room['BookingCode']]['room']['TotalPrice'] == $room['TotalPrice']) $trusted++;
            }
        }
        if($trusted < count($this->selection)) {
            $this->logError('Room prices are not trusted.' . $trusted . 'x' . count($this->selection));
        }

        return $trusted;
    }

    protected function prepareArgs() {
        $ret = '<AvailRequestSegments>';
        $iter = 1;
        foreach($this->selection as $sel) {
            $ret .= '
                <AvailRequestSegment>
                    <HotelSearchCriteria>
                        <Criterion ExactMatch="true">
                            <HotelRef HotelCityCode="' . $sel['HotelCityCode'] . '" ChainCode="' . $sel['ChainCode'] . '" HotelCode="' . $sel['HotelCode'] . '"></HotelRef>
                            <StayDateRange Start="' . $this->getDateNormal($this->passengers['start_date']) . '" End="' . $this->getDateNormal($this->passengers['end_date']) . '"></StayDateRange>
                            <RatePlanCandidates>
                                <RatePlanCandidate RatePlanCode="' . $sel['room']['RatePlanCode'] . '"/>
                            </RatePlanCandidates>
                            <RoomStayCandidates>
                                <RoomStayCandidate BookingCode="' . $sel['room']['BookingCode'] . '" RoomTypeCode="' . $sel['room']['RoomTypeCode'] . '" Quantity="' . $sel['room']['cnt'] . '" RoomID="' . $iter++ . '">
                                    <GuestCounts IsPerRoom="false">
                                        <GuestCount AgeQualifyingCode="10" Count="' . $this->passengers['cnt'] . '"></GuestCount>
                                    </GuestCounts>
                                </RoomStayCandidate>
                            </RoomStayCandidates>
                        </Criterion>
                    </HotelSearchCriteria>
                </AvailRequestSegment>';
        }
        $ret .= '</AvailRequestSegments>';

        return [new SoapVar($ret, XSD_ANYXML)];
    }

}
