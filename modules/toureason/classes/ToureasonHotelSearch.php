<?php
require_once(_PS_MODULE_DIR_ . 'toureason/classes/SoapClientToureason.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/SoapHeaders.php');

/**
 * Class for searching of free hotels via IATA code from AMADEUS SOAP
 */
class ToureasonHotelSearch extends SoapClientToureason
{

    public $moreIndicator = NULL;

    public $city = NULL;

    public $start_date = NULL;

    public $end_date = NULL;

    public $count_rooms = 0;

    public $adults = 0;

    public $children = 0;

    public $hotelCode = '';

    public $hotelsMatchedCnt = 0;

    protected $soapURI = 'http://www.opentravel.org/OTA/2003/05';

    protected $soapAction = 'http://webservices.amadeus.com/Hotel_MultiSingleAvailability_10.0';

    protected $method = 'OTA_HotelAvailRQ';

    protected $methodReply = 'OTA_HotelAvailRS';

    protected $debug = FALSE;

    protected $otaAgeQualifyingCode = [    // from OT code list sources
        "7" => "Infant",
        "8" => "Child",
        "9" => "Teenager",
        "10" => "Adult",
        "11" => "Senior ",
        "12" => "Additional occupant with adult",
        "13" => "Additional occupant without adult",
        "14" => "Free child",
        "15" => "Free adult"
    ];

    protected $bedTypeCode = [
        "1" => "Double",
        "3" => "King",
        "5" => "Queen",
        "8" => "Twins",
        "9" => "Single"
    ];

    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        $request = str_replace('<ns1:OTA_HotelAvailRQ',
            '<ns1:OTA_HotelAvailRQ ' .
            'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' .
            'EchoToken="MultiSingle" ' .
            'SummaryOnly="true" ' .
            'RateRangeOnly="true" ' .
            'RateDetailsInd="true" ' .
            'MaxResponses="30" ' .
            'SearchCacheLevel="VeryRecent" ' .
            'Version="4.000" ' .
            'PrimaryLangID="EN" ' .
            'RequestedCurrency="EUR" ' .
            'AvailRatesOnly="true" ' .
            'ExactMatchOnly="false" ' .
            'SortOrder="RA" '.
            'xmlns="http://www.opentravel.org/OTA/2003/05"',
            $request);
        $request = str_replace('ns1:OTA_HotelAvailRQ', 'OTA_HotelAvailRQ', $request);
        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }

    static function cmp1($a, $b)
    {
        if ($a['TotalPrice'] == $b['TotalPrice']) {
            return 0;
        }
        return ((int)$a['TotalPrice'] < (int)$b['TotalPrice']) ? -1 : 1;
    }

    public function parseResponse()
    {
        $respObject = $this->getResponse();
        //var_dump($respObject);

        foreach($respObject->children() as $second) {
            if ($second->getName() == 'Success') {
                $xmlSuccess = $second;
            }
            if ($second->getName() == 'Errors') {
                $xmlErrors = $second;
            }
            if ($second->getName() == 'Warnings') {
                $xmlWarnings = $second;
            }
            if ($second->getName() == 'HotelStays') {
                $xmlHotelStays = $second;
            }
            if ($second->getName() == 'HotelStay') {    // if is only one hotel
                $xmlHotelStay = $second;
            }
            if ($second->getName() == 'RoomStays') {
                $xmlRoomStays = $second;
            }
            if ($second->getName() == 'CurrencyConversions') {
                $xmlCurrencyConversions = $second;
            }

        }
        if (isset($xmlErrors)) {
            if(isset($xmlErrors[0]->Error[0])) {
                $this->logError('Error type: ' . $xmlErrors[0]->Error[0]['Type'] . ' Code: ' . $xmlErrors[0]->Error[0]['Code'] . ' Status: ' . $xmlErrors[0]->Error[0]['Status']);
            }
            throw new Exception(Tools::displayError('Search error.'));
        } elseif (!isset($xmlHotelStays) && !isset($xmlHotelStay)) {
            return [];
        } else {
            $rooms = [];
            if (isset($xmlRoomStays)) {
                $rateConversion = (isset($xmlCurrencyConversions) ? $xmlCurrencyConversions->CurrencyConversion[0]['RateConversion'] : 1);
                foreach($xmlRoomStays->children() as $xmlRoom) {
                    $rooms = array_merge($rooms, self::addRoom($xmlRoom, $rateConversion));
                }
            }
            $hotels = [];
            if (count($rooms) > 0) {
                if (isset($xmlHotelStays)) {
                    foreach($xmlHotelStays->children() as $xmlHotel) {
                        $ht = $this->addHotel($xmlHotel, $rooms);
                        if($ht) $hotels[] = $ht;
                    }
                } else {
                    $ht = $this->addHotel($xmlHotelStay, $rooms);
                    if($ht) $hotels[] = $ht;
                }
            }
            $this->hotelsMatchedCnt += count($hotels);
            if(isset($xmlRoomStays['MoreIndicator']) && ($this->hotelsMatchedCnt <= Configuration::get('TOUREASON_HOTEL_SEARCH_CNT'))) {
                // recursive calling of service while moreindicator is present
                $client = new ToureasonHotelSearch();
                $client->moreIndicator = (string)$xmlRoomStays['MoreIndicator'];
                $client->city = $this->city;
                $client->start_date = $this->start_date;
                $client->end_date = $this->end_date;
                $client->adults = $this->adults;
                $client->children = $this->children;
                $client->count_rooms = $this->count_rooms;
                $client->hotelsMatchedCnt = $this->hotelsMatchedCnt;
                $ret = $client->process();
                $hotels = array_merge($hotels, $client->parseResponse($ret));
            }
            return $hotels;
        }
    }

    public static function addRoom(SimpleXMLElement $room, $rateConversion)
    {
        if (!isset($room['AvailabilityStatus'])) {
            return [];
        }

        // check available credit card type to pay
        $gPay = $room->RatePlans[0]->RatePlan[0]->Guarantee[0];
        $ccardFound = false;
        if((string)$gPay['GuaranteeType'] == 'Deposit') {
            $gacc = $gPay->GuaranteesAccepted[0];
            if(isset($gacc) && isset($gacc->GuaranteeAccepted)) {
                foreach($gacc->GuaranteeAccepted as $gnode) {
                    if((string)$gnode->PaymentCard[0]['CardCode'] == Configuration::get('TOUREASON_CC_TYPE')) {
                        $ccardFound = true;
                    }
                }
            }
        }
        if(! $ccardFound) return [];

        $keys = explode(' ', $room['RPH']);
        $roomProperties['AvailabilityStatus'] = strval($room['AvailabilityStatus']);
        //$roomProperties['InfoSource']=$room['InfoSource'];    // Used to specify the source of the data being exchanged as
        //determined by trading partners.
        //1A for Amadeus
        //Chaincode for Provider
        //$roomProperties['SourceOfBusiness']=isset($room['SourceOfBusiness'])?$room['SourceOfBusiness']:'';    //Returning general information about the booking, the pricing,
        //... e.g 1-Step, 2-Step. see User Guided for detailed
        if (isset($room->RoomRates[0]->RoomRate[0]->Total[0]['AmountAfterTax'])) {
            $price = self::getToureasonPrice(floatval((string)$room->RoomRates[0]->RoomRate[0]->Total[0]['AmountAfterTax']) * floatval($rateConversion));
        }
        // if price is unknown we won't offer
        else return [];

        $roomProperties['RoomTypeCode'] = isset($room->RoomTypes[0]->RoomType[0]['RoomTypeCode']) ? strval($room->RoomTypes[0]->RoomType[0]['RoomTypeCode']) : '';
        $roomProperties['RoomType'] = isset($room->RoomTypes[0]->RoomType[0]['RoomType']) ? strval($room->RoomTypes[0]->RoomType[0]['RoomType']) : '';    // using for own elimination od rows
        $roomProperties['BookingCode'] = isset($room->RoomRates[0]->RoomRate[0]['BookingCode']) ? strval($room->RoomRates[0]->RoomRate[0]['BookingCode']) : '';
        $roomProperties['RatePlanCode'] = isset($room->RoomRates[0]->RoomRate[0]['RatePlanCode']) ? strval($room->RoomRates[0]->RoomRate[0]['RatePlanCode']) : '';
        $roomProperties['NumberOfUnits'] = isset($room->RoomRates[0]->RoomRate[0]['NumberOfUnits']) ? strval($room->RoomRates[0]->RoomRate[0]['NumberOfUnits']) : '';
        $roomProperties['TotalPrice'] = isset($price) ? $price : '';
        $roomProperties['RoomInfo'] = '';
        if (isset($room->RoomRates[0]->RoomRate[0]->RoomRateDescription[0])) {
            foreach($room->RoomRates[0]->RoomRate[0]->RoomRateDescription[0]->Text as $key => $val) {
                $roomProperties['RoomInfo'] .= $val;
            }
        }
        $roomProperties['Count'] = 1;
        foreach($keys as $valK) {
            $respons[$valK] = $roomProperties;
        }
        return $respons;
    }

    public static function getToureasonPrice($price)
    {
        return ceil($price * (100 + floatval(Configuration::get('TOUREASON_HOTEL_PERC'))) / 100);
    }

    protected function addHotel(SimpleXMLElement $hotel, $rooms)
    {
        $respons = [];
        $respons['RoomStayRPH'] = explode(' ', $hotel['RoomStayRPH']);
        $respons['Rooms'] = [];
        foreach($respons['RoomStayRPH'] as $rph) {
            if(isset($rooms[$rph])) {
                $keyrooms = $rooms[$rph]['RoomType'] . $rooms[$rph]['TotalPrice'];
                if (array_key_exists($keyrooms, $respons['Rooms'])) {
                    $respons['Rooms'][$keyrooms]['Count'] = $respons['Rooms'][$keyrooms]['Count'] + 1;
                } else {
                    $respons['Rooms'][$keyrooms] = $rooms[$rph];
                }
            }
        }
        if(count($respons['Rooms']) == 0) return false;
        usort($respons['Rooms'], ["ToureasonHotelSearch", "cmp1"]);
        $respons['PriceFrom'] = $respons['Rooms'][0]['TotalPrice'];
        $respons['HotelCode'] = strval($hotel->BasicPropertyInfo[0]['HotelCode']);
        $respons['ChainCode'] = strval($hotel->BasicPropertyInfo[0]['ChainCode']);
        $respons['HotelCityCode'] = strval($hotel->BasicPropertyInfo[0]['HotelCityCode']);
        $respons['HotelName'] = strval($hotel->BasicPropertyInfo[0]['HotelName']);
        $respons['Address'] = $hotel->BasicPropertyInfo[0]->Address[0]->AddressLine[0] . ', ' . $hotel->BasicPropertyInfo[0]->Address[0]->CityName;
        if (isset($hotel->BasicPropertyInfo[0]->VendorMessages[0]->VendorMessage[0]->SubSection[0]->Paragraph[0]->Text[0])) {
            $respons['Notice'] = '';
            foreach($hotel->BasicPropertyInfo[0]->VendorMessages[0]->VendorMessage[0]->SubSection[0]->Paragraph[0]->Text as $valCom) {
                if (isset($valCom[0])) {
                    $respons['Notice'] = $respons['Notice'] . $valCom[0];
                } elseif (isset($valCom)) {
                    $respons['Notice'] = $respons['Notice'] . $valCom;
                }
            }
        }
        return $respons;
    }

    public function getSelection($resFromFormPost)
    {
        $reservation = [];
        foreach($resFromFormPost as $key => $val) {
            if (substr($key, 0, 4) == 'room' && intval($val) > 0) {
                $explod = explode('__', $key);
                $explod2 = explode('__', $resFromFormPost['hotel__' . $explod[1] . '__' . $explod[2]]);
                $reservation[] = ['HotelCode' => $explod[1], 'RoomBookingCode' => $explod[2], 'Count' => $val, 'CountBedInRoom' => $explod[3], 'TotalPrice' => $explod2[2], 'hotelName' => $explod2[0], 'hotelAddress' => $explod2[1]];
            }
        }
        return $reservation;
    }

    protected function setHeaders()
    {
        $this->headers[] = new WsseAuthHeader($this->login, $this->passwd);
        parent::setHeaders();
    }

    protected function prepareArgs()
    {
        $arg[] = $this->getAvailRequestSegments();
        return $arg;
    }

    /**
     * Body for OTA_HotelAvailRQ
     * AlternateAvailability="Never" - remove duplication in select
     * <HotelRef HotelCityCode="LON"/> - it means IATA mod of selection
     *
     * @return \SoapVar
     */
    protected function getAvailRequestSegments()
    {
        $vysledek = new SoapVar('<AvailRequestSegments>' .
            '<AvailRequestSegment InfoSource="Multisource"' . (isset($this->moreIndicator) ? (' MoreDataEchoToken="' . $this->moreIndicator . '"') : '') . '>' .
            '<HotelSearchCriteria AvailableOnlyIndicator="false">' .
            '<Criterion ExactMatch="true" AlternateAvailability="Never">' .
            '<HotelRef HotelCityCode="' . $this->city . '"></HotelRef>' .
            '<StayDateRange Start="' . $this->getDateNormal($this->start_date) . '" End="' . $this->getDateNormal($this->end_date) . '"></StayDateRange>' .
            '<RoomStayCandidates>' .
            '<RoomStayCandidate RoomID="1">' .
            '<GuestCounts IsPerRoom="false">' .    // IsPerRoom=True, all guests share the same room
            ($this->adults > 0 ? '<GuestCount AgeQualifyingCode="10" Count="' . $this->adults . '"></GuestCount>' : '') .
            //($this->children>0?'<GuestCount AgeQualifyingCode="7" Count="'.$this->children.'"></GuestCount>':'').
            '</GuestCounts>' .
            '</RoomStayCandidate>
              </RoomStayCandidates>
            </Criterion>
          </HotelSearchCriteria>
        </AvailRequestSegment>
      </AvailRequestSegments>', XSD_ANYXML);
        return $vysledek;
    }

}
