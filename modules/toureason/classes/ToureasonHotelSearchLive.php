<?php
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonHotelSearch.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');


class ToureasonHotelSearchLive extends ToureasonHotelSearch {

    public $selection = array();
    public $passengers = array();

    protected $soapURI = 'http://www.opentravel.org/OTA/2003/05';
    protected $soapAction = 'http://webservices.amadeus.com/Hotel_MultiSingleAvailability_10.0';
    protected $method = 'OTA_HotelAvailRQ';
    protected $methodReply = 'OTA_HotelAvailRS';

    protected $debug = FALSE;

    public function __doRequest($request, $location, $action, $version, $one_way = 0) {
        $request = str_replace('<ns1:OTA_HotelAvailRQ',
            '<ns1:OTA_HotelAvailRQ ' .
            'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' .
            'EchoToken="MultiSingle" ' .
            'SummaryOnly="true" ' .
            'RateRangeOnly="true" ' .
            'RateDetailsInd="true" ' .
            'SearchCacheLevel="Live" ' .
            'Version="4.000" ' .
            'PrimaryLangID="EN" ' .
            'RequestedCurrency="EUR" ' .
            'AvailRatesOnly="true" ' .
            'ExactMatchOnly="false" ' .
            'xmlns="http://www.opentravel.org/OTA/2003/05"',
            $request);
        $request = str_replace('ns1:OTA_HotelAvailRQ', 'OTA_HotelAvailRQ', $request);
        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }

    protected function getAvailRequestSegments() {
        //dump($this->selection);
        $ret = '<AvailRequestSegments>';
        $iter = 1;
        foreach($this->selection as $sel) {
            $ret .= '<AvailRequestSegment InfoSource="Distribution">' .
            '<HotelSearchCriteria AvailableOnlyIndicator="true">' .
            '<Criterion ExactMatch="true" AlternateAvailability="Never">' .
            '<HotelRef HotelCityCode="' . $sel['HotelCityCode'] . '" ChainCode="' . $sel['ChainCode'] . '" HotelCode="' . $sel['HotelCode'] . '"></HotelRef>' .
            '<StayDateRange Start="' . $this->getDateNormal($this->passengers['start_date']) . '" End="' . $this->getDateNormal($this->passengers['end_date']) . '"></StayDateRange>' .
            '<RoomStayCandidates>'.
            '<RoomStayCandidate Quantity="' . $sel['room']['cnt'] . '" RoomID="' . $iter++ . '">' .
            '<GuestCounts IsPerRoom="false">' .
            '<GuestCount AgeQualifyingCode="10" Count="' . $this->passengers['cnt'] . '"></GuestCount>' .
            '</GuestCounts>
                </RoomStayCandidate>
              </RoomStayCandidates>
            </Criterion>
          </HotelSearchCriteria>
        </AvailRequestSegment>';
        }
        $ret .= '</AvailRequestSegments>';

        return new SoapVar($ret, XSD_ANYXML);
    }

    public function parseResponse() {
        $available = 0;
        $resp = parent::parseResponse();
        if(count($resp) == 0) {
            $this->logError('Rooms are not available in LIVE search. No rooms.');
            return $available;
        }
        //dump($this->selection);
        //dump($resp);

        if (isset($respObject->Errors)) {
            if(isset($respObject->Errors[0]->Error[0])) {
                $this->logError('Error type: ' . $xmlErrors[0]->Error[0]['Type'] . ' Code: ' . $xmlErrors[0]->Error[0]['Code'] . ' Status: ' . $xmlErrors[0]->Error[0]['Status']);
            }
            throw new Exception(Tools::displayError('Checking availability error.'));
        }

        foreach($resp[0]['Rooms'] as $room) {
            if(isset($this->selection[$room['BookingCode']])) {
                if($this->selection[$room['BookingCode']]['room']['cnt'] <= $room['Count']) $available++;
            }
        }
        if($available < count($this->selection)) {
            $this->logError('Rooms are not available in LIVE search.' . $available . 'x' . count($this->selection));
        }

        return $available;
    }

    protected function setHeaders() {
        $this->headers[] = new SessionStartHeader();
        parent::setHeaders();
    }
}
