<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/SoapClientToureason.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');

class ToureasonHotelSell extends SoapClientToureason {

    protected $soapURI = 'http://webservices.amadeus.com/HBKRCQ_13_7_1A';
    protected $soapAction = 'http://webservices.amadeus.com/HBKRCQ_13_7_1A';
    protected $method = 'Hotel_Sell';
    protected $debug = false;

    public $selection = array();
    public $passengers = array();

    protected function setHeaders() {
        $this->headers[] = new SessionSequenceHeader($this->session);
        parent::setHeaders();
    }

    public function parseResponse() {
        $respObject = $this->getResponse();
        //dump($respObject);

        if(isset($respObject->errorGroup)) {
            $this->logError((string)$respObject->errorGroup[0]->errorDescription[0]->freeText[0]);
            return false;
        }

        return true;
    }

    protected function prepareArgs() {

        $args[] =
            $this->createAVar('systemIdentifier', array(
                $this->createAVar('deliveringSystem', array(
                    $this->createSVar('companyId', 'WEBS')
                ))
            ));

        $args[] =
            $this->createAVar('groupIndicator', array(
                $this->createAVar('statusDetails', array(
                    $this->createSVar('indicator', 'GR'),
                    $this->createSVar('action', 2)
                ))
            ));

        $args[] =
            $this->createAVar('travelAgentRef', array(
                $this->createSVar('status', 'APE'),
                $this->createAVar('reference', array(
                    $this->createSVar('type', 'ST'),
                    $this->createSVar('value', 1)
                ))
            ));

        foreach($this->selection as $sel) {
            $hotelRef = array(
                $this->createAVar('hotelReference', array(
                    $this->createSVar('chainCode', $sel['ChainCode']),
                    $this->createSVar('cityCode', $sel['HotelCityCode']),
                    $this->createSVar('hotelCode', substr($sel['HotelCode'], 5, 3))
                )),
                $this->createSVar('hotelName', $sel['HotelName'])
            );

            $args[] =
                $this->createAVar('roomStayData', array(
                    $this->createSVar('markerRoomStayData', ''),
                    $this->createAVar('globalBookingInfo', array(
                        $this->createAVar('markerGlobalBookingInfo', $hotelRef),
                        $this->createAVar('bookingSource', array(
                            $this->createAVar('originIdentification', array(
                                $this->createSVar('originatorId', '15210742') // ???
                            ))
                        )),
                        $this->createAVar('representativeParties', array(
                            $this->createAVar('occupantList', array(
                                /*$this->createAVar('passengerReference', array(
                                    $this->createSVar('type', ''),
                                    $this->createSVar('value', '')
                                ))*/
                            )),
                            $this->createAVar('guestContactInfo', array(
                                $this->createSVar('phoneOrEmailType', 'EML'),
                                $this->createSVar('emailAddress', $this->passengers['email'])
                            ))
                        ))
                    )),
                    $this->createAVar('roomList', array(
                        $this->createSVar('markerRoomstayQuery', ''),
                        $this->createAVar('roomRateDetails', array(
                            $this->createSVar('marker', ''),
                            $this->createAVar('hotelProductReference', array(
                                $this->createAVar('referenceDetails', array(
                                    $this->createSVar('type', 'BC'),
                                    $this->createSVar('value', $sel['room']['BookingCode'])
                                ))
                            )),
                            $this->createSVar('markerOfExtra', '')
                        )),
                        $this->createAVar('guaranteeOrDeposit', array(
                            $this->createAVar('paymentInfo', array(
                                $this->createAVar('paymentDetails', array(
                                    $this->createSVar('formOfPaymentCode', 1),
                                    $this->createSVar('paymentType', 1),
                                    $this->createSVar('serviceToPay', 3)
                                ))
                            )),
                            $this->createAVar('groupCreditCardInfo', array(
                                $this->createAVar('creditCardInfo', array(
                                    $this->createAVar('ccInfo', array(
                                        $this->createSVar('vendorCode', Configuration::get('TOUREASON_CC_TYPE')),
                                        $this->createSVar('cardNumber', Configuration::get('TOUREASON_CC_NUMBER')),
                                        $this->createSVar('expiryDate', Configuration::get('TOUREASON_CC_EXPIRE'))
                                    ))
                                ))
                            ))
                        ))
                    ))
                ));
        }

        return $args;
    }

}
