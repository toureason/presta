<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/SoapClientToureason.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');

class ToureasonRetrievePNR extends SoapClientToureason {

    protected $soapURI = 'http://xml.amadeus.com/PNRRET_14_1_1A';
    protected $soapAction = 'http://webservices.amadeus.com/PNRRET_14_1_1A';
    protected $method = 'PNR_Retrieve';
    protected $methodReply = 'PNR_Reply';
    protected $debug = false;

    public $pnr = array();


    public function parseResponse($response) {
        $respObject = $this->getResponse();
        //dump($respObject);

        if($respObject->pnrHeader->reservationInfo->reservation->controlNumber == $this->pnr['controlNumber']) {
            $tickets = [];
            foreach($respObject->dataElementsMaster->dataElementsIndiv as $div) {
                if($div->elementManagementData->segmentName == 'FA') {
                    list($pax,) = explode('/', $div->otherDataFreetext->longFreetext);
                    list($foo, $tn) = explode(' ', $pax);
                    $tickets[] = $tn;
                }
            }
            return $tickets;
        }

        $this->logError('PNR not retrieved.');
        return false;
    }

    protected function setHeaders() {
        $this->headers[] = new WsseAuthHeader($this->login, $this->passwd);
        $this->headers[] = new SessionStartHeader();
        parent::setHeaders();
    }

    protected function prepareArgs() {

        $args[] =
            $this->createAVar('retrievalFacts', array(
                $this->createAVar('retrieve', array(
                    $this->createSVar('type', 2)
                )),
                $this->createAVar('reservationOrProfileIdentifier', array(
                    $this->createAVar('reservation', array(
                        $this->createSVar('companyId', $this->pnr['companyId']),
                        $this->createSVar('controlNumber', $this->pnr['controlNumber'])
                    ))
                ))
            ));

        return $args;
    }


}
