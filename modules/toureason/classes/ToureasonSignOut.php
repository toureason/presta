<?php

require_once(_PS_MODULE_DIR_.'toureason/classes/SoapClientToureason.php');
require_once(_PS_MODULE_DIR_.'toureason/classes/SoapHeaders.php');

class ToureasonSignOut extends SoapClientToureason {

    protected $soapURI = 'http://xml.amadeus.com/VLSSOQ_04_1_1A';
    protected $soapAction = 'http://webservices.amadeus.com/VLSSOQ_04_1_1A';
    protected $method = 'Security_SignOut';
    protected $debug = false;


    public function parseResponse($response) {
        //dump($response);
        if(isset($response->errorSection)) return false;
        return true;
    }

    protected function setHeaders() {
        $this->headers[] = new SessionSequenceHeader($this->session, 'End');
        parent::setHeaders();
    }

    protected function prepareArgs() {
        return array();
    }


}
