CREATE TABLE IF NOT EXISTS `PREFIX_toureason_airlines` (
  `id_toureason_airlines` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `iata_code` char(2) COLLATE latin1_general_ci NOT NULL,
  `name` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `country` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `active` char(1) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_toureason_airlines`),
  UNIQUE `iata_code` (`iata_code`),
  KEY `iata_code_active` (`iata_code`,`active`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
