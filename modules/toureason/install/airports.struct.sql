CREATE TABLE IF NOT EXISTS `PREFIX_toureason_airports` (
  `id_toureason_airports` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `iata_code` char(3) COLLATE latin1_general_ci NOT NULL,
  `name` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `country` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `iscity` enum('yes','no') COLLATE latin1_general_ci NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_toureason_airports`),
  KEY `iata_code` (`iata_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
