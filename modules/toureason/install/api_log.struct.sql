CREATE TABLE IF NOT EXISTS `PREFIX_toureason_api_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service` varchar(100) NOT NULL,
  `ts` varchar(50) DEFAULT NULL,
  `session` varchar(20) DEFAULT NULL,
  `errmsg` varchar(250) DEFAULT NULL,
  `request` text,
  `response` mediumtext,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
