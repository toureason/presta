CREATE TABLE IF NOT EXISTS `PREFIX_toureason_flight_search_result` (
  `id_cart` int(10) unsigned NOT NULL,
  `json_search` text NOT NULL,
  `json_result` text NOT NULL,
  `soap_response` text NOT NULL,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_cart`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
