CREATE TABLE IF NOT EXISTS `PREFIX_toureason_flight_select` (
  `id_flight` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cart` int(10) unsigned NOT NULL,
  `json_flight` text NOT NULL,
  `json_passengers` text NOT NULL,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_flight`),
  INDEX (`id_cart`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
