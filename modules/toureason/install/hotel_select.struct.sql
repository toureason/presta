CREATE TABLE IF NOT EXISTS `PREFIX_toureason_hotel_select` (
  `id_hotel` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cart` int(10) unsigned NOT NULL,
  `json_hotel` text NOT NULL,
  `json_passengers` text NOT NULL,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_hotel`),
  INDEX (`id_cart`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
