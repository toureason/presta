CREATE TABLE IF NOT EXISTS `PREFIX_toureason_pnr` (
  `id_pnr` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cart` int(10) unsigned NOT NULL,
  `json_pnr` text NOT NULL,
  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pnr`),
  INDEX (`id_cart`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
