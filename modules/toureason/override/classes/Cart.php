<?php

require_once(_PS_MODULE_DIR_ . 'toureason/classes/Db/ToureasonDb.php');


class Cart extends CartCore {

    public $foo;

	/*
    public function getOrderTotal($with_taxes = true, $type = Cart::BOTH, $products = null, $id_carrier = null, $use_cache = true) {
        $order_total = parent::getOrderTotal($with_taxes, $type, $products, $id_carrier, $use_cache);

        if(isset($this->id) && ($type == Cart::BOTH)) {
            $tvals = ToureasonDb::loadCartTValues($this->id);
            return Tools::ps_round((float) ($order_total + $tvals['flight_price'] + $tvals['hotel_price']), Configuration::get('_PS_PRICE_COMPUTE_PRECISION_'));
        }

        return $order_total;
	}
    */

    public function getProducts($refresh = false, $id_product = false, $id_country = null) {
        if(strpos($_SERVER['REQUEST_URI'], '/modules/paypal/') === false) return parent::getProducts($refresh, $id_product, $id_country);
        $this->_products = parent::getProducts(true, $id_product, $id_country);
        $tvals = ToureasonDb::loadCartTValues($this->id);
        if($tvals['flight_price'] > 0) {
            $this->_products[] = array(
                'id_product' => $tvals['id_flight'],
                'name' => 'Flight',
                'description_short' => '...',
                'price_wt' => $tvals['flight_price'],
                'quantity' => 1,
                'cart_quantity' => 1,
                'is_virtual' => false,
                'id_address_delivery' => $this->_products[0]['id_address_delivery'],
                'id_shop' => $this->_products[0]['id_shop'],
                'id_product_attribute' => 0,
                'additional_shipping_cost' => 0,
                'weight_attribute' => null,
                'weight' => 0,
            );
        }
        if($tvals['hotel_price'] > 0) {
            $this->_products[] = array(
                'id_product' => $tvals['id_hotel'],
                'name' => 'Hotel',
                'description_short' => '...',
                'price_wt' => $tvals['hotel_price'],
                'quantity' => 1,
                'cart_quantity' => 1,
                'is_virtual' => false,
                'id_address_delivery' => $this->_products[0]['id_address_delivery'],
                'id_shop' => $this->_products[0]['id_shop'],
                'id_product_attribute' => 0,
                'additional_shipping_cost' => 0,
                'weight_attribute' => null,
                'weight' => 0,
            );
        }
        return $this->_products;
    }

}
