<?php
require_once(_PS_MODULE_DIR_ . 'toureason/classes/Db/ToureasonFlightDb.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonFlightSearch.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonFlightInfoPrice.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonFlightSell.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonFlightMultiElements.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonFlightPricePNR.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonFlightCreateTST.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonFlightMultiElementsPNR.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonFlightMultiElementsPayment.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonFlightIssueTicket.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonHotelSearch.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonHotelSearchLive.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonHotelDetail.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonHotelPricing.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonHotelSell.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/Db/ToureasonHotelDb.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonRetrievePNR.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/ToureasonSignOut.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/CartAddress.php');
require_once(_PS_MODULE_DIR_ . 'toureason/classes/Db/ToureasonDb.php');

class OrderController extends OrderControllerCore{

    const STEP_SUMMARY_EMPTY_CART = -1;

    const STEP_FLIGHTS = 101;

    const STEP_HOTELS = 102;

    const STEP_ADDRESSES = 1;

    const STEP_DELIVERY = 2;

    const STEP_PAYMENT = 103;

    const FLIGHT_PAGINATION = 10;

    const HOTEL_PAGINATION = 5;


    public $step;
    public $lastSession = null;

    protected $loadNext = -1;

    public static $flightSort = 'price';
    public static $hotelSort = 'price';
    public static $sortDir = 1;


    public function initContent(){

        $this->process();

        if (!isset($this->context->cart)) {
            $this->context->cart = new Cart();
        }

        if (!$this->useMobileTheme()) {
            // These hooks aren't used for the mobile theme.
            // Needed hooks are called in the tpl files.
            $this->context->smarty->assign(array(
                'HOOK_HEADER'       => Hook::exec('displayHeader'),
                'HOOK_TOP'          => Hook::exec('displayTop'),
                'HOOK_LEFT_COLUMN'  => ($this->display_column_left  ? Hook::exec('displayLeftColumn') : ''),
                'HOOK_RIGHT_COLUMN' => ($this->display_column_right ? Hook::exec('displayRightColumn', array('cart' => $this->context->cart)) : ''),
            ));
        } else {
            $this->context->smarty->assign('HOOK_MOBILE_HEADER', Hook::exec('displayMobileHeader'));
        }

        $action = Tools::getValue("action");
        if($action == 'ticketing') $this->ticketing();
        if ($action && in_array($this->step, array(self::STEP_FLIGHTS, self::STEP_HOTELS, self::STEP_PAYMENT))) {
            switch ($action) {
                case "flightsearch":
                    $this->flightSearch();
                    break;

                case "flightset":
                    $this->flightSet();
                    break;

                case "flightunset":
                    $this->flightUnset();
                    break;

                case "flightsetpax":
                    $this->flightSetPax();
                    break;

                case "airportsload":
                    $this->airportsLoad();
                    break;

                case "hotelsearch":
                    $this->hotelSearch();
                    break;

                case "hotelset":
                    $this->hotelSet();
                    break;

                case "hotelsetpax":
                    $this->hotelSetPax();
                    break;

                case "hotelunset":
                    $this->hotelUnset();
                    break;

                case "hotelnext":
                    $this->hotelNext();
                    break;

                case "citiesload":
                    $this->citiesLoad();
                    break;
            }

        }

        if (Tools::isSubmit('ajax') && Tools::getValue('method') == 'updateExtraCarrier') {
            $delivery_option = $this->context->cart->getDeliveryOption();
            $delivery_option[(int)Tools::getValue('id_address')] = Tools::getValue('id_delivery_option');
            $this->context->cart->setDeliveryOption($delivery_option);
            $this->context->cart->save();
            $return = [
                'content' => Hook::exec(
                    'displayCarrierList',
                    [
                        'address' => new Address((int)Tools::getValue('id_address'))
                    ]
                )
            ];
            $this->ajaxDie(Tools::jsonEncode($return));
        }
        if ($this->nbProducts) {
            $this->context->smarty->assign('virtual_cart', $this->context->cart->isVirtualCart());
        }
        if (!Tools::getValue('multi-shipping')) {
            $this->context->cart->setNoMultishipping();
        }
        $is_advanced_payment_api = (bool)Configuration::get('PS_ADVANCED_PAYMENT_API');
        self::$smarty->assign("_current_step", (int)$this->step);
        if(isset($this->context->cart) && $this->context->cookie->id_cart) {
            $this->context->smarty->assign('product_list', $this->context->cart->getProducts());
            $this->context->smarty->assign('token_cart', Tools::getToken(false));
            $cartVals = ToureasonDb::loadCartTValues($this->context->cookie->id_cart);
            self::$smarty->assign('orderTotal', $this->context->cart->getOrderTotal(true) + $cartVals['flight_price'] + $cartVals['hotel_price']);
            self::$smarty->assign('flight_price', $cartVals['flight_price']);
            self::$smarty->assign('hotel_price', $cartVals['hotel_price']);
            $fselect = $hselect = false;
            if($cartVals['id_flight']) {
                $fselect = ToureasonFlightDb::loadFlightSelect($this->context->cookie->id_cart);
                $fgroup1 = json_decode($fselect['json_flight'], TRUE);
                $flightNos = [];
                foreach(array_merge($fgroup1[1], isset($fgroup1[2]) ? $fgroup1[2] : []) as $flight) {
                    $flightNos[] = '#' . $flight['number'];
                }
                $pax1 = json_decode($fselect['json_passengers'], TRUE);
                self::$smarty->assign('flight_pax', $pax1);
                self::$smarty->assign('flight_nos', implode(', ', $flightNos));
            }
            if($cartVals['id_hotel']) {
                $hselect = ToureasonHotelDb::loadHotelSelect($this->context->cookie->id_cart);
                $fpax = json_decode($hselect['json_passengers'], TRUE);
                self::$smarty->assign('hotel_pax', $fpax['cnt']);
                self::$smarty->assign('hotel_rooms', $fpax['roomsCnt']);
            }
        }
        switch ((int)$this->step) {
            case OrderController::STEP_SUMMARY_EMPTY_CART:
                $this->context->smarty->assign('empty', 1);
                $this->setTemplate(_PS_THEME_DIR_ . 'shopping-cart.tpl');
                break;
            case self::STEP_FLIGHTS:
                if ($action == 'flightsetpax') {
                    $this->setTemplate(_PS_THEME_DIR_ . '_flights_setpax.tpl');
                    $formParamsSearch = ToureasonFlightDb::loadFlightSearch($this->context->cookie->id_cart);
                    $formParamsSearch['depdate'] = substr($formParamsSearch['departure'], 6, 4) . '-' . substr($formParamsSearch['departure'], 3, 2) . '-' . substr($formParamsSearch['departure'], 0, 2);
                    if($formParamsSearch['return_date']){
                        $formParamsSearch['retdate'] = substr($formParamsSearch['return_date'], 6, 4) . '-' . substr($formParamsSearch['return_date'], 3, 2) . '-' . substr($formParamsSearch['return_date'], 0, 2);
                    }
                    $this->context->smarty->assign('form_params_search', $formParamsSearch);
                } else {
                    $this->setTemplate(_PS_THEME_DIR_ . '_flights.tpl');
                    $this->context->smarty->assign('form_params', ToureasonFlightDb::loadFlightSearch($this->context->cookie->id_cart));
                    //$fselect = ToureasonFlightDb::loadFlightSelect($this->context->cookie->id_cart);
                    if($fselect) {
                        $fgroup = json_decode($fselect['json_flight'], TRUE);
                        if($fgroup) $this->showSavedFlight($fgroup);
                    }
                    $cms = new CMS((int)Configuration::get('TOUREASON_LOADING_ARTICLE_ID'), intval($this->context->cookie->id_lang));
                    if (Validate::isLoadedObject($cms)) $this->context->smarty->assign('loading', $cms->content);
                }
                break;
            case self::STEP_HOTELS:
                if ($action == 'hotelsetpax') {
                    $this->setTemplate(_PS_THEME_DIR_ . '_hotels_setpax.tpl');
                    $formParamsSearch = ToureasonHotelDb::loadHotelSearch($this->context->cookie->id_cart);
                    $formParamsSearch['startISO'] = substr($formParamsSearch['start_date'], 6, 4) . '-' . substr($formParamsSearch['start_date'], 3, 2) . '-' . substr($formParamsSearch['start_date'], 0, 2);
                    $formParamsSearch['endISO'] = substr($formParamsSearch['end_date'], 6, 4) . '-' . substr($formParamsSearch['end_date'], 3, 2) . '-' . substr($formParamsSearch['end_date'], 0, 2);
                    $this->context->smarty->assign('form_params', $formParamsSearch);
                } else {
                    $this->setTemplate(_PS_THEME_DIR_ . '_hotels.tpl');
                    $this->context->smarty->assign('form_params', ToureasonHotelDb::loadHotelSearch($this->context->cookie->id_cart));
                    //$fselect = ToureasonHotelDb::loadHotelSelect($this->context->cookie->id_cart);
                    if($hselect) {
                        $selection = json_decode($hselect['json_hotel'], TRUE);
                        if($selection) $this->showSavedHotel($selection);
                    }
                    $cms = new CMS((int)Configuration::get('TOUREASON_LOADING_ARTICLE_ID'), intval($this->context->cookie->id_lang));
                    if (Validate::isLoadedObject($cms)) $this->context->smarty->assign('loading', $cms->content);
                }
                break;
            case OrderController::STEP_ADDRESSES:
                $this->_assignAddress();
                $this->processAddressFormat();
                if (Tools::getValue('multi-shipping') == 1) {
                    $this->_assignSummaryInformations();
                    $this->context->smarty->assign('product_list', $this->context->cart->getProducts());
                    $this->setTemplate(_PS_THEME_DIR_ . 'order-address-multishipping.tpl');
                } else {
                    $this->setTemplate(_PS_THEME_DIR_ . 'order-address.tpl');
                }
                break;
            case OrderController::STEP_DELIVERY:
                if (Tools::isSubmit('processAddress')) {
                    $this->processAddress();
                }
                $this->autoStep();
                $this->_assignCarrier();
                $this->setTemplate(_PS_THEME_DIR_ . 'order-carrier.tpl');
                break;
            case OrderController::STEP_PAYMENT:
                if(! $cartVals['id_flight'] && ! $cartVals['id_hotel']) {
                    Tools::redirect('index.php?controller=order&step=' . self::STEP_FLIGHTS);
                }
                if(isset($this->context->cart->id_address_delivery)) {
                    $address = new Address($this->context->cart->id_address_delivery);
                }
                if(isset($this->context->cart->id_customer)) {
                    $customer = new Customer($this->context->cart->id_customer);
                }
                $addressValidate = 0;
                if (Tools::isSubmit('alias')) {
                    $this->_updateMessage(Tools::getValue('message'));
                    if(! isset($address)) $address = new Address();
                    $addrErrors = $address->validateController();
                    if(! isset($customer)) $customer = new Customer();
                    $_POST['passwd'] = md5(time()._COOKIE_KEY_);
                    $custErrors = $customer->validateController();
                    if((count($custErrors) > 0) || (count($addrErrors) > 0)) {
                        $this->context->controller->errors[] = Tools::displayError('Error in saving address');
                        $addressValidate = 1;
                    }
                    else {
                        $customer->active = 1;
                        $customer->is_guest = 1;
                        $customer->save();
                        $address->id_customer = $customer->id;
                        $address->save();
                        $this->updateContextGuest($customer);
                        $this->context->cart->id_address_delivery = $address->id;
                        $this->context->cart->update();
                    }
                    if(count($this->context->controller->errors) == 0) {
                        try {
                            if($cartVals['id_flight']) {
                                $pnr = ToureasonDb::loadPNR($this->context->cookie->id_cart);
                                //$fselect = ToureasonFlightDb::loadFlightSelect($this->context->cookie->id_cart);
                                $fgroup = json_decode($fselect['json_flight'], TRUE);
                                $pax = json_decode($fselect['json_passengers'], TRUE);

                                if(! $pnr || ($pnr['flight'] != $cartVals['id_flight'])) {

                                    $this->checkFlightInfoPrice(null, null, $fgroup, $pax);

                                    $book1 = new ToureasonFlightSell();
                                    $book1->passengers = $pax;
                                    $book1->fgroup = $fgroup;
                                    $ret = $book1->process();
                                    $result = $book1->parseResponse($ret);
                                    $this->lastSession = $book1->getSession();
                                    if (! $result) throw new Exception(Tools::displayError('Unservicable flight.'));

                                    $book2 = new ToureasonFlightMultiElements();
                                    $book2->setSession($this->lastSession);
                                    $book2->marketingCarrier = $fgroup[1][0]['carrier'];
                                    $book2->passengers = $book1->passengers;
                                    $ret = $book2->process();
                                    $book2->parseResponse($ret);
                                    $this->lastSession = $book2->getSession();

                                    $book3 = new ToureasonFlightPricePNR();
                                    $book3->setSession($this->lastSession);
                                    $book3->fgroup = $fgroup;
                                    $book3->passengers = $book2->passengers;
                                    $ret = $book3->process();
                                    $result = $book3->parseResponse($ret);
                                    $this->lastSession = $book3->getSession();

                                    $book4 = new ToureasonFlightCreateTST();
                                    $book4->setSession($this->lastSession);
                                    $book4->passengers = $book3->passengers;
                                    $ret = $book4->process();
                                    $result = $book4->parseResponse($ret);
                                    $this->lastSession = $book4->getSession();

                                    $book5 = new ToureasonFlightMultiElementsPNR();
                                    $book5->setSession($this->lastSession);
                                    $ret = $book5->process();
                                    $pnrNew = $book5->parseResponse($ret);
                                    $this->lastSession = $book5->getSession();

                                    $saved = ToureasonFlightDb::saveCreatedPNR(self::mergePNR('flight', $cartVals['id_flight'], $pnrNew), $book4->passengers, $this->context->cookie->id_cart);
                                    if ($saved === FALSE) {
                                        throw new Exception(Tools::displayError('Unable to save PNR.'));
                                    }

                                    $this->endSession();
                                }
                            }

                            Tools::redirect('/modules/paypal/express_checkout/payment.php?current_shop_url=http' . (!empty($_SERVER['HTTPS'])?"s":"") . '://' . $_SERVER['HTTP_HOST'] . '/index.php?controller%3Dorder%26step%3D103&express_checkout=payment_cart');
                        }
                        catch (Exception $ex) {
                            $this->context->controller->errors[] = $ex->getMessage();
                            $this->endSession();
                        }
                    }
                }

                if(isset($address)) $this->context->smarty->assign('address', $address);
                if(isset($customer)) $this->context->smarty->assign('customer', $customer);

                $this->context->smarty->assign('cgv', $this->context->cookie->check_cgv);
                $this->context->smarty->assign('address_validate', $addressValidate);
                $msg = Message::getMessageByCartId((int)$this->context->cart->id);
                $this->context->smarty->assign('oldMessage', (is_array($msg) ? $msg['message'] : ''));

                $cgv = Tools::getValue('cgv') || $this->context->cookie->check_cgv;
                Context::getContext()->cookie->check_cgv = $cgv;

                $this->_assignPayment();
                $this->_assignWrappingAndTOS();
                $this->assignCountries(isset($address) ? $address->id_country : 0);

                //$fselect = ToureasonFlightDb::loadFlightSelect($this->context->cookie->id_cart);
                if($fselect) {
                    $fgroup = json_decode($fselect['json_flight'], TRUE);
                    if($fgroup) $this->showSavedFlight($fgroup);
                }
                if($hselect) {
                    $selection = json_decode($hselect['json_hotel'], TRUE);
                    if($selection) $this->showSavedHotel($selection);
                }

                if ($is_advanced_payment_api === TRUE) {
                    $this->_assignAddress();
                }
                $this->_assignSummaryInformations();
                $this->setTemplate(_PS_THEME_DIR_ . 'order-payment.tpl');
                break;
            default:
                $this->_assignSummaryInformations();
                $this->setTemplate(_PS_THEME_DIR_ . 'shopping-cart.tpl');
                break;
        }
    }

    protected function updateContextGuest($customer) {
        $this->context->customer = $customer;
        $this->context->cookie->id_customer = (int)$customer->id;
        $this->context->cookie->customer_lastname = $customer->lastname;
        $this->context->cookie->customer_firstname = $customer->firstname;
        $this->context->cookie->passwd = $customer->passwd;
        $this->context->cookie->logged = 1;
        $customer->logged = 1;
        $this->context->cookie->email = $customer->email;
        $this->context->cookie->is_guest = 1;
        // Update cart address
        $this->context->cart->secure_key = $customer->secure_key;
    }

    protected function assignCountries($id_country) {
        // Generate countries list
        if (Configuration::get('PS_RESTRICT_DELIVERED_COUNTRIES')) {
            $countries = Carrier::getDeliveredCountries($this->context->language->id, true, true);
        } else {
            $countries = Country::getCountries($this->context->language->id, true);
        }

        $list = '';
        foreach ($countries as $country) {
            $selected = ((int)$country['id_country'] === (int)$id_country) ? ' selected="selected"' : '';
            $list .= '<option value="'.(int)$country['id_country'].'"'.$selected.'>'.htmlentities($country['name'], ENT_COMPAT, 'UTF-8').'</option>';
        }

        // Assign vars
        $this->context->smarty->assign(array(
            'countries_list' => $list,
            'countries' => $countries,
            'sl_country' => (int)$id_country,
        ));
    }

    protected function flightSearch(){

        $output = '';

        $next = intval(Tools::getValue("next"));
        $reload = intval(Tools::getValue("reload"));
        $sort = Tools::getValue("sort");
        if(! in_array($sort, array('price', 'time', 'move'))) $sort = 'price';
        self::$flightSort = $sort;
        $sortDir = Tools::getValue("sortdir");
        if($sortDir == 'desc') self::$sortDir = -1;
        // not load from Amadeus API (load from DB)
        if(! $reload) {
            $result = ToureasonFlightDb::loadFlightSearchResult($this->context->cookie->id_cart);
            if($result) {
                $output = $this->getFlightSearchResultsOutput($result, $next, self::FLIGHT_PAGINATION);
                $this->ajaxDie(Tools::jsonEncode(['output'=>$output, 'next'=>$this->loadNext]));
            }
        }

        $lastSearch = array(
            'adults' => intval(Tools::getValue("adults")),
            'children' => intval(Tools::getValue("children")),
            'infants' => intval(Tools::getValue("infants")),
            'from' => strtoupper(Tools::getValue("from")),
            'to' => strtoupper(Tools::getValue("to")),
            'from_auto' => Tools::getValue("from_auto"),
            'to_auto' => Tools::getValue("to_auto"),
            'departure' => Tools::getValue("departure"),
            'da_time' => Tools::getValue("da_time"),
            'pref_time' => Tools::getValue("pref_time"),
            'direct' => Tools::getValue("direct"),
            'return_date' => Tools::getValue("return_date"),
            'return_da_time' => Tools::getValue("return_da_time"),
            'return_pref_time' => Tools::getValue("return_pref_time"),
            'class' => Tools::getValue("class"),
        );
        $depISO = $retISO = '';
        if($lastSearch['departure']) $depISO = substr($lastSearch['departure'], 6, 4) . '-' . substr($lastSearch['departure'], 3, 2) . '-' . substr($lastSearch['departure'], 0, 2);
        if($lastSearch['return_date']) $retISO = substr($lastSearch['return_date'], 6, 4) . '-' . substr($lastSearch['return_date'], 3, 2) . '-' . substr($lastSearch['return_date'], 0, 2);
        $errors = [];

        // validations
        if (preg_match('/^[a-z]{3,3}$/i', $lastSearch['from']) !== 1) {
            $errors[] = Tools::displayError('Bad airport code - from.');
        }
        if (preg_match('/^[a-z]{3,3}$/i', $lastSearch['to']) !== 1) {
            $errors[] = Tools::displayError('Bad airport code - to.');
        }
        if ((preg_match('/^[0-9]{2,2}\.[0-9]{2,2}\.[0-9]{4,4}$/', $lastSearch['departure']) !== 1) || ($depISO < date('Y-m-d'))) {
            $errors[] = Tools::displayError('Bad departure date.');
        }
        else if($depISO > date('Y-m-d', time() + 361*86400)) {
            $errors[] = Tools::displayError('Departure date cannot go more than 361 days into future.');
        }
        if ($lastSearch['return_date']) {
            if((preg_match('/^[0-9]{2,2}\.[0-9]{2,2}\.[0-9]{4,4}$/', $lastSearch['return_date']) !== 1) || ($retISO < date('Y-m-d'))) {
                $errors[] = Tools::displayError('Bad return date.');
            }
            else if($retISO > date('Y-m-d', time() + 361*86400)) {
                $errors[] = Tools::displayError('Return date cannot go more than 361 days into future.');
            }
        }
        if($lastSearch['return_date'] && ($retISO < $depISO)) {
            $errors[] = Tools::displayError('Return date has to be after departure date.');
        }
        if ($lastSearch['from'] == $lastSearch['to']) {
            $errors[] = Tools::displayError('You cannot travel to the same destination.');
        }
        if (($lastSearch['adults'] < 1) || ($lastSearch['adults'] > 9)) {
            $errors[] = Tools::displayError('Bad count of adults.');
        }
        if (($lastSearch['children'] < 0) || ($lastSearch['children'] > 9)) {
            $errors[] = Tools::displayError('Bad count of children.');
        }
        if (($lastSearch['infants'] < 0) || ($lastSearch['infants'] > 9)) {
            $errors[] = Tools::displayError('Bad count of infants.');
        }
        if ($lastSearch['infants'] > $lastSearch['adults']) {
            $errors[] = Tools::displayError('Infants count may not be more than adults.');
        }
        if (!in_array($lastSearch['class'], ['', 'M', 'W', 'Y', 'C', 'F'])) {
            $errors[] = Tools::displayError('Bad class.');
        }
        if (!in_array($lastSearch['da_time'], ['', 'TD', 'TA']) || !in_array($lastSearch['pref_time'], ['', 'M', 'A', 'E', 'N'])) {
            $errors[] = Tools::displayError('Bad preffered time.');
        }
        if (!in_array($lastSearch['return_da_time'], ['', 'TD', 'TA']) || !in_array($lastSearch['return_pref_time'], ['', 'M', 'A', 'E', 'N'])) {
            $errors[] = Tools::displayError('Bad preffered time of return.');
        }

        // search
        if ((count($errors) == 0) && $lastSearch['from'] && $lastSearch['to'] && $lastSearch['adults'] && $lastSearch['departure']) {
            try {
                //$pnr = $this->getPNR('flight');
                $flight = new ToureasonFlightSearch();
                $flight->from = $lastSearch['from'];
                $flight->to = $lastSearch['to'];
                $flight->adults = $lastSearch['adults'];
                if ($lastSearch['children']) {
                    $flight->children = $lastSearch['children'];
                }
                if ($lastSearch['infants']) {
                    $flight->infants = $lastSearch['infants'];
                }
                if ($lastSearch['class']) {
                    $flight->cabin = $lastSearch['class'];
                }
                if ($lastSearch['direct']) {
                    $flight->direct = TRUE;
                }
                $flight->date = self::getAirDate($lastSearch['departure']);
                if ($lastSearch['da_time'] && $lastSearch['pref_time']) {
                    $flight->timeDA = $lastSearch['da_time'];
                    $flight->timeInterval = $lastSearch['pref_time'];
                }
                if ($lastSearch['return_date']) {
                    $flight->returnDate = self::getAirDate($lastSearch['return_date']);
                    if ($lastSearch['return_da_time'] && $lastSearch['return_pref_time']) {
                        $flight->returnTimeDA = $lastSearch['return_da_time'];
                        $flight->returnTimeInterval = $lastSearch['return_pref_time'];
                    }
                }
                $ret = $flight->process();
                if (is_object($ret) && isset($ret->errorMessageText)) {
                    $errors[] = $ret->errorMessageText->description;
                }

                $result = $flight->parseResponse($ret);

                if (count($result) > 0) {
                    ToureasonFlightDb::saveFlightSearch($lastSearch, $ret, $result, $this->context->cookie->id_cart);
                    $output = $this->getFlightSearchResultsOutput($result, 0, self::FLIGHT_PAGINATION);
                }

            } catch (Exception $ex) {
                //echo $ex->faultcode;
                $errors[] = $ex->getMessage();
            }
        }
        if(count($errors) > 0) $this->ajaxDie(Tools::jsonEncode(['error'=>$errors]));

        $this->ajaxDie(Tools::jsonEncode(['output'=>$output, 'next'=>$this->loadNext]));
    }

    public static function cmp($a, $b) {
        if(self::$flightSort == 'move') {
            $ret = self::cmpWithKey($a, $b, 'movesTotal');
            if ($ret == 0) {
                return self::cmpWithKey($a, $b, 'priceOrig');
            }
            return $ret;
        }

        $key = 'priceOrig';
        if(self::$flightSort == 'time') $key = 'durationTotal';
        return self::cmpWithKey($a, $b, $key);
    }

    public static function cmpWithKey($a, $b, $key) {
        if ($a[0][$key] == $b[0][$key]) {
            return 0;
        }
        return self::$sortDir * (($a[0][$key] < $b[0][$key]) ? -1 : 1);
    }

    public static function cmpHotel($a, $b) {
        $key = 'PriceFrom';
        //if(self::$hotelSort == 'price') $key = 'PriceFrom';
        return self::cmpWithKeyHotel($a, $b, $key);
    }

    public static function cmpWithKeyHotel($a, $b, $key) {
        if ($a[$key] == $b[$key]) {
            return 0;
        }
        return self::$sortDir * (($a[$key] < $b[$key]) ? -1 : 1);
    }

    protected function getFlightSearchResultsOutput($res, $next, $cnt) {

        usort($res['seg'][1], array('self', 'cmp'));
        if(isset($res['seg'][2])) {
            usort($res['seg'][2], array('self', 'cmp'));
        }

        $result = $res;
        if(count($res['seg'][1]) > $cnt) {
            unset($result['seg']);
            $iter = 0;
            foreach($res['seg'][1] as $index => $seg1) {
                if(($iter >= $next) && ($iter < ($next + $cnt))) {
                    $result['seg'][1][$index] = $res['seg'][1][$index];
                    if(isset($res['seg'][2][$index])) $result['seg'][2][$index] = $res['seg'][2][$index];
                }
                $iter++;
            }
            if($iter > ($next + $cnt)) $this->loadNext = $next + $cnt;
        }

        $this->context->smarty->assign('flights', $result);
        ob_start();
        $this->smartyOutputContent(_PS_THEME_DIR_ . '_flights_search_results.tpl');
        $output = ob_get_clean();
        $output = str_replace('</body></html>', '', $output);

        return $output;
    }

    private static function getAirDate($date){
        return str_replace('.', '', str_replace('.20', '', $date));
    }

    protected function flightSet(){
        $fgroup = intval(Tools::getValue("fgroup"));
        $searchResult = ToureasonFlightDb::loadFlightSearchResult($this->context->cookie->id_cart);
        if (isset($searchResult['seg'][1][$fgroup])) {
            try {
                //$pnr = $this->getPNR('flight');
                list($fgrArr, $passengers) = $this->checkFlightInfoPrice($searchResult, $fgroup);
                $fselect = ToureasonFlightDb::loadFlightSelect($this->context->cookie->id_cart);
                if($fselect) {
                    $pax = json_decode($fselect['json_passengers'], TRUE);
                    foreach($passengers as $ptype => $pobj) {
                        $pax[$ptype]['cnt'] = $passengers[$ptype]['cnt'];
                        if(isset($passengers[$ptype]['price'])) $pax[$ptype]['price'] = $passengers[$ptype]['price'];
                    }
                    $passengers = $pax;
                }

                // write flight to DB
                $flightId = ToureasonFlightDb::saveFlightSelect(
                    $fgrArr,
                    $passengers,
                    $this->context->cookie->id_cart,
                    ToureasonFlightSearch::getToureasonPrice($fgrArr[1][0]['priceOrig'])
                );
                if ($flightId === FALSE) {
                    $this->context->controller->errors[] = Tools::displayError('Unable to save flight selection.');
                } else {
                    Tools::redirect('index.php?controller=order&step=' . self::STEP_FLIGHTS . '&action=flightsetpax');
                }
            } catch (Exception $ex) {
                $this->context->controller->errors[] = $ex->getMessage();
            }
        } else {
            $this->context->controller->errors[] = Tools::displayError('Unable to retrieve selected flight.');
        }
    }

    protected function checkFlightInfoPrice($searchResult = null, $fgroupIndex = null, $fgroup = null, $pax = null){
        $flight = new ToureasonFlightInfoPrice();
        if(isset($fgroup) && isset($pax)) {
            $flight->fgroup = $fgroup;
            $flight->passengers = $pax;
        }
        else if(isset($searchResult) && isset($fgroupIndex)) {
            $flight->fgroup[1] = $searchResult['seg'][1][$fgroupIndex];
            if (isset($searchResult['seg'][2])) {
                $flight->fgroup[2] = $searchResult['seg'][2][$fgroupIndex];
            }
            $flight->passengers = $searchResult['passengers'];
        }
        else throw new Exception(Tools::displayError('Unable to check flight price.'));
        $ret = $flight->process();
        $result = $flight->parseResponse($ret);
        return [$flight->fgroup, $flight->passengers];
    }

    protected function checkHotelAvailability($selection, $passengers, $endSession=true){
        $step1 = new ToureasonHotelSearchLive();
        $step1->selection = $selection;
        $step1->passengers = $passengers;
        $ret = $step1->process();
        $available = $step1->parseResponse($ret);
        $this->lastSession = $step1->getSession();

        if($available < count($selection)) {
            throw new Exception(Tools::displayError('Rooms are not available.'));
        }

        $step2 = new ToureasonHotelPricing();
        $step2->setSession($this->lastSession);
        $step2->selection = $selection;
        $step2->passengers = $passengers;
        $ret = $step2->process();
        $trusted = $step2->parseResponse($ret);
        $this->lastSession = $step2->getSession();

        if($trusted < count($selection)) {
            throw new Exception(Tools::displayError('Price check not passed.'));
        }

        if($endSession) {
            $this->endSession();
        }

        return true;
    }

    protected function flightUnset(){
        ToureasonFlightDb::deleteFlightSelect($this->context->cookie->id_cart);
        Tools::redirect('index.php?controller=order&step=' . self::STEP_FLIGHTS);
    }

    protected function hotelUnset(){
        ToureasonHotelDb::deleteHotelSelect($this->context->cookie->id_cart);
        Tools::redirect('index.php?controller=order&step=' . self::STEP_HOTELS);
    }

    protected function showSavedFlight($fgroup){
        $compIds = [];
        $locIds = [];
        foreach($fgroup as $fgr) {
            foreach($fgr as $flight) {
                $compIds[] = $flight['company'];
                $locIds[] = $flight['loc'][0]['id'];
                $locIds[] = $flight['loc'][1]['id'];
            }
        }
        $this->context->smarty->assign('fgroup', $fgroup);
        $this->context->smarty->assign('locations', ToureasonFlightDb::loadLocations($locIds));
        $this->context->smarty->assign('companies', ToureasonFlightDb::loadCompanies($compIds));
    }

    protected function showSavedHotel($selection){
        $this->context->smarty->assign('selection', $selection);
    }

    protected function flightSetPax(){
        $firstName = $lastName = $sal = $ffcard = $ffcardcode = $birth = array();
        try {
            $fselect = ToureasonFlightDb::loadFlightSelect($this->context->cookie->id_cart);
            if(! $fselect || ! $fselect['json_flight']) throw new Exception(Tools::displayError('Flight not selected'));
            $fgroup = json_decode($fselect['json_flight'], TRUE);
            $pax = json_decode($fselect['json_passengers'], TRUE);
            if($fgroup) $this->showSavedFlight($fgroup);
            $this->context->smarty->assign('passengers', $pax);

            if (Tools::isSubmit('first_name')) {
                $firstName = Tools::getValue("first_name");
                $lastName = Tools::getValue("last_name");
                $sal = Tools::getValue("sal");
                $ffcard = Tools::getValue("ffcard");
                $ffcardcode = Tools::getValue("ffcardcode");
                $birth = Tools::getValue("birth");
                $birthError = ['INF' => Tools::displayError('Infants are from 0 to 2 years.'), 'CH' => Tools::displayError('Children are from 2 to 12 years.')];
                foreach($firstName as $paxtype => $fns) {
                    if (count($fns) != $pax[$paxtype]['cnt']) {
                        throw new Exception(Tools::displayError('Bad passengers count.'));
                    }
                    $pax[$paxtype]['list'] = array();
                    foreach($fns as $index => $fn) {
                        if (($fn == '') || ($lastName[$paxtype][$index] == '')) {
                            throw new Exception(Tools::displayError('All passenger names must be filled.'));
                        }
                        if (! preg_match('/^[A-Z ]{2,20}$/i', $fn) || ! preg_match('/^[A-Z ]{2,20}$/i', $lastName[$paxtype][$index])) {
                            throw new Exception(Tools::displayError('You can use only letters A-Z and SPACE char in names. Allowed length is from 2 to 20 chars.'));
                        }
                        // check birth
                        if($paxtype != 'ADT') {
                            if(! $birth[$paxtype][$index]) throw new Exception(Tools::displayError('It is necessary to type birth dates of children and infants.'));
                            $birthMaxISO = strval(intval(substr($birth[$paxtype][$index], 6, 4)) + ($paxtype == 'CH' ? 12 : 2)) . '-' . substr($birth[$paxtype][$index], 3, 2) . '-' . substr($birth[$paxtype][$index], 0, 2);
                            if(isset($fgroup[2])) $cmpDate = $fgroup[2][count($fgroup[2]) - 1]['depDate'];
                            else $cmpDate = $fgroup[1][count($fgroup[1]) - 1]['depDate'];
                            $cmpDateISO = substr($cmpDate, 6, 4) . '-' . substr($cmpDate, 3, 2) . '-' . substr($cmpDate, 0, 2);
                            if($birthMaxISO <= $cmpDateISO) throw new Exception($birthError[$paxtype]);
                        }
                        // check salutation
                        if(! in_array($sal[$paxtype][$index], array('MR', 'MRS', 'MISS'))) throw new Exception(Tools::displayError('Bad salutation value.'));
                        $pax[$paxtype]['list'][$index] = array(
                            'first' => strtoupper($fn),
                            'last' => strtoupper($lastName[$paxtype][$index]),
                            'sal' => strtoupper($sal[$paxtype][$index]),
                            'ffcard' => (isset($ffcard[$paxtype][$index]) ? substr(strtoupper($ffcard[$paxtype][$index]), 0, 50) : ''),
                            'ffcardcode' => (isset($ffcardcode[$paxtype][$index]) ? substr(strtoupper($ffcardcode[$paxtype][$index]), 0, 2) : ''),
                            'birth' => (isset($birth[$paxtype][$index]) ? $birth[$paxtype][$index] : '')
                        );
                    }
                }

                if (count($this->context->controller->errors) == 0) {
                    $saved = ToureasonFlightDb::savePax($pax, $this->context->cookie->id_cart);
                    if ($saved === FALSE) {
                        throw new Exception(Tools::displayError('Unable to save passengers.'));
                    }
                    Tools::redirect('index.php?controller=order&step=' . self::STEP_HOTELS);
                }
            }

            else if(isset($pax['ADT']['list'])) {
                foreach($pax as $type => $fns) {
                    if(isset($fns['list'])) {
                        foreach($fns['list'] as $index => $fn) {
                            $firstName[$type][$index] = $fn['first'];
                            $lastName[$type][$index] = $fn['last'];
                            $sal[$type][$index] = $fn['sal'];
                            $ffcard[$type][$index] = $fn['ffcard'];
                            $ffcardcode[$type][$index] = $fn['ffcardcode'];
                            $birth[$type][$index] = $fn['birth'];
                        }
                    }
                }
            }
        }

        catch (Exception $ex) {
            $this->context->controller->errors[] = $ex->getMessage();
        }

        $this->context->smarty->assign(
            'form_params',
            [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'sal' => $sal,
                'ffcard' => $ffcard,
                'ffcardcode' => $ffcardcode,
                'birth' => $birth,
            ]);
    }

    protected function hotelSetPax(){
        $firstName = $lastName = $sal = array();
        $email = '';
        try {
            $fselect = ToureasonHotelDb::loadHotelSelect($this->context->cookie->id_cart);
            if(! $fselect || ! $fselect['json_hotel']) throw new Exception(Tools::displayError('Hotel not selected'));
            $selection = json_decode($fselect['json_hotel'], TRUE);
            $pax = json_decode($fselect['json_passengers'], TRUE);
            if($selection) $this->showSavedHotel($selection);
            $this->context->smarty->assign('passengers', $pax);

            if (Tools::isSubmit('first_name')) {
                $firstName = Tools::getValue("first_name");
                $lastName = Tools::getValue("last_name");
                $sal = Tools::getValue("sal");
                $email = Tools::getValue("email");
                if (preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i', $email) !== 1) {
                    throw new Exception(Tools::displayError('Bad format of e-mail address.'));
                }
                if (count($firstName) != $pax['cnt']) {
                    throw new Exception(Tools::displayError('Bad adults count.'));
                }
                $pax['list'] = array();
                foreach($firstName as $index => $fn) {
                    if (($fn == '') || ($lastName[$index] == '')) {
                        throw new Exception(Tools::displayError('All names must be filled.'));
                    }
                    if (! preg_match('/^[A-Z ]{2,20}$/i', $fn) || ! preg_match('/^[A-Z ]{2,20}$/i', $lastName[$index])) {
                        throw new Exception(Tools::displayError('You can use only letters A-Z and SPACE char in names. Allowed length is from 2 to 20 chars.'));
                    }
                    // check salutation
                    if(! in_array($sal[$index], array('MR', 'MRS', 'MISS'))) throw new Exception(Tools::displayError('Bad salutation value.'));

                    $pax['list'][$index] = array(
                        'first' => strtoupper($fn),
                        'last' => strtoupper($lastName[$index]),
                        'sal' => strtoupper($sal[$index])
                    );
                }
                $pax['email'] = $email;
                if (count($this->context->controller->errors) == 0) {
                    $saved = ToureasonHotelDb::savePax($pax, $this->context->cookie->id_cart);
                    if ($saved === FALSE) {
                        throw new Exception(Tools::displayError('Unable to save passengers.'));
                    }
                    Tools::redirect('index.php?controller=order&step=' . self::STEP_PAYMENT);
                }
            }
            else if(isset($pax['list'])) {
                foreach($pax['list'] as $index => $fn) {
                    $firstName[$index] = $fn['first'];
                    $lastName[$index] = $fn['last'];
                    $sal[$index] = $fn['sal'];
                }
                if(isset($pax['email'])) $email = $pax['email'];
            }
        }
        catch (Exception $ex) {
            $this->context->controller->errors[] = $ex->getMessage();
        }

        $this->context->smarty->assign(
            'form_params_names',
            [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'sal' => $sal,
                'email' => $email
            ]);
    }

    private static function mergePNR($type, $val, $pnr){
        $pnr[$type] = $val;
        return $pnr;
    }

/*    private function getPNR($type){
        // TODO: comment next line! Uncommented only for testing purposes.
        return false;
        $texts = array(
            'flight' => Tools::displayError('Flight cannot be changed.'),
            'hotel' => Tools::displayError('Hotel cannot be changed.'),
        );
        $pnr = ToureasonDb::loadPNR($this->context->cookie->id_cart);
        if($pnr) {
            if(isset($pnr[$type]) && $pnr[$type]) throw new Exception($texts[$type]);
            return $pnr;
        }
        return false;
    }
*/
    protected function ticketing(){
        $tickets = array();
        $orderId = intval(Tools::getValue('id_order'));
        $order = new Order($orderId);
        if(isset($order->id_cart) && ($order->id_cart > 0)) {
            $cartVals = ToureasonDb::loadCartTValues($order->id_cart);

            try {
                if($cartVals['id_flight']) {
                    $pnr = ToureasonDb::loadPNR($order->id_cart);
                    if(! $pnr) throw new Exception(Tools::displayError('Unable to load PNR'));
                    $ticket1 = new ToureasonRetrievePNR();
                    $ticket1->pnr = $pnr;
                    $ret = $ticket1->process();
                    $tickets = $ticket1->parseResponse($ret);
                    $this->lastSession = $ticket1->getSession();

                    if(count($tickets) == 0) {
                        $ticket2 = new ToureasonFlightMultiElementsPayment();
                        $ticket2->setSession($this->lastSession);
                        $ret = $ticket2->process();
                        $result = $ticket2->parseResponse($ret);
                        $this->lastSession = $ticket2->getSession();

                        $ticket3 = new ToureasonFlightIssueTicket();
                        $ticket3->indicator = 'TKT';
                        $ticket3->setSession($this->lastSession);
                        $ret = $ticket3->process();
                        $result = $ticket3->parseResponse($ret);
                        $this->lastSession = $ticket3->getSession();

                        $ticket4 = new ToureasonFlightIssueTicket();
                        $ticket4->indicator = 'ET';
                        $ticket4->setSession($this->lastSession);
                        $ret = $ticket4->process();
                        $result = $ticket4->parseResponse($ret);
                        $this->lastSession = $ticket4->getSession();
                    }

                    $this->endSession();
                }

                if($cartVals['id_hotel']) {
                    $fselect = ToureasonHotelDb::loadHotelSelect($order->id_cart);
                    $passengers = json_decode($fselect['json_passengers'], TRUE);
                    $selection = json_decode($fselect['json_hotel'], TRUE);

                    $this->checkHotelAvailability($selection, $passengers, false);

                    $step3 = new ToureasonHotelSell();
                    $step3->setSession($this->lastSession);
                    $step3->selection = $selection;
                    $step3->passengers = $passengers;
                    $ret = $step3->process();
                    $sold = $step3->parseResponse($ret);
                    $this->lastSession = $step3->getSession();
                    if(! $sold) throw new Exception(Tools::displayError('Unable to book hotel rooms.'));

                    $this->endSession();
                }
            }
            catch (Exception $ex) {
                $this->context->controller->errors[] = $ex->getMessage();
                $this->endSession();
            }
        }

        if(count($this->context->controller->errors) > 0) $this->ajaxDie(Tools::jsonEncode(['error'=>$this->context->controller->errors]));
        $this->ajaxDie(Tools::jsonEncode(['tickets'=>$tickets]));
    }

    protected function endSession() {
        if(isset($this->lastSession)) {
            $sout = new ToureasonSignOut();
            $sout->setSession($this->lastSession);
            $ret = $sout->process();
            $sout->parseResponse($ret);
        }
    }

    protected function getTickets($pnr=null, $cartId){
        $tickets = array();

        try {
            if(! isset($pnr)) {
                $pnr = ToureasonDb::loadPNR($cartId);
                if(! $pnr) throw new Exception(Tools::displayError('Unable to load PNR'));
            }

            if(isset($pnr['tickets'])) return $pnr['tickets'];

            $tn1 = new ToureasonRetrievePNR();
            $tn1->pnr = $pnr;
            $ret = $tn1->process();
            $tickets = $tn1->parseResponse($ret);
            $this->lastSession = $tn1->getSession();

            $this->endSession();

            if(! isset($pnr['tickets'])) {
                $pnr['tickets'] = $tickets;
                ToureasonDb::savePNR($pnr, $cartId);
            }
        }
        catch (Exception $ex) {
            $this->context->controller->errors[] = $ex->getMessage();
        }

        return $tickets;
    }

    protected function airportsLoad(){
        $ret = [];
        $term = Tools::getValue("term");

        if (strlen($term) > 2) {
            $locations = ToureasonFlightDb::airportsLoad($term);

            foreach($locations as $location) {
                $ret[] = ['label' => $location['name'] . " ({$location['country']})", 'value' => $location['iata_code']];
            }
        }

        $this->ajaxDie(Tools::jsonEncode($ret));
    }

    protected function hotelSearch(){

        $output = '';

        $next = intval(Tools::getValue("next"));
        $reload = intval(Tools::getValue("reload"));
        $sort = Tools::getValue("sort");
        if(! in_array($sort, array('price'))) $sort = 'price';
        self::$hotelSort = $sort;
        $sortDir = Tools::getValue("sortdir");
        if($sortDir == 'desc') self::$sortDir = -1;

        $lastSearch = array(
            'city' => strtoupper(Tools::getValue("city")),
            'city_auto' => Tools::getValue("city_auto"),
            'start_date' => Tools::getValue("start_date"),
            'end_date' => Tools::getValue("end_date"),
            'adults' => intval(Tools::getValue("adults")),
            'children' => intval(Tools::getValue("children")),
            'count_rooms' => intval(Tools::getValue("count_rooms"))
        );

        // not load from Amadeus API (load from DB)
        if(! $reload) {
            $result = ToureasonHotelDb::loadHotelSearchResult($this->context->cookie->id_cart);
            if($result) {
                $output = $this->getHotelSearchResultsOutput($result, $lastSearch['count_rooms'], $next, self::HOTEL_PAGINATION);
                $this->ajaxDie(Tools::jsonEncode(['output'=>$output, 'next'=>$this->loadNext]));
            }
            else $this->ajaxDie(Tools::jsonEncode(['output'=>'', 'next'=>$next]));
        }

        $startISO = $endISO = '';
        if($lastSearch['start_date']) $startISO = substr($lastSearch['start_date'], 6, 4) . '-' . substr($lastSearch['start_date'], 3, 2) . '-' . substr($lastSearch['start_date'], 0, 2);
        if($lastSearch['end_date']) $endISO = substr($lastSearch['end_date'], 6, 4) . '-' . substr($lastSearch['end_date'], 3, 2) . '-' . substr($lastSearch['end_date'], 0, 2);
        $lastSearch['nights'] = floor((strtotime($endISO) - strtotime($startISO)) / 86400);
        $errors = [];

        // validations
        if (preg_match('/^[a-z]{3,3}$/i', $lastSearch['city']) !== 1) {
            $errors[] = Tools::displayError('Bad city code.');
        }
        if ((preg_match('/^[0-9]{2,2}\.[0-9]{2,2}\.[0-9]{4,4}$/', $lastSearch['start_date']) !== 1) || ($startISO < date('Y-m-d'))) {
            $errors[] = Tools::displayError('Bad start date.');
        }
        if ((preg_match('/^[0-9]{2,2}\.[0-9]{2,2}\.[0-9]{4,4}$/', $lastSearch['end_date']) !== 1) || ($endISO < date('Y-m-d'))) {
            $errors[] = Tools::displayError('Bad end date.');
        }
        if ($endISO <= $startISO) {
            $errors[] = Tools::displayError('End date has to be after begin date.');
        }
        if (($lastSearch['adults'] < 1) || ($lastSearch['adults'] > 15)) {
            $errors[] = Tools::displayError('Bad count of adults.');
        }
        if (($lastSearch['count_rooms'] < 1) || ($lastSearch['count_rooms'] > 15)) {
            $errors[] = Tools::displayError('Bad count of rooms.');
        }
        if ($lastSearch['count_rooms'] > $lastSearch['adults']) {
            $errors[] = Tools::displayError('Too much rooms.');
        }

        if ((count($errors) == 0) && $lastSearch['city'] && $lastSearch['start_date'] && $lastSearch['end_date'] && $lastSearch['adults'] && $lastSearch['count_rooms']) {
            try {
                $hotel = new ToureasonHotelSearch();
                $hotel->city = $lastSearch['city'];
                $hotel->start_date = $lastSearch['start_date'];
                $hotel->end_date = $lastSearch['end_date'];
                $hotel->adults = $lastSearch['adults'];
                $hotel->children = $lastSearch['children'];
                $hotel->count_rooms = $lastSearch['count_rooms'];
                $ret = $hotel->process();
                $sr = $hotel->parseResponse($ret);
                ToureasonHotelDB::saveHotelSearch($lastSearch, $sr, $this->context->cookie->id_cart);
                $output = $this->getHotelSearchResultsOutput($sr, $lastSearch['count_rooms'], 0, self::HOTEL_PAGINATION);
            }
            catch (Exception $ex) {
                //echo $ex->faultcode;
                $errors[] = $ex->getMessage();
            }
        }
        if(count($errors) > 0) $this->ajaxDie(Tools::jsonEncode(['error'=>$errors]));

        $this->ajaxDie(Tools::jsonEncode(['output'=>$output, 'next'=>$this->loadNext]));
    }

    protected function getHotelDetail($hotelCode) {
        $det = new ToureasonHotelDetail();
        $det->id = $hotelCode;
        $resp = $det->process();
        return $det->parseResponse($resp);
    }

    protected function getHotelSearchResultsOutput($res, $count_rooms, $next, $cnt) {

        usort($res, array('self', 'cmpHotel'));

        $result = [];
        if(count($res) > 0) {
            $iter = 0;
            foreach($res as $index => $obj) {
                if(($iter >= $next) && ($iter < ($next + $cnt))) {
                    $res[$index]['detail'] = self::getHotelDetail($obj['HotelCode']);
                    $result[$index] = $res[$index];
                }
                $iter++;
            }
            ToureasonHotelDB::updateHotelSearchResult($res, $this->context->cookie->id_cart);
            if($iter > ($next + $cnt)) $this->loadNext = $next + $cnt;
        }

        $this->context->smarty->assign('hotels', $result);
        $this->context->smarty->assign('count_rooms', $count_rooms);
        ob_start();
        $this->smartyOutputContent(_PS_THEME_DIR_ . '_hotels_search_results.tpl');
        $output = ob_get_clean();
        $output = str_replace('</body></html>', '', $output);

        return $output;
    }

    protected function hotelSet(){
        $selection = [];
        $fp = ToureasonHotelDb::loadHotelSearch($this->context->cookie->id_cart);
        $sr = ToureasonHotelDb::loadHotelSearchResult($this->context->cookie->id_cart);
        $rooms = Tools::getValue("rooms");
        $price = 0;
        $roomsCnt = 0;
        if($sr && $fp && is_array($rooms)) {
            foreach($rooms as $hindex => $foo) {
                foreach($foo as $rindex => $rcnt) {
                    if($rcnt > 0) {
                        $iter = $sr[$hindex]['Rooms'][$rindex]['BookingCode'];
                        $selection[$iter] = $sr[$hindex];
                        $selection[$iter]['room'] = $sr[$hindex]['Rooms'][$rindex];
                        $selection[$iter]['room']['cnt'] = (int)$rcnt;
                        $selection[$iter]['nights'] = $fp['nights'];
                        $selection[$iter]['adults'] = $fp['adults'];
                        $selection[$iter]['count_rooms'] = $fp['count_rooms'];
                        unset($selection[$iter]['Rooms']);
                        unset($selection[$iter]['RoomStayRPH']);
                        $price += $selection[$iter]['room']['cnt'] * $selection[$iter]['room']['TotalPrice'];
                        $roomsCnt += $selection[$iter]['room']['cnt'];
                    }
                }
            }
        }

        if (count($selection) > 0) {
            try {
                $fselect = ToureasonHotelDb::loadHotelSelect($this->context->cookie->id_cart);
                if($fselect) {
                    $passengers = json_decode($fselect['json_passengers'], TRUE);
                }
                $passengers['cnt'] = $fp['adults'];
                $passengers['roomsCnt'] = $roomsCnt;
                $passengers['start_date'] = $fp['start_date'];
                $passengers['end_date'] = $fp['end_date'];

                $this->checkHotelAvailability($selection, $passengers);

                // write flight to DB
                $hotelId = ToureasonHotelDb::saveHotelSelect(
                    $selection,
                    $passengers,
                    $this->context->cookie->id_cart,
                    $fp['nights'] * $price
                );
                if ($hotelId === FALSE) {
                    $this->context->controller->errors[] = Tools::displayError('Unable to save hotel selection.');
                }
                else {
                    Tools::redirect('index.php?controller=order&step=' . self::STEP_HOTELS . '&action=hotelsetpax');
                }
            }
            catch (Exception $ex) {
                $this->context->controller->errors[] = $ex->getMessage();
                $this->endSession();
            }
        }
        else {
            //dump($rooms);
            //dump($selection);
            //dump($sr);
            $this->context->controller->errors[] = Tools::displayError('Nothing selected.');
        }
    }

    protected function hotelNext(){
        $cartVals = ToureasonDb::loadCartTValues($this->context->cookie->id_cart);
        if(! $cartVals['id_flight'] && ! $cartVals['id_hotel']) {
            $this->context->controller->errors[] = Tools::displayError('You have to select flight or hotel to continue.');
        }
        else Tools::redirect('index.php?controller=order&step=' . self::STEP_PAYMENT);
    }

    protected function citiesLoad(){
        $ret = [];
        $term = Tools::getValue("term");

        if (strlen($term) > 2) {
            $locations = ToureasonHotelDb::citiesLoad($term);

            foreach($locations as $location) {
                $ret[] = ['label' => $location['name'] . " ({$location['country']})", 'value' => $location['iata_code']];
            }
        }

        $this->ajaxDie(Tools::jsonEncode($ret));
    }


    public function setMedia(){
        parent::setMedia();
        if ($this->step == self::STEP_FLIGHTS) {
            $this->addCSS(_THEME_CSS_DIR_ . 'order-flight.css');
            $this->addCSS(_THEME_CSS_DIR_ . 'jquery-ui/jquery-ui.min.css');
            $this->addJS(_THEME_JS_DIR_ . 'jquery-ui.min.js');
            $this->addJS(_THEME_JS_DIR_ . 'order-flight.js');
        }
        if ($this->step == self::STEP_HOTELS) {
            $this->addCSS(_THEME_CSS_DIR_ . 'order-hotel.css');
            $this->addCSS(_THEME_CSS_DIR_ . 'jquery-ui/jquery-ui.min.css');
            $this->addJS(_THEME_JS_DIR_ . 'jquery-ui.min.js');
            $this->addJS(_THEME_JS_DIR_ . 'order-hotels.js');
        }
        if ($this->step == self::STEP_PAYMENT) {
            $this->addCSS(_THEME_CSS_DIR_ . 'order-flight.css');
            $this->addJS(_THEME_JS_DIR_ . 'jquery-ui.min.js');
            $this->addJS(_THEME_JS_DIR_ . 'order-flight.js');
            $this->addJS(_THEME_JS_DIR_ . 'order-summary.js');
            $this->addJS('/js/validate.js');
        }
    }
}
