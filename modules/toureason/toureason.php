<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

class TouReason extends Module{

    protected static $confFields = [
        'TOUREASON_EXCHANGE_RATE_EUR2CZK' => [
            'default' => 27.55,
            'label' => 'Exchange rate from EUR to CZK',
            'type' => 'float',
        ],
        'TOUREASON_FLIGHT_SEARCH_CNT' => [
            'default' => 30,
            'label' => 'Max count of recommended flights',
            'type' => 'int',
        ],
        'TOUREASON_HOTEL_SEARCH_CNT' => [
            'default' => 30,
            'label' => 'Max count of searched hotels',
            'type' => 'int',
        ],
        'TOUREASON_FLIGHT_PERC' => [
            'default' => 3.0,
            'label' => 'Add percentage for flight price',
            'type' => 'float',
        ],
        'TOUREASON_HOTEL_PERC' => [
            'default' => 0.0,
            'label' => 'Add percentage for hotel price',
            'type' => 'float',
        ],
        'TOUREASON_CC_TYPE' => [
            'default' => 'AX',
            'label' => 'Credit card type (2 letters)',
            'type' => 'string',
        ],
        'TOUREASON_CC_NUMBER' => [
            'default' => '371449635311004',
            'label' => 'Credit card number',
            'type' => 'string',
        ],
        'TOUREASON_CC_EXPIRE' => [
            'default' => '0616',
            'label' => 'Credit card expiration (4 digits)',
            'type' => 'string',
        ],
        'TOUREASON_LOADING_ARTICLE_ID' => [
            'default' => 6,
            'label' => 'CMS content id for loading modal',
            'type' => 'int',
        ],

    ];

    public function __construct(){
        $this->name = 'toureason';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = '1. Web IT s.r.o.';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = ['min' => '1.6', 'max' => _PS_VERSION_];
        $this->bootstrap = TRUE;

        parent::__construct();

        $this->displayName = $this->l('TouReason flight ticket and hotel module');
        $this->description = $this->l('TouReason flight ticket and hotel module.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!ConfigurationCore::get('MYMODULE_NAME')) {
            $this->warning = $this->l('No name provided');
        }
    }

    public function install(){
        foreach(self::$confFields as $key => $field) {
            Configuration::updateValue($key, $field['default']);
        }

        if ($this->loadSQLFile(dirname(__FILE__) . '/install/api_log.struct.sql') != 1) {
            return FALSE;
        }

        if ($this->loadSQLFile(dirname(__FILE__) . '/install/pnr.struct.sql') != 1) {
            return FALSE;
        }

        if ($this->loadSQLFile(dirname(__FILE__) . '/install/airlines.struct.sql') == 1) {
            if ($this->loadSQLFile(dirname(__FILE__) . '/install/airlines.dat.sql') != 1) {
                return FALSE;
            }
        } else {
            return FALSE;
        }

        if ($this->loadSQLFile(dirname(__FILE__) . '/install/airports.struct.sql') == 1) {
            if ($this->loadSQLFile(dirname(__FILE__) . '/install/airports.dat.sql') != 1) {
                return FALSE;
            }
        } else {
            return FALSE;
        }

        if ($this->loadSQLFile(dirname(__FILE__) . '/install/flight_search_result.struct.sql') != 1) {
            return FALSE;
        }
        if ($this->loadSQLFile(dirname(__FILE__) . '/install/flight_select.struct.sql') != 1) {
            return FALSE;
        }

        if (!$this->loadSQLFile(__DIR__ . '/install/hotel_search_result.struct.sql')) {
            return FALSE;
        }

        if (!$this->loadSQLFile(__DIR__ . '/install/hotel_select.struct.sql')) {
            return FALSE;
        }

        if (!self::checkColumnExists(_DB_NAME_, _DB_PREFIX_ . 'cart', 'id_pnr')) {
            if ($this->loadSQLFile(dirname(__FILE__) . '/install/cart-id_pnr.alter.sql') != 1) {
                return FALSE;
            }
        }

        if (!self::checkColumnExists(_DB_NAME_, _DB_PREFIX_ . 'cart', 'id_flight')) {
            if ($this->loadSQLFile(dirname(__FILE__) . '/install/cart-id_flight.alter.sql') != 1) {
                return FALSE;
            }
        }

        if (!self::checkColumnExists(_DB_NAME_, _DB_PREFIX_ . 'cart', 'flight_price')) {
            if ($this->loadSQLFile(dirname(__FILE__) . '/install/cart-flight_price.alter.sql') != 1) {
                return FALSE;
            }
        }

        if (!self::checkColumnExists(_DB_NAME_, _DB_PREFIX_ . 'cart', 'id_hotel')) {
            if ($this->loadSQLFile(dirname(__FILE__) . '/install/cart-id_hotel.alter.sql') != 1) {
                return FALSE;
            }
        }

        if (!self::checkColumnExists(_DB_NAME_, _DB_PREFIX_ . 'cart', 'hotel_price')) {
            if ($this->loadSQLFile(dirname(__FILE__) . '/install/cart-hotel_price.alter.sql') != 1) {
                return FALSE;
            }
        }

        return (parent::install());
    }

    private function loadSQLFile($file){
        $fcontent = file_get_contents($file);
        $fcontent = str_replace('PREFIX_', _DB_PREFIX_, $fcontent);
        $sqls = preg_split("/;\s*[\r\n]+/", $fcontent);
        $ret = TRUE;
        foreach($sqls as $sql) {
            if ($sql) {
                $ret &= Db::getInstance()->execute(trim($sql));
            }
        }
        return $ret;
    }

    private static function checkColumnExists($db, $table, $col){
        $sql =
            "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS " .
            "WHERE table_schema = '$db' AND table_name = '$table' AND column_name = '$col' ";
        $rows = Db::getInstance()->executeS($sql);
        if (count($rows) == 0) {
            return FALSE;
        }
        return TRUE;
    }

    public function uninstall(){
        foreach(self::$confFields as $key => $field) {
            Configuration::deleteByName($key);
        }

        $this->loadSQLFile(dirname(__FILE__) . '/uninstall/api_log.sql');
        $this->loadSQLFile(dirname(__FILE__) . '/uninstall/pnr.sql');
        $this->loadSQLFile(dirname(__FILE__) . '/uninstall/airlines.sql');
        $this->loadSQLFile(dirname(__FILE__) . '/uninstall/airports.sql');
        $this->loadSQLFile(dirname(__FILE__) . '/uninstall/flight_search_result.sql');
        $this->loadSQLFile(dirname(__FILE__) . '/uninstall/flight_select.sql');
        $this->loadSQLFile(dirname(__FILE__) . '/uninstall/hotel_select.sql');
        $this->loadSQLFile(dirname(__FILE__) . '/uninstall/hotel_search_result.sql');

        return (parent::uninstall());
    }

    public function getContent(){
        $html = '';
        if (Tools::isSubmit('submitModule')) {
            foreach(self::$confFields as $key => $field) {
                $val = Tools::getValue($key);
                if ($field['type'] == 'int') {
                    $val = intval($val);
                } else {
                    if ($field['type'] == 'float') {
                        $val = floatval($val);
                    }
                }
                Configuration::updateValue($key, Tools::getValue($key));
            }
            $html = $this->displayConfirmation($this->l('Configuration updated'));
        }

        return $html . $this->renderForm();
    }

    public function renderForm(){
        foreach(self::$confFields as $key => $field) {
            $input[] = [
                'type' => 'text',
                'label' => $this->l($field['label']),
                'name' => $key,
                'size' => 10,
                'required' => TRUE,
            ];
        }
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ],
                'input' => $input,
                'submit' => [
                    'title' => $this->l('Save'),
                    'class' => 'button',
                ]
            ],
        ];

        $helper = new HelperForm();
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = $lang->id;
        $this->fields_form = [];

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', FALSE) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = [
            'fields_value' => [],
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        ];

        $helper->show_toolbar = FALSE;

        foreach(self::$confFields as $key => $field) {
            $helper->tpl_vars['fields_value'][$key] = Tools::getValue($key, Configuration::get($key));
        }

        return $helper->generateForm([$fields_form]);
    }

}
