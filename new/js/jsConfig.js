$('.order-list-item-more-button').click(function () {

    if($(this).hasClass('active')){
        $(this).removeClass('active').text('Show detail');
    }else{
        $(this).addClass('active').text('Hide detail');
    }
    $(this).closest('table').find('.order-list-item-more-list').toggle();

    return false;
});

$(function () {
    setOrderListWidth();
});

$(window).resize(function () {
    setOrderListWidth();
});

function setOrderListWidth() {
    if ($('.order-list').length) {
        var orderListContent = $('.order-list-content');
        var orderListHeader = $('.order-list-header');
        var control = orderListContent.children('.order-list-item').first();
        var col = [0];

        orderListContent.children('.order-list-item').each(function () {
            for (var i = 1; i <= 9; i++) {
                $(this).find('.col-' + i).removeAttr('style');
            }
        });

        for (var i = 1; i <= 9; i++) {
            col.push(control.find('.col-' + i).width());

        }

        orderListContent.children('.order-list-item').each(function () {
            for (var i = 1; i <= 9; i++) {
                $(this).find('.col-' + i).width(col[i]);
            }
        });

        orderListHeader.find('.col-1').width(col[1] + col[2] + col[3] + col[4] + col[5]);
        orderListHeader.find('.col-2').width(col[6]);
        orderListHeader.find('.col-3').width(col[7]);
    }

    ('.dataTable').DataTable();
}