<?php
require_once(_PS_MODULE_DIR_ . 'toureason/classes/Db/ToureasonDb.php');
class Cart extends CartCore {
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    /*
    * module: toureason
    * date: 2016-03-10 22:09:31
    * version: 1.0.0
    */
    public function getProducts($refresh = false, $id_product = false, $id_country = null) {
        if(strpos($_SERVER['REQUEST_URI'], '/modules/paypal/') === false) return parent::getProducts($refresh, $id_product, $id_country);
        $this->_products = parent::getProducts(true, $id_product, $id_country);
        $tvals = ToureasonDb::loadCartTValues($this->id);
        if($tvals['flight_price'] > 0) {
            $this->_products[] = array(
                'id_product' => $tvals['id_flight'],
                'name' => 'Flight',
                'description_short' => '...',
                'price_wt' => $tvals['flight_price'],
                'quantity' => 1,
                'cart_quantity' => 1,
                'is_virtual' => false,
                'id_address_delivery' => $this->_products[0]['id_address_delivery'],
                'id_shop' => $this->_products[0]['id_shop'],
                'id_product_attribute' => 0,
                'additional_shipping_cost' => 0,
                'weight_attribute' => null,
                'weight' => 0,
            );
        }
        if($tvals['hotel_price'] > 0) {
            $this->_products[] = array(
                'id_product' => $tvals['id_hotel'],
                'name' => 'Hotel',
                'description_short' => '...',
                'price_wt' => $tvals['hotel_price'],
                'quantity' => 1,
                'cart_quantity' => 1,
                'is_virtual' => false,
                'id_address_delivery' => $this->_products[0]['id_address_delivery'],
                'id_shop' => $this->_products[0]['id_shop'],
                'id_product_attribute' => 0,
                'additional_shipping_cost' => 0,
                'weight_attribute' => null,
                'weight' => 0,
            );
        }
        return $this->_products;
    }
}
