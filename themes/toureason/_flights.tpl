{capture name=path}{l s='Your flight tickets'}{/capture}

<h1 id="cart_title" class="page-heading">{l s='Your flight tickets'}
</h1>

<div id="loading_content">
    {if isset($loading)}
        {$loading}
    {/if}
</div>

{if isset($account_created)}
    <p class="alert alert-success">
        {l s='Your account has been created.'}
    </p>
{/if}

{assign var='current_step' value='flights'}
{assign var='cabins' value=['M'=>{l s="Economy Standard" },'W'=>{l s="Economy Premium" },'Y'=>{l s="Economy" },'C'=>{l s="Business" },'F'=>{l s="First" }]}
{include file="$tpl_dir./order-steps.tpl"}
{include file="$tpl_dir./errors.tpl"}

{*<h3>{l s="Search form"}</h3>*}


</div>
<div class="col-lg-3 col-lg-push-9 cart-box" id="cartBox">
    <div class="main">
        {include file='./_order_basket_mini.tpl'}
        <div class="cart-box-footer">
            <div class="row">
                <form action="{$link->getPageLink('order.php', true)|escape:'html':'UTF-8'}" method="post">
                    <div class="col-md-12">
                        <button type="submit" name="processFlights" class="btn btn-arrow btn-info btn-block">
                            <span>{l s='Proceed to checkout'}</span>
                        </button>

                        <div class="col-md-12">
                            {if (isset($fgroup)) }
                                <input type="hidden" class="hidden" name="step" value="101"/>
                                <input type="hidden" class="hidden" name="action" value="flightsetpax"/>
                            {else}
                                <input type="hidden" class="hidden" name="step" value="102"/>
                            {/if}
                            <input type="hidden" name="back" value="{$back}"/>


                            <a href="{$link->getPageLink('order', true)|escape:'html':'UTF-8'}" class="btn btn-default btn-block">
                                {l s='Backward'}
                            </a>
                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-9 col-lg-pull-3 mainContent">
    <div class="order-filter order-filter-state-2">

        <button class="btn btn-block btn-open-filter btn-rounded">{l s="Flight filter"}</button>

        <form id="flight_search_form" class="flights-form" action="{$link->getPageLink('order', true, NULL, "&step=101")|escape:'html':'UTF-8'}" method="post">
            <input type="hidden" name="from" value="{if (isset($form_params.from))}{$form_params.from}{/if}"/>
            <input type="hidden" name="to" value="{if (isset($form_params.to))}{$form_params.to}{/if}"/>

            <div class="row row-sm">
                <div class="col-xs-12 col-sm-6 col-md-5 box-1">
                    <div class="form-group">
                        <label for="from_auto">
                            {l s="From"}:
                        </label>
                        <input placeholder="{l s="From"}" class="form-control" type="text" name="from_auto" id="from_auto" value="{if (isset($form_params.from_auto))}{$form_params.from_auto}{/if}"/>
                        <span class="icon"></span>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-5 box-2">
                    <div class="form-group">
                        <label for="to_auto">
                            {l s="To"}:
                        </label>
                        <input placeholder="{l s="To"}" class="form-control" type="text" name="to_auto" id="to_auto" value="{if (isset($form_params.to_auto))}{$form_params.to_auto}{/if}"/>
                        <span class="icon"></span>
                    </div>
                </div>

                {*<p class="info-title">{l s="Advanced filter"}:</p>*}
                <div class="col-xs-12 col-sm-12 col-md-2 box-6">
                    <div class="form-group">
                        <label for="class">
                            {l s="Class"}:
                        </label>
                        <select name="class" id="class" class="form-control">
                            <option value="">All class</option>
                            <option value="M"
                                    {if (isset($form_params.class) && ($form_params.class == 'M'))}selected{/if}>{$cabins['M']}</option>
                            <option value="W"
                                    {if (isset($form_params.class) && ($form_params.class == 'W'))}selected{/if}>{$cabins['W']}</option>
                            <option value="C"
                                    {if (isset($form_params.class) && ($form_params.class == 'C'))}selected{/if}>{$cabins['C']}</option>
                            <option value="F"
                                    {if (isset($form_params.class) && ($form_params.class == 'F'))}selected{/if}>{$cabins['F']}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row row-sm">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 box-7">
                    <div class="form-group">
                        <label for="departure">
                            {l s="Departure date"}:
                        </label>
                        <input class="form-control" placeholder="{l s="Departure"}" type="text" name="departure" id="departure" value="{if (isset($form_params.departure))}{$form_params.departure}{/if}" maxlength="10"/>
                        <span class="icon"></span>
                    </div>
                </div>


                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 box-8 ">
                    <div class="form-group">

                        <!--label>
        {l s="Return ticket"}:
        <input type="checkbox" name="return" value="1" {if (isset($form_params.return) && $form_params.return)}checked{/if} />
    </label-->

                        <label for="return_date">
                            {l s="Return date"}:
                        </label>
                        <input placeholder="{l s="Return"}" class="form-control" type="text" name="return_date" id="return_date"
                               value="{if (isset($form_params.return_date))}{$form_params.return_date}{/if}" maxlength="10"/>
                        <span class="icon"></span>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 box-3">
                    <div class="form-group">
                        <label for="adults">
                            {l s="Adults"}:
                        </label>
                        <select name="adults" id="adults" class="form-control">
                        {for $cnt=1 to 9}
                            <option value="{$cnt}" {if (isset($form_params.adults) && ($form_params.adults == $cnt))}selected{/if}>{$cnt} {if $cnt == 1}{l s='Adult'}{else}{l s='Adults'}{/if}</option>
                        {/for}
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 box-4">
                    <div class="form-group">
                        <label for="children">
                            {l s="Children"}:
                        </label>
                        <select name="children" id="children" class="form-control">
                        {for $cnt=0 to 9}
                            <option value="{$cnt}" {if (isset($form_params.children) && ($form_params.children == $cnt))}selected{/if}>{$cnt} {if $cnt == 1}{l s='Child'}{else}{l s='Children'}{/if}</option>
                        {/for}
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 box-5">
                    <div class="form-group">
                        <label for="infants">
                            {l s="Infants"}:
                        </label>
                        <select name="infants" id="infants" class="form-control">
                        {for $cnt=0 to 9}
                            <option value="{$cnt}" {if (isset($form_params.infants) && ($form_params.infants == $cnt))}selected{/if}>{$cnt} {if $cnt == 1}{l s='Infant'}{else}{l s='Infants'}{/if}</option>
                        {/for}
                        </select>
                    </div>
                </div>


                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 box-9">
                    <div class="form-group">
                        <p class="checkbox">
                            <input class="form-control" type="checkbox" name="direct" id="direct" value="1" {if (isset($form_params.direct) && $form_params.direct)}checked{/if} />
                            <label for="direct">
                                {l s="Only direct flights"}
                            </label>
                        </p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row row-sm collapse" id="collapseExample">

                <div class="col-xs-12 col-sm-6 col-md-3 box-10">


                    <div class="form-group">
                        <label for="da_time">{l s="Preferred time of"}:</label>
                        <select name="da_time" id="da_time" class="form-control">
                            <option value="">Direction</option>
                            <option value="TD"
                                    {if (isset($form_params.da_time) && ($form_params.da_time == 'TD'))}selected{/if}>{l s="Departure"}</option>
                            <option value="TA"
                                    {if (isset($form_params.da_time) && ($form_params.da_time == 'TA'))}selected{/if}>{l s="Arrival"}</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 box-11">
                    <div class="form-group">
                        <label for="pref_time" class="hidden-xs">&nbsp;</label>
                        <select name="pref_time" id="pref_time" class="form-control">
                            <option value="">Time</option>
                            <option value="M"
                                    {if (isset($form_params.pref_time) && ($form_params.pref_time == 'M'))}selected{/if}>{l s="Morning (06-12h)"}</option>
                            <option value="A"
                                    {if (isset($form_params.pref_time) && ($form_params.pref_time == 'A'))}selected{/if}>{l s="Afternoon (12-18h)"}</option>
                            <option value="E"
                                    {if (isset($form_params.pref_time) && ($form_params.pref_time == 'E'))}selected{/if}>{l s="Evening (18-22h)"}</option>
                            <option value="N"
                                    {if (isset($form_params.pref_time) && ($form_params.pref_time == 'N'))}selected{/if}>{l s="Night (22-06h)"}</option>
                        </select>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 box-12">

                    <div class="form-group">
                        <label for="return_da_time"> {l s="Preferred time of"}: </label>
                        <select name="return_da_time" class="form-control">
                            <option value="">Direction</option>
                            <option value="TD"
                                    {if (isset($form_params.return_da_time) && ($form_params.return_da_time == 'TD'))}selected{/if}>{l s="Departure"}</option>
                            <option value="TA"
                                    {if (isset($form_params.return_da_time) && ($form_params.return_da_time == 'TA'))}selected{/if}>{l s="Arrival"}</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 box-13">
                    <div class="form-group">
                        <label for="return_pref_time" class="hidden-xs">&nbsp;</label>
                        <select name="return_pref_time" class="form-control">
                            <option value="">Time</option>
                            <option value="M"
                                    {if (isset($form_params.return_pref_time) && ($form_params.return_pref_time == 'M'))}selected{/if}>{l s="Morning (06-12h)"}</option>
                            <option value="A"
                                    {if (isset($form_params.return_pref_time) && ($form_params.return_pref_time == 'A'))}selected{/if}>{l s="Afternoon (12-18h)"}</option>
                            <option value="E"
                                    {if (isset($form_params.return_pref_time) && ($form_params.return_pref_time == 'E'))}selected{/if}>{l s="Evening (18-22h)"}</option>
                            <option value="N"
                                    {if (isset($form_params.return_pref_time) && ($form_params.return_pref_time == 'N'))}selected{/if}>{l s="Night (22-06h)"}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row row-sm">

                <div class="col-xs-12 col-sm-12 col-md-12 box-14">
                    <button class="pull-left button btn btn-default button-medium btn-formSearch" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        {l s="Advanced filter"}
                    </button>
                    <button id="flight_search_btn" type="button" onClick="searchFlightsNew();" class="pull-right button btn btn-info button-medium btn-formSearch" data-text-search="{l s='Search'}"
                            data-text-loading="{l s='Loading...'}"></button>

                </div>
            </div>
            <div class="clear"></div>
        </form>
    <div class="clear"></div>
    </div>
    <div class="ticekts">

        {if (isset($fgroup)) }
        <div class="flight-detail flight-detail-2 flight-detail-header">
            <div class="row">
                <div class="col-xs-6">
                    <h3>{l s="Selected flight"}</h3>
                </div>
                <div class="col-xs-6 flight_check_buttons text-right">
                    <a class="unset-flight"
                       href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=101&action=flightunset")|escape:'html':'UTF-8'}">{l s="Unset flight"}</a>

</div>
</div>
            </div>
            {include file='./_flights_inc_selflight.tpl'}

        {*<div class="row row-sm flight_check_buttons">*}
            {*<div class="col-sm-12 col-md-3">*}


            {*</div>*}
            {*<div class="col-sm-12 col-md-3 flight_check_button">*}

{*<a class="btn btn-info  btn-block btn-rounded" href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=101&action=flightsetpax")|escape:'html':'UTF-8'}">{l s="Check passengers"}</a>*}

            {*</div>*}
        {*</div>*}
{*</div>*}
{/if}


<div class="order-list">
<form id="flight_set" action="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=101&action=flightset")|escape:'html':'UTF-8'}" method="post">
    <input type="hidden" name="fgroup"/>

    <div>
        <table class="flight_results flight_results_table_list" id="flight_results_table" data-next-index="0">
        </table>
    </div>

    <div class="text-center buttons-more-box">
        <button id="flight_search_btn_next" type="button" data-sorting="price" onClick="searchFlights();" class="btn btn-showmore" data-text-search="{l s='Show more'}" data-text-loading="{l s='Loading...'}"></button>
</div>
</form>
</div>
</div>

{* eu-legal *}
{hook h="displayBeforeShoppingCartBlock"}
<!-- end order-detail-content -->




<div class="clear"></div>
<div class="cart_navigation_extra">
<div id="HOOK_SHOPPING_CART_EXTRA">{if isset($HOOK_SHOPPING_CART_EXTRA)}{$HOOK_SHOPPING_CART_EXTRA}{/if}</div>
</div>
{strip}
    {addJsDef deliveryAddress=$cart->id_address_delivery|intval}
    {addJsDefL name=txtProduct}{l s='product' js=1}{/addJsDefL}
    {addJsDefL name=txtProducts}{l s='products' js=1}{/addJsDefL}
{/strip}
