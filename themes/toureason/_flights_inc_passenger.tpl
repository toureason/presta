{for $iter=1 to $passengers[$paxtype]['cnt']}
    <div class="col-xs-12 col-sm-2 col-md-1">
        <div class="form-group">
            <label for="sal_{$paxtype}_{$iter}">
                {l s="Salutation"}:
            </label>
            <select name="sal[{$paxtype}][{$iter}]" id="sal_{$paxtype}_{$iter}" class="form-control">
                <option value="MR"
                        {if (isset($form_params.sal[$paxtype][$iter]) && ($form_params.sal[$paxtype][$iter] == 'MR'))}selected{/if}>{l s="Mr."}</option>
                <option value="MRS"
                        {if (isset($form_params.sal[$paxtype][$iter]) && ($form_params.sal[$paxtype][$iter] == 'MRS'))}selected{/if}>{l s="Mrs."}</option>
                <option value="MISS"
                        {if (isset($form_params.sal[$paxtype][$iter]) && ($form_params.sal[$paxtype][$iter] == 'MISS'))}selected{/if}>{l s="Ms."}</option>
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-4">
        <div class="form-group">
            <label for="first_name_{$paxtype}_{$iter}">
                {l s="First name"}:
            </label>
            <input class="form-control" placeholder="{l s="First name"}" type="text" name="first_name[{$paxtype}][{$iter}]" id="first_name_{$paxtype}_{$iter}"
                   value="{if (isset($form_params.first_name[$paxtype][$iter]))}{$form_params.first_name[$paxtype][$iter]}{/if}"/>
        </div>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-4">
        <div class="form-group">
            <label for="last_name_{$paxtype}_{$iter}">
                {l s="Last name"}:
            </label>
            <input class="form-control" type="text" placeholder="{l s="Last name"}" name="last_name[{$paxtype}][{$iter}]" id="last_name_{$paxtype}_{$iter}"
                   value="{if (isset($form_params.last_name[$paxtype][$iter]))}{$form_params.last_name[$paxtype][$iter]}{/if}"/>
        </div>
    </div>
    {if $paxtype != 'INF'}
        <div class="clearfix"></div>
        <div class="col-md-12">
            <strong>FF card</strong>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5">
            <div class="form-group">
                <label for="ffcardcode_{$paxtype}_{$iter}">
                    {l s="FF card company code"}:
                </label>
                <input class="form-control" placeholder="{l s="FF card company code"}" type="text" name="ffcardcode[{$paxtype}][{$iter}]" id="ffcardcode_{$paxtype}_{$iter}"
                       value="{if (isset($form_params.ffcardcode[$paxtype][$iter]))}{$form_params.ffcardcode[$paxtype][$iter]}{/if}" maxlength="2"/>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
                <label for="ffcard_{$paxtype}_{$iter}">
                    {l s="FF card no."}:
                </label>
                <input class="form-control" type="text" name="ffcard[{$paxtype}][{$iter}]" id="ffcard_{$paxtype}_{$iter}"
                       value="{if (isset($form_params.ffcard[$paxtype][$iter]))}{$form_params.ffcard[$paxtype][$iter]}{/if}"/>
            </div>
        </div>
    {/if}
    {if $paxtype != 'ADT'}
        <div class="col-md-12"></div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
                <label for="birth_{$paxtype}_{$iter}">
                    {l s="Date of birth"}:
                </label>
                <input class="form-control data-birth-{$paxtype}" placeholder="{l s="Date of birth"}" type="text" name="birth[{$paxtype}][{$iter}]" id="birth_{$paxtype}_{$iter}"
                       value="{if (isset($form_params.birth[$paxtype][$iter]))}{$form_params.birth[$paxtype][$iter]}{/if}" maxlength="10"/>
            </div>
        </div>
    {/if}
    <div class="clearfix"></div>
{/for}
