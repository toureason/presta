{if (isset($fgroup)) }
    {assign var='cabins' value=['M'=>{l s="Economy Standard" },'W'=>{l s="Economy Premium" },'Y'=>{l s="Economy" },'C'=>{l s="Business" },'F'=>{l s="First" }]}
    {assign var='fbaUnits' value=['K'=>{l s="kilograms"},'L'=>{l s="pounds"}]}
    {if isset($fgroup.2)}{assign var='fgs' value=[$fgroup.1, $fgroup.2]}
    {else}{assign var='fgs' value=[$fgroup.1]}
    {/if}
    {assign var='paxcnt' value=$flight_pax.ADT.cnt + $flight_pax.CH.cnt + $flight_pax.INF.cnt}
    <div class="flight-detail flight-detail-2 flight-detail-3">
        <table class="flight_results">

            <tr class="company-name">
                <td colspan="7">
                    {assign var='fcdiv' value=''}
                    {assign var='companyUsed' value='[]'}
                    {foreach $fgs as $fgseg=>$fgroup2}
                        {foreach $fgroup2 as $findex=>$flight}
                            {if ! isset($companyUsed[$flight.company])}
                            {$fcdiv}{$companies[$flight.company]['name']}
                            {assign var='fcdiv' value=' / '}
                            {append var='companyUsed' value='1' index=$flight.company}
                            {/if}
                        {/foreach}
                    {/foreach}
                </td>
            </tr>
            <tr class="passanger-desktop">
                <td colspan="7">{$paxcnt} {if $paxcnt == 1}{l s='passenger'}{else}{l s='passengers'}{/if}</td>
            </tr>
            {foreach $fgs as $fsegindex=>$fgroup2}
                <tr class="flightInfo-row">
                    <td style="width: 350px" class="flightInfo">
                        <span class="date">{$fgroup2.0.depDate}</span>
                        <span class="passanger-mobile">{$paxcnt} {if $paxcnt == 1}{l s='passenger'}{else}{l s='passengers'}{/if}</span>
                        <span class="mobile-style">
                            <strong class="time">{$fgroup2.0.depTime}</strong>
                            <span class="city">{$fgroup2.0.loc.0.id}</span>
                        </span>
                        <span class="arrow">&#8594;</span>
                        <strong class="time3">
                            <span class="time1">{$fgroup2[$fgroup2|@count - 1].arrTime}</span>
                            <span class="city1">{$fgroup2[$fgroup2|@count - 1].loc.1.id}</span>
                        </strong>
                    </td>
                    <td class="duration">{$fgroup2.0.duration}</td>
                    <td class="ticket-numbers">
                        {assign var='fcdiv' value=''}
                        {foreach $fgroup2 as $findex=>$flight}{$fcdiv}#{$flight.number}{assign var='fcdiv' value=', '}{/foreach}
                    </td>
                    {if $fsegindex == 0}<td rowspan="2" class="order-list-price-box" style="width: 90px"><span class="order-list-price">{$fgroup2.0.price} &#8364;</span></td>{/if}
                </tr>
            {/foreach}
            <tr class="buttons-more table-last" id="row-bg-sel">
                <td class="first last " colspan="7">
                    <button id="flight_details_btn_sel" type="button" onclick="showFlightDetails('sel');">{l s='Show details'}</button>
                    <button id="flight_details_btn_2_sel" type="button" onclick="hideFlightDetails('sel');" style="display: none">{l s='Hide details'}</button>
                </td>
            </tr>
            <tr id="flight_details_sel" class="flight_details">
                <td class="first last" colspan="4">
                    <table>
                        <tbody>
                        {foreach $fgs as $fgseg=>$fgroup2}
                            {foreach $fgroup2 as $findex=>$flight}
                            <tr>
                                <th style="width: 55px; border-bottom: none" class="hidden-xs hidden-sm"></th>
                                <th style="width: 104px" class="flight-direction">
                                    {if ($fgseg == 0)}{l s='Depart'}
                                    {else}{l s='Return'}
                                    {/if}
                                </th>
                                <th style="max-width: 300px" class="flight-company">{$companies[$flight.company]['name']} ({$flight.company})</th>
                                <th class="flight-number">
                                    Flight {$flight.number} ({if isset($cabins[$flight.cabin])}{$cabins[$flight.cabin]}{else}{$flight.cabin}{/if})
                                </th>
                                <th style="width: 129px" class="flight-duration">
                                    {$flight.duration_simple}
                                </th>
                            </tr>
                            <tr>
                                <td class="hidden-xs hidden-sm">&nbsp;</td>
                                <td>{$flight.depTime}</td>
                                <td>{$flight.depDate}</td>
                                <td><i class="t-icon t-icon-plane_start t-icon_tables"></i>{$flight.loc.0.id} {$locations[$flight.loc.0.id]['name']}</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="hidden-xs hidden-sm">&nbsp;</td>
                                <td>{$flight.arrTime}</td>
                                <td>{$flight.arrDate}</td>
                                <td><i class="t-icon t-icon-plane_end t-icon_tables"></i>{$flight.loc.1.id} {$locations[$flight.loc.1.id]['name']}</td>
                                <td>&nbsp;</td>
                            </tr>
                            {/foreach}
                        {/foreach}
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </div>
{/if}
