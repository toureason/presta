{if (isset($flights) && isset($flights.seg))}
    {assign var='cabins' value=['M'=>{l s="Economy Standard" },'W'=>{l s="Economy Premium" },'Y'=>{l s="Economy" },'C'=>{l s="Business" },'F'=>{l s="First" }]}
    {assign var='fbaUnits' value=['K'=>{l s="kilograms"},'L'=>{l s="pounds"}]}
    {assign var='counterColor' value=0}
    {foreach $flights.seg.1 as $fgindex => $fgroup}
        {assign var='stops' value=$fgroup|@count - 1}
        {if $fgindex == 0}
           <thead class="searchHead searchHead-flights">
           <tr>
               {*<th>*}
               {*<button id="flight_details_btn_{$fgindex}" type="button" onclick="showFlightDetails({$fgindex});" class="button btn btn-default button-small">{l s='Show details'}</button>*}
               {*</th>*}
               {*<th>{l s='Direction'}</th>*}
               <th>{l s='Date'}</th>
               <th id="sort-time" class="down"><a href="javascript:searchFlightsSort('time');">{l s='Duration'}</a></th>
               <th id="sort-move" class="down"><a href="javascript:searchFlightsSort('move');">{l s='Stops'}</a></th>
               <th style="width: 150px" class="down">{l s='Free baggages'}</th>
               <th id="sort-price"  class="down"><a href="javascript:searchFlightsSort('price');">{l s='Price'}</a></th>
               {*<th>{l s='Company'}</th>*}
               {*<th>{l s='Class'}</th>*}
               <th>&nbsp;</th>
           </tr>
           </thead>
        {/if}

        {if isset($flights.seg.2)}{assign var='fgs' value=[$fgroup, $flights.seg.2[$fgindex]]}
        {else}{assign var='fgs' value=[$fgroup]}
        {/if}

        {if $counterColor == 1}
            {assign var='counterColor' value=0}
            {else}
            {assign var='counterColor' value=1}
        {/if}
        <tr class="bg-{$counterColor} table-first company-name">
            <td colspan="7">
            <div class="company-name-in">
                {assign var='fcdiv' value=''}
                {assign var='companyUsed' value='[]'}
                {foreach $fgs as $fgseg=>$fgroup2}
                    {foreach $fgroup2 as $findex=>$flight}
                        {if ! isset($companyUsed[$flight.company])}
                            {$fcdiv}{$flights.companies[$flight.company]}
                            {assign var='fcdiv' value=' / '}
                            {append var='companyUsed' value='1' index=$flight.company}
                        {/if}
                    {/foreach}
                {/foreach}
            </div>
                <div class="td-5-mobile"><span class="order-list-price">{$fgroup.0.price} &#8364;</span></div>
                <div class="last td-6-mobile">
                    <button type="button" class="btn btn-default" onClick="setFlight({$fgroup.0.flightId});" class="button btn btn-default button-medium">{l s='Select'}</button>
                </div>
            </td>
        </tr>
        <tr class="bg-{$counterColor} tr-firstInfo">

            <td class="first td-1" style="width: 280px">
                <span class="mainBox">
                    <span class="time-from">{$fgroup.0.depTime}</span>
                    <span class="place-from">{$fgroup.0.loc.0.id}</span>
                </span>
                <span class="arrow">&#8594;</span>
                <span class="mainBox">
                    <span class="time-to">{$fgroup[$fgroup|@count - 1].arrTime}</span>
                    <span class="place-to">{$fgroup[$fgroup|@count - 1].loc.1.id}</span>
                </span>
            </td>
            <td class="gray td-2">{$fgroup.0.duration}</td>
            <td class="stop gray td-3">{if $stops == 0}{l s='nonstop'}{else}{$stops}{/if}</td>


            <td class="gray td-4">
                {if $fgroup.0.fba.quantityCode == 'N'}
                    {$fgroup.0.fba.freeAllowance} {if $fgroup.0.fba.freeAllowance == 1}{l s='piece'}{else}{l s='pieces'}{/if}
                {else}
                    {$fgroup.0.fba.freeAllowance} {$fbaUnits[$fgroup.0.fba.unitQualifier]}
                {/if}
            </td>
            {*<td class="gray">{$flights.companies[$fgroup.0.company]} ({$fgroup.0.company})</td>*}
            <td rowspan="2" class="td-5"><span class="order-list-price">{$fgroup.0.price} &#8364;</span></td>
            {*<td>{if isset($cabins[$fgroup.0.cabin])}{$cabins[$fgroup.0.cabin]}*}
            {*{else}{$fgroup.0.cabin}*}
            {*{/if}*}
            {*</td>*}
            <td class="last td-6" rowspan="2">
                <button type="button" class="btn btn-default" onClick="setFlight({$fgroup.0.flightId});" class="button btn btn-default button-medium">{l s='Select'}</button>
            </td>
        </tr>
        {if isset($flights.seg.2)}
            {assign var='stops2' value=$flights.seg.2[$fgindex]|@count - 1}
            <tr class="bg-{$counterColor} tr-secondInfo">
                <td class="first td-1">
                     <span class="mainBox">
                    <span class="time-from">{$flights.seg.2[$fgindex].0.depTime}</span>
                    <span class="place-from">{$flights.seg.2[$fgindex].0.loc.0.id}</span>
                         </span>
                    <span class="arrow">&#8594;</span>
                     <span class="mainBox">
                    <span class="time-to">{$flights.seg.2[$fgindex][$flights.seg.2[$fgindex]|@count - 1].arrTime}</span>
                    <span class="place-to">{$flights.seg.2[$fgindex][$flights.seg.2[$fgindex]|@count - 1].loc.1.id}</span>
                         </span>
                </td>
                <td class="gray td-2">{$flights.seg.2[$fgindex].0.duration}</td>
                <td class="stop gray td-3">{if $stops2 == 0}{l s='nonstop'}{else}{$stops2}{/if}</td>
                <td class="gray td-4">
                    {if $flights.seg.2[$fgindex].0.fba.quantityCode == 'N'}
                        {$flights.seg.2[$fgindex].0.fba.freeAllowance} {if $flights.seg.2[$fgindex].0.fba.freeAllowance == 1}{l s='piece'}{else}{l s='pieces'}{/if}
                    {else}
                        {$flights.seg.2[$fgindex].0.fba.freeAllowance} {$fbaUnits[$flights.seg.2[$fgindex].0.fba.unitQualifier]}
                    {/if}
                </td>
                {*<td class="last gray">{$flights.companies[$flights.seg.2[$fgindex].0.company]} ({$flights.seg.2[$fgindex].0.company})</td>*}

                {*<td>{if isset($cabins[$flights.seg.2[$fgindex].0.cabin])}{$cabins[$flights.seg.2[$fgindex].0.cabin]}*}
                {*{else}{$flights.seg.2[$fgindex].0.cabin}*}
                {*{/if}*}
                {*</td>*}
            </tr>
        {/if}
        <tr class="border-bottom1 bg-{$counterColor} buttons-more table-last" id="row-bg-{$fgindex}">
            <td class="first last " colspan="7">
                <button id="flight_details_btn_{$fgindex}" type="button" onclick="showFlightDetails({$fgindex});">{l s='Show details'}</button>
                <button id="flight_details_btn_2_{$fgindex}" type="button" onclick="hideFlightDetails({$fgindex});" style="display: none">{l s='Hide details'}</button>
            </td>
        </tr>
        <tr id="flight_details_{$fgindex}" class="flight_details bg-{$counterColor}">
            <td class="first last" colspan="7" >
                <table>

                    {foreach $fgs as $fgseg=>$fgroup2}
                        {foreach $fgroup2 as $findex=>$flight}
                        <tr class="th">
                            <th class="first" style="width: 55px; border-bottom: none"></th>
                            <th style="width: 104px" class="flight-direction">
                                {if ($fgseg == 0)}{l s='Depart'}
                                {else}{l s='Return'}
                                {/if}
                            </th>
                            <th style="max-width: 300px" class="flight-company">{$flights.companies[$flight.company]} ({$flight.company})</th>
                            {*<th>{l s='Class'}</th>*}
                            {*<th>{l s='Departure'}</th>*}
                            {*<th>{l s='Arrival'}</th>*}
                            <th class="flight-number">
                                Flight {$flight.number} ({if isset($cabins[$flight.cabin])}{$cabins[$flight.cabin]}{else}{$flight.cabin}{/if})
                            </th>
                            <th style="width: 129px" class="flight-duration">
                                {$flight.duration_simple}
                            </th>
                        </tr>

                            <tr class="td td-1">
                                <td class="first">&nbsp;</td>
                                <td class="flight-time">{$flight.depTime}</td>
                                <td>{$flight.depDate} </td>
                                <td class="flight_details_sel-city"><i class="t-icon t-icon-plane_start t-icon_tables"></i>{$flight.loc.0.id} {$flights.locations[$flight.loc.0.id]}</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr class="td td-2">
                                <td class="first">&nbsp;</td>
                                <td class="flight-time">{$flight.arrTime}</td>
                                <td>{$flight.arrDate} </td>
                                <td class="flight_details_sel-city"><i class="t-icon t-icon-plane_end t-icon_tables"></i>{$flight.loc.1.id} {$flights.locations[$flight.loc.1.id]}</td>
                                <td>&nbsp;</td>
                            </tr>
                            {*<tr>*}
                                {*<td>#{$flight.number}</td>*}
                                {*<td>{$flight.duration_simple}</td>*}
                                {*<td>{$flights.companies[$flight.company]} ({$flight.company})</td>*}
                                {*<td>*}
                                    {*{if isset($cabins[$flight.cabin])}{$cabins[$flight.cabin]}*}
                                    {*{else}{$flight.cabin}*}
                                    {*{/if}*}
                                {*</td>*}
                                {*<td>{$flight.depDate} {$flight.depTime} {$flights.locations[$flight.loc.0.id]} ({$flight.loc.0.id})</td>*}
                                {*<td>{$flight.arrDate} {$flight.arrTime} {$flights.locations[$flight.loc.1.id]} ({$flight.loc.1.id})</td>*}
                            {*</tr>*}
                        {/foreach}
                    {/foreach}
                </table>
            </td>
        </tr>
    {/foreach}
{/if}
