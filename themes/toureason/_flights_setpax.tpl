{capture name=path}{l s='Your flight tickets'}{/capture}

<h1 id="cart_title" class="page-heading">{l s='Your flight tickets'}
</h1>

{assign var='current_step' value='flights'}
{assign var='cabins' value=['M'=>{l s="Economy Standard" },'W'=>{l s="Economy Premium" },'Y'=>{l s="Economy" },'C'=>{l s="Business" },'F'=>{l s="First" }]}
{include file="$tpl_dir./order-steps.tpl"}
{include file="$tpl_dir./errors.tpl"}

<form id="flight_setpax_form" class="flights-form" action="{$link->getPageLink('order', true, NULL, "&step=101&action=flightsetpax")|escape:'html':'UTF-8'}" method="post">
    <div class="row">
        <div class="col-lg-3 col-lg-push-9 cart-box" id="cartBox">
            <div class="main">
                {include file='./_order_basket_mini.tpl'}
                <div class="cart-box-footer">
                    <div class="row">

                        <div class="col-md-12">
                            <button type="submit" name="processFlights" class="btn btn-arrow btn-info btn-block processFlights" >
                                <span>{l s='Proceed to checkout'}</span>
                            </button>
                        </div>
                        <div class="col-md-12">
                            <a href="{$link->getPageLink('order', true, NULL, "&step=101")|escape:'html':'UTF-8'}" class="btn btn-default btn-block">
                                {l s='Backward'}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-lg-pull-3 mainContent">
            {if $form_params_search}
                {assign var='paxCnt' value=$form_params_search.adults + $form_params_search.children + $form_params_search.infants}
                <div class="order-filter order-filter-state-1" {if (isset($fgroup)) }id="subfilter"{/if}>

                    <div class="order-filter-state-content order-filter-state-1-content">
                        <div class="box-in box-in-1">
                            <table>
                                <tr>
                                    <th>{$form_params_search.from}</th>
                                    <th class="arrow">
                                        {if $form_params_search.return_date}
                                            &#8596;
                                        {else}
                                            &#8594;
                                        {/if}
                                    </th>
                                    <th>{$form_params_search.to}</th>
                                </tr>
                                {*<tr>*}
                                    {*<!--td colspan="3"> 4 flights</td-->*}
                                {*</tr>*}
                            </table>
                        </div>
                        <div class="box-in box-in-2">
                            <table>
                                <tr>
                                    <th>{$form_params_search.depdate|date_format:'%b %e'}</th>
                                    {if $form_params_search.return_date}
                                        <th class="arrow">&#8594;</th>
                                        <th>{$form_params_search.retdate|date_format:'%b %e'}</th>
                                    {/if}
                                </tr>
                                <tr>
                                    <td>{$form_params_search.depdate|date_format:'%a'}</td>
                                    {if $form_params_search.return_date}
                                        <td>&nbsp;</td>
                                        <td>{$form_params_search.retdate|date_format:'%a'}</td>
                                    {/if}
                                </tr>
                            </table>
                        </div>
                        <div class="box-in box-in-3 {if $form_params_search.class}{else}empty{/if}">
                        {if $form_params_search.class}
                                <table>
                                    <tr>
                                        <th>{$cabins[$form_params_search.class]}</th>
                                    </tr>
                                    <tr>
                                        <td>{l s='cabin'}</td>
                                    </tr>
                                </table>
                        {/if}
                        </div>
                        <div class="box-in box-in-4">
                            <table>
                                <tr>
                                    <th>{$paxCnt}</th>
                                </tr>
                                <tr>
                                    <td>
                                        {if $paxCnt == 1}
                                            {l s='passenger'}
                                        {else}
                                            {l s='passengers'}
                                        {/if}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="box-in box-in-5">
                            <a class="btn btn-rounded btn-default-iverse" href="{$link->getPageLink('order', true, NULL, "&step=101")|escape:'html':'UTF-8'}">{l s='New search'}</a>
                        </div>
                    </div>

                    {*<a class="button-exclusive btn btn-default" href="{$link->getPageLink('order', true, NULL, "&step=101")|escape:'html':'UTF-8'}">{l s='New search'}</a>*}

                </div>
            {/if}


            {include file='./_flights_inc_selflight.tpl'}
            {if (isset($passengers)) }
                <div class="flight-detail flight-detail-no-border">
                    <strong>{l s="Passengers"}</strong>
                    <div class="flight-detail-item ">
                        {*<strong>{l s="Adults"}</strong>*}
                        <div class="row row-sm">
                            {include file='./_flights_inc_passenger.tpl' paxtype='ADT'}
                        </div>
                    </div>
                    {if $passengers['CH']['cnt'] > 0}
                        <div class="flight-detail-item ">
                            <strong>{l s="Children"}</strong>
                            <div class="row row-sm">
                                {include file='./_flights_inc_passenger.tpl' paxtype='CH'}
                            </div>
                        </div>
                    {/if}
                    {if $passengers['INF']['cnt'] > 0}
                        <div class="flight-detail-item ">
                            <strong>{l s="Infants"}</strong>
                            <div class="row row-sm">
                                {include file='./_flights_inc_passenger.tpl' paxtype='INF'}
                            </div>
                        </div>
                    {/if}
                </div>
            {/if}
        </div>
    </div>
</form>

{* eu-legal *}
{hook h="displayBeforeShoppingCartBlock"}
<!-- end order-detail-content -->


<div class="clear"></div>
<div class="cart_navigation_extra">
    <div id="HOOK_SHOPPING_CART_EXTRA">{if isset($HOOK_SHOPPING_CART_EXTRA)}{$HOOK_SHOPPING_CART_EXTRA}{/if}</div>
</div>
{strip}
    {addJsDef deliveryAddress=$cart->id_address_delivery|intval}
    {addJsDefL name=txtProduct}{l s='product' js=1}{/addJsDefL}
    {addJsDefL name=txtProducts}{l s='products' js=1}{/addJsDefL}
{/strip}
