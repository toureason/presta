{capture name=path}{l s='Your hotels'}{/capture}

<h1 id="cart_title" class="page-heading">{l s='Your hotels'}
</h1>

<div id="loading_content">
    {if isset($loading)}
        {$loading}
    {/if}
</div>

{assign var='current_step' value='hotels'}
{include file="$tpl_dir./order-steps.tpl"}
<div class="row">
    <div class="col-lg-3 col-lg-push-9 cart-box" id="cartBox">
        <div class="main">
            {include file='./_order_basket_mini.tpl'}
            <div class="cart-box-footer">
                <div class="row">
                    <form action="{$link->getPageLink('order.php', true)|escape:'html':'UTF-8'}" method="post">
                        <input type="hidden" class="hidden" name="step" value="102"/>
                        <input type="hidden" name="back" value="{$back}"/>
                        {if isset($selection) && count($selection)>0}
                            <input type="hidden" class="hidden" name="action" value="hotelsetpax"/>
                        {else}
                            <input type="hidden" class="hidden" name="action" value="hotelnext"/>
                        {/if}

                        <div class="col-md-12">
                            <button type="submit" name="processHotels" class="btn btn-arrow btn-info btn-block">
                                <span>{l s='Proceed to checkout'}</span>
                            </button>
                        </div>

                        <div class="col-md-12">
                            <a href="{$link->getPageLink('order', true, NULL, "&step=101")|escape:'html':'UTF-8'}" class="btn btn-default btn-block">
                                {l s='Backward'}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-9 col-lg-pull-3 mainContent">
        {include file="$tpl_dir./errors.tpl"}

        <div class="order-filter order-filter-state-3">
            <button class="btn btn-block btn-open-filter btn-rounded">{l s="Hotel filter"}</button>
            <form id="hotel_search_form" class="hotels-form"
                  action="{$link->getPageLink('order', true, NULL, "&step=102")|escape:'html':'UTF-8'}" method="post">
                <input type="hidden" name="city" value="{if (isset($form_params.city))}{$form_params.city}{/if}"/>
                <input type="hidden" name="action" value="hotelsearch"/>

                <div class="row row-sm">
                    <div class="col-xs-12 col-sm-6 col-md-8">
                    <div class="form-group">
                        <label for="city">
                            {l s="City"}:
                        </label>
                        <input type="text" name="city_auto" id="city_auto"
                               value="{if (isset($form_params.city_auto))}{$form_params.city_auto}{/if}"
                               class="form-control" placeholder="{l s="City"}:"/>
                    </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-2">
                    <div class="form-group">
                        <label for="count_rooms">
                            {l s="Count rooms"}:
                        </label>
                        {*<input type="number" name="count_rooms" id="count_rooms" min="1" max="15" placeholder="{l s="Count rooms"}"*}
                               {*value="{if (isset($form_params.count_rooms))}{$form_params.count_rooms}{else}1{/if}"*}
                               {*class="form-control"/>*}
                        <select name="count_rooms" id="count_rooms" class="form-control">
                            {for $cnt=1 to 15}
                                <option value="{$cnt}" {if (isset($form_params.count_rooms) && ($form_params.count_rooms == $cnt))}selected{/if}>{$cnt} Room{if $cnt > 1}s{/if}</option>
                            {/for}
                        </select>
                    </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-2">
                    <div class="">
                        <div class="form-group">
                            <label for="adults">
                                {l s="Adults"}:
                            </label>
                            <select name="adults" id="adults" class="form-control">
                            {for $cnt=1 to 15}
                                <option value="{$cnt}" {if (isset($form_params.adults) && ($form_params.adults == $cnt))}selected{/if}>{$cnt} Adult{if $cnt > 1}s{/if}</option>
                            {/for}
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </div>
                    </div>
                <div class="row row-sm">
                    <div class="col-xs-12 col-sm-6 col-md-3 box-7">
                        <div class="form-group">
                            <label for="start_date">
                                {l s="Start date"}:
                            </label>
                            <input type="text" name="start_date" id="start_date"
                                   value="{if (isset($form_params.start_date))}{$form_params.start_date}{/if}" placeholder="{l s="Start date"}"
                                   class="form-control"/>
                            <span class="icon"></span>
                        </div>
                        </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 box-8">
                        <div class="form-group">
                            <label for="end_date">
                                {l s="End date"}:
                            </label>
                            <input type="text" name="end_date" id="end_date" placeholder="{l s="End date"}"
                                   value="{if (isset($form_params.end_date))}{$form_params.end_date}{/if}" class="form-control"/>
                            <span class="icon"></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-md-push-3">
                        <button id="hotel_search_btn" type="button" onClick="searchHotelsNew();" class="pull-right button btn btn-info button-medium btn-formSearch" data-text-search="{l s='Search'}"
                        data-text-loading="{l s='Loading...'}"></button>
                    </div>
                    </div>

            </form>
        </div>

        {include file='./_hotels_inc_selection.tpl'}

        <div class="hotel-list">
            <form id="hotel_set"
                  action="{$link->getPageLink('order', true, NULL, "&step=102&action=hotelset")|escape:'html':'UTF-8'}"
                  method="post">
                <input type="hidden" name="hotel_id"/>
                <div>
                    <table class="hotel_results" id="hotel_results_table" data-next-index="0"
                        data-deselect-msg="{l s='You can choose rooms from only one hotel. Other rooms were deselected.'}"
                        data-count-msg="{l s='You cannot select more rooms than you requested.'}"
                        >
                    </table>
                </div>
                <div class="text-center buttons-more-box">
                    <button id="hotel_search_btn_next" type="button" data-sorting="price" onClick="searchHotels();" class="btn btn-showmore" data-text-search="{l s='Show more'}" data-text-loading="{l s='Loading...'}"></button>

                <button type="submit" id="hotel_select_rooms_btn" class="btn btn-arrow btn-info-2">{l s='Select Rooms'}</button>
                </div>
            </form>
        </div>

    </div>
</div>
