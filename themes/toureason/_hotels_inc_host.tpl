{for $iter=1 to $passengers['cnt']}
    <div class="col-xs-12 col-sm-2 col-md-1">
        <div class="form-group">
            <label for="sal_{$iter}">
                {l s="Salutation"}:
            </label>
            <select name="sal[{$iter}]" id="sal_{$iter}" class="form-control">
                <option value="MR"
                        {if (isset($form_params_names.sal[$iter]) && ($form_params_names.sal[$iter] == 'MR'))}selected{/if}>{l s="Mr."}</option>
                <option value="MRS"
                        {if (isset($form_params_names.sal[$iter]) && ($form_params_names.sal[$iter] == 'MRS'))}selected{/if}>{l s="Mrs."}</option>
                <option value="MISS"
                        {if (isset($form_params_names.sal[$iter]) && ($form_params_names.sal[$iter] == 'MISS'))}selected{/if}>{l s="Ms."}</option>
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-4">
        <div class="form-group">
            <label for="first_name_{$iter}">
                {l s="First name"}:
            </label>
            <input class="form-control" placeholder="{l s="First name"}" type="text" name="first_name[{$iter}]" id="first_name_{$iter}"
                   value="{if (isset($form_params_names.first_name[$iter]))}{$form_params_names.first_name[$iter]}{/if}"/>
        </div>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-4">
        <div class="form-group">
            <label for="last_name_{$iter}">
                {l s="Last name"}:
            </label>
            <input class="form-control" type="text" placeholder="{l s="Last name"}" name="last_name[{$iter}]" id="last_name_{$iter}"
                   value="{if (isset($form_params_names.last_name[$iter]))}{$form_params_names.last_name[$iter]}{/if}"/>
        </div>
    </div>
    <div class="clearfix"></div>
{/for}
