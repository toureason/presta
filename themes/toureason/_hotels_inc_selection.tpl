{if isset($selection) && count($selection)>0}
    {assign var='counterColor' value=0}
    <table class="hotel_results">
        <tbody>
        {foreach $selection as $val}
        {if $counterColor == 1}
            {assign var='counterColor' value=0}
        {else}
            {assign var='counterColor' value=1}
        {/if}
            <tr class="hotel-info hotel-info-mobile">
                    <td colspan="3">
                        <h2>{$val['HotelName']}</h2>
                        <div class="td-3-mobile">
                        <span class="order-list-price order-list-price-2">
                    {if isset($form_params)}
                    <span class="head">{$form_params.nights} {if $form_params.nights == 1}{l s='Night'}{else}{l s='Nights'}{/if}</span>
                    <span class="price">
                        <span class="in">{$val['room']['TotalPrice'] * $val['room']['cnt'] * $form_params.nights} &euro;</span>
                    </span>
                    {/if}
                </span>
                        </div>
                    </td>
            </tr>
        <tr class="hotel-info hotel-info-0 bg-{$counterColor}">
            <td class="first">
                <img src="http://fpoimg.com/100x100" alt="" width="100" height="100">
            </td>
            <td class="hotel-description">
                <h2>{$val['HotelName']}</h2>

                <div>
                    <span><strong>{$val['Address']}</strong></span> -
                    <span>{$val['room']['NumberOfUnits']} {if $val['room']['NumberOfUnits'] == 1}{l s='Bed'}{else}{l s='Beds'}{/if}</span> -
                    <span>{$val['room']['cnt']} {if $val['room']['cnt'] == 1}{l s='Room'}{else}{l s='Rooms'}{/if}</span>
                </div>
                <div id="hotel_desc_0">
                    <p class="p-hotel-description" id="p-hotel-description_0">{$val['room']['RoomInfo']}</p>
                </div>
            </td>
            <td class="last td-3">
                {if isset($form_params)}
                <span class="order-list-price order-list-price-2">
                    <span class="head">{$form_params.nights} {if $form_params.nights == 1}{l s='Night'}{else}{l s='Nights'}{/if}</span>
                    <span class="price">
                        <span class="in">{$val['room']['TotalPrice'] * $val['room']['cnt'] * $form_params.nights} &euro;</span>
                    </span>
                </span>
                {else}
                &nbsp;
                {/if}
            </td>
        </tr>
        {/foreach}
        </tbody>
    </table>
{/if}
