{if isset($hotels) && count($hotels)>0}
    {assign var='counterColor' value=0}
    {foreach $hotels as $hindex => $val}
        {if $counterColor == 1}
            {assign var='counterColor' value=0}
        {else}
            {assign var='counterColor' value=1}
        {/if}
        {if $hindex == 0}
        <thead class="searchHead searchHead-hotels">
        <tr>
            <th class="up" id="sort-category" colspan="2"><a href="#">{l s='Category'}</a></th>
            <th class="up" id="sort-price"><a href="javascript:searchHotelsSort('price');">{l s='Price'}</a></th>
        </tr>
        </thead>
        {/if}
        <tr  class="hotel-info hotel-info-mobile hotel-info-{$hindex} bg-{$counterColor}">
            <td colspan="3">
                <h2>{$val['HotelName']}
                    <span class="stars star-rating-{$val.detail.stars}">
                        <span class="star star-1"></span>
                        <span class="star star-2"></span>
                        <span class="star star-3"></span>
                        <span class="star star-4"></span>
                        <span class="star star-5"></span>
                    </span>
                </h2>
            <div class="td-3-mobile"><span class="order-list-price"> <span class="head">{l s='1 night from'}</span> <span class="price"><span class="in">{$val.PriceFrom} &#8364;</span> <span class="pp">{l s='pp'}</span></span></span></div>
            </td>
        </tr>
        <tr  class="hotel-info hotel-info-{$hindex} bg-{$counterColor}">
            <td class="first">
                {if $val.detail.thumb}
                    <a href="{$val.detail.img}" target="_blank"><img src="{$val.detail.thumb}" alt="" width="100" height="100" /></a>
                {else}
                <img src="http://fpoimg.com/100x100" alt="" width="100" height="100">
                {/if}
            </td>
            <td class="hotel-description">
                <h2>{$val['HotelName']}
                    <span class="stars star-rating-{$val.detail.stars}">
                        <span class="star star-1"></span>
                        <span class="star star-2"></span>
                        <span class="star star-3"></span>
                        <span class="star star-4"></span>
                        <span class="star star-5"></span>
                    </span>
                </h2>
                <p class="p-hotel-description" id="p-hotel-description_{$hindex}">
                    <span id="hotel_desc_{$hindex}">
                        {$val.detail.desc}
                    </span>
                </p>

                <button id="hotel_details_btn_{$hindex}" type="button"
                        onclick="showHotelDetails({$hindex}, '{$val['HotelCode']}');"
                        class="button-more">{l s='Show details'}</button>
                <button type="button" onclick="hideHotelDetails({$hindex});" style="display: none"
                        class="button-more" id="hotel_details_btn_2_{$hindex}">{l s='Hide details'}</button>
            </td>
            <td class="last td-3"><span class="order-list-price"> <span class="head">{l s='1 night from'}</span> <span class="price"><span class="in">{$val.PriceFrom} &#8364;</span> <span class="pp">{l s='pp'}</span></span></span></td>
        </tr>

        <tr id="hotel_details_{$hindex}" class="hotel_details bg-{$counterColor}">
            <td  class="first">
                <span id="hotel_img_{$hindex}">&nbsp;</span>
            </td>
            <td colspan="2" class="last">
                <table>
                    <span class="hidden">{$i=0}</span>
                    {foreach $val['Rooms'] as $rindex => $room}
                        {if $i>=5}{break}{/if}
                        {if $room['TotalPrice']>0}
                            <tr>
                                <td class="first roomInfo">{$room['RoomInfo']}</td>
                                <td class="orderPrice"><span class="order-list-price"> {$room['TotalPrice']} &euro;</span></td>
                                {*<td>{$room['Count']}</td>*}
                                <td style="width: 90px" class="last">
                                    <select onchange="checkRooms({$hindex}, this);" class="form-control" name="rooms[{$hindex}][{$rindex}]" data-hotel="{$hindex}">
                                        {for $cnt=0 to min($count_rooms, $room['Count'])}
                                        <option value="{$cnt}">{$cnt} {if $cnt != 1}{l s='Rooms'}{else}{l s='Room'}{/if}</option>
                                        {/for}
                                    </select>
                                </td>
                            </tr>
                            <span class="hidden">{$i++}</span>
                        {/if}
                    {/foreach}
                </table>
            </td>
        </tr>
    {/foreach}
{/if}
