{capture name=path}{l s='Your hotels'}{/capture}

<h1 id="cart_title" class="page-heading">{l s='Your hotels'}
</h1>

{assign var='current_step' value='hotels'}
{include file="$tpl_dir./order-steps.tpl"}

<form action="{$link->getPageLink('order.php', true)|escape:'html':'UTF-8'}" method="post">
<input type="hidden" class="hidden" name="step" value="102"/>
<input type="hidden" class="hidden" name="action" value="hotelsetpax"/>
<input type="hidden" name="back" value="{$back}"/>

<div class="row">
    <div class="col-lg-3 col-lg-push-9 cart-box" id="cartBox">
        <div class="main">
            {include file='./_order_basket_mini.tpl'}
            <div class="cart-box-footer">
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" name="processHotels" class="btn btn-arrow btn-info btn-block">
                            <span>{l s='Proceed to checkout'}</span>
                        </button>
                    </div>
                    <div class="col-md-12">
                        <a href="{$link->getPageLink('order', true, NULL, "&step=102")|escape:'html':'UTF-8'}" class="btn btn-default btn-block">
                            {l s='Backward'}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-9 col-lg-pull-3 mainContent">
        {include file="$tpl_dir./errors.tpl"}

        {if isset($selection) && count($selection)>0}
        <div class="order-filter order-filter-state-1 order-filter-state-hotels">
            <div class="order-filter-state-content order-filter-state-1-content">
                <div class="box-in box-in-1">
                    <table>
                        <tbody>
                        <tr>
                            <th>{$form_params.city_auto}</th>
                        </tr>
                        <tr>
                            <!--td colspan="3"> 4 flights</td-->
                        </tr>
                        </tbody></table>
                </div>
                <div class="box-in box-in-2">
                    <table>
                        <tbody><tr>
                            <th>{$form_params.startISO|date_format:'%b %e'}</th>
                            <th class="arrow">&#8594;</th>
                            <th>{$form_params.endISO|date_format:'%b %e'}</th>
                        </tr>
                        <tr>
                            <td>{$form_params.startISO|date_format:'%a'}</td>
                            <td>&nbsp;</td>
                            <td>{$form_params.endISO|date_format:'%a'}</td>
                        </tr>
                        </tbody></table>
                </div>
                <div class="box-in box-in-3">
                    <table>
                        <tbody><tr>
                            <th>{$form_params.nights} {if $form_params.nights == 1}{l s='Night'}{else}{l s='Nights'}{/if}</th>
                        </tr>
                        <tr>
                            <td>{l s='stay'}</td>
                        </tr>
                        </tbody></table>
                </div>
                <div class="box-in box-in-4">
                    <table>
                        <tbody><tr>
                            <th>{$form_params.count_rooms}</th>
                        </tr>
                        <tr>
                            <td>{if $form_params.count_rooms == 1}{l s='room'}{else}{l s='rooms'}{/if}</td>
                        </tr>
                        </tbody></table>
                </div>
                <div class="box-in box-in-5">
                    <a class="btn btn-rounded btn-default-iverse" href="{$link->getPageLink('order', true, NULL, "&step=102")|escape:'html':'UTF-8'}">{l s='Change search'}</a>
                </div>
            </div>
        </div>
        {/if}

        {include file='./_hotels_inc_selection.tpl'}

        <div class="flight-detail flight-detail-no-border">
            <strong>{l s="Hosts"}</strong>
            <div class="flight-detail-item ">
                <div class="row row-sm">
                    {include file='./_hotels_inc_host.tpl'}
                </div>
            </div>
            <strong>{l s="Contact e-mail address"}</strong>
            <div class="flight-detail-item ">
                <div class="row row-sm">
                    <div class="col-xs-12 col-sm-6 col-md-5">
                        <div class="form-group">
                            <input class="form-control" type="email" placeholder="{l s="E-mail"}" name="email" id="email"
                            value="{if (isset($form_params_names.email))}{$form_params_names.email}{/if}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

</form>
