                <div class="cart-box-header">
                    <div class="cart-box-icon"></div>
                    <div class="cart-box-price">
                        {l s='Total'}: <strong><em>{displayPrice price=$orderTotal}</em></strong>
                    </div>
                    <div class="box-arrow-bottom"></div>
                </div>
                <div class="cart-box-content">
                    <table class="cart-box-content-list">
                    <tr class="cart-box-content-head text-uppercase">
                       <td colspan="4"> <strong>{l s='Event tickets'}</strong></td>
                    </tr>

                        {foreach $product_list as $product}
                        <tr>
                            <td class="name">{$product.name|escape:'htmlall':'UTF-8'}</td>
                            <td class="count">
                            {$product.cart_quantity}&nbsp;{if $product.cart_quantity == 1}{l s='ticket'}{else}{l s='tickets'}{/if}
                            </td>
                            <td class="price">{displayPrice price=$product.total_wt}</td>
                            <td class="button">
                                <a href="{$link->getPageLink('cart', true, NULL, "delete=1&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;id_address_delivery={$product.id_address_delivery|intval}&amp;token={$token_cart}")|escape:'html':'UTF-8'}" class="btn-remove"><span>&times;</span></a>
                            </td>
                        </tr>
                        {/foreach}
                    {if $flight_price > 0}
                    {assign var='paxcnt' value=$flight_pax.ADT.cnt + $flight_pax.CH.cnt + $flight_pax.INF.cnt}
                    <tr class="cart-box-content-head text-uppercase">
                        <td colspan="4">  <strong>{l s='Flight tickets'}</strong></td>
                    </tr>

                        <tr>
                            <td class="name">{$flight_nos}</td>
                            <td class="count">
                            {$paxcnt}&nbsp;{if $paxcnt == 1}{l s='passenger'}{else}{l s='passengers'}{/if}
                            </td>
                            <td class="price">{displayPrice price=$flight_price}</td>
                            <td class="button">
                                <a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=101&action=flightunset")|escape:'html':'UTF-8'}" class="btn-remove"><span>&times;</span></a>
                            </td>
                        </tr>
                    {/if}
                    {if $hotel_price > 0}
                        {assign var='paxcnt' value=$hotel_pax}
                        <tr class="cart-box-content-head text-uppercase">
                            <td colspan="4">  <strong>{l s='Hotel'}</strong></td>
                        </tr>

                        <tr>
                            <td class="name">{$hotel_rooms}&nbsp;{if $hotel_rooms == 1}{l s='room'}{else}{l s='rooms'}{/if}</td>
                            <td class="count">
                            {$paxcnt}&nbsp;{if $paxcnt == 1}{l s='adult'}{else}{l s='adults'}{/if}
                            </td>
                            <td class="price">{displayPrice price=$hotel_price}</td>
                            <td class="button">
                                <a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=102&action=hotelunset")|escape:'html':'UTF-8'}" class="btn-remove"><span>&times;</span></a>
                            </td>
                        </tr>

                    {/if}
                    </table>
                </div>
