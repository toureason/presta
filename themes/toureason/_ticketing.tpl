{capture name=path}{l s='Your flight tickets'}{/capture}

<h1 id="cart_title" class="page-heading">{l s='Your flight tickets'}
</h1>

{assign var='current_step' value='flights'}
{include file="$tpl_dir./order-steps.tpl"}
{include file="$tpl_dir./errors.tpl"}

<div>{l s='Flight is booked'}</div>
{if isset($tickets) && $tickets|@count > 0}
<div>{l s='Tickets issued'}</div>
{foreach $tickets as $ticket}
    {$ticket}<br />
{/foreach}
{else}
<form id="ticketing_form" class="flights-form" action="index.php?controller=order&step=101&action=ticketing" method="post">
    <button name="go" type="submit" class="button btn btn-default button-medium btn-formSearch">{l s='Issue ticket'}</button>
</form>
{/if}

{* eu-legal *}
{hook h="displayBeforeShoppingCartBlock"}
<!-- end order-detail-content -->


<div id="HOOK_SHOPPING_CART">{$HOOK_SHOPPING_CART}</div>

<form action="{$link->getPageLink('order.php', true)|escape:'html':'UTF-8'}" method="post">
    <input type="hidden" class="hidden" name="step" value="102"/>
    <input type="hidden" name="back" value="{$back}"/>

    <p class="cart_navigation clearfix">
        <a href="
    {if (isset($smarty.server.HTTP_REFERER)
        && ($smarty.server.HTTP_REFERER == $link->getPageLink('order', true)
        || $smarty.server.HTTP_REFERER == $link->getPageLink('order-opc', true)
        || strstr($smarty.server.HTTP_REFERER, 'step=')))
        || !isset($smarty.server.HTTP_REFERER)}
        {$link->getPageLink('index')}
    {else}
        {$smarty.server.HTTP_REFERER|escape:'html':'UTF-8'|secureReferrer}
    {/if}"
           class="button-exclusive btn btn-default" title="{l s='Continue shopping'}">
            <i class="icon-chevron-left"></i>{l s='Continue shopping'}
        </a>
        <button type="submit" name="processFlights" class="button btn btn-default button-medium">
            <span>{l s='Proceed to checkout'}<i class="icon-chevron-right right"></i></span>
        </button>
    </p>
</form>

<div class="clear"></div>
<div class="cart_navigation_extra">
    <div id="HOOK_SHOPPING_CART_EXTRA">{if isset($HOOK_SHOPPING_CART_EXTRA)}{$HOOK_SHOPPING_CART_EXTRA}{/if}</div>
</div>
{strip}
    {addJsDef deliveryAddress=$cart->id_address_delivery|intval}
    {addJsDefL name=txtProduct}{l s='product' js=1}{/addJsDefL}
    {addJsDefL name=txtProducts}{l s='products' js=1}{/addJsDefL}
{/strip}
