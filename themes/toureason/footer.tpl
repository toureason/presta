{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- .row -->
				</div><!-- #columns -->
			</div><!-- .columns-container -->
			{if $page_name == 'index'}
			<div class="featured-container">
				<div class="container">
					<a class="more button_exclusive">More events</a>
					<h3 class="featured-title">Best Events</h3>
					<div class="category-list row">
						<div class="block-category col-sm-4 col-xs-12">
							<div class="title">
								<h4><a href="#">FC Barcelona</a></h4>
							</div>
							<div class="image">
								<a href="#"><img src="{$base_dir_ssl}img/cms/category-image.jpg" /></a>
							</div>
							<a href="#" class="category-button">Design Your Package</a>
						</div>
						<div class="block-category col-sm-4 col-xs-12">
							<div class="title">
								<h4><a href="#">FC Barcelona</a></h4>
							</div>
							<div class="image">
								<a href="#"><img src="{$base_dir_ssl}img/cms/category-image.jpg" /></a>
							</div>
							<a href="#" class="category-button">Design Your Package</a>
						</div>
						<div class="block-category col-sm-4 col-xs-12">
							<div class="title">
								<h4><a href="#">FC Barcelona</a></h4>
							</div>
							<div class="image">
								<a href="#"><img src="{$base_dir_ssl}img/cms/category-image.jpg" /></a>
							</div>
							<a href="#" class="category-button">Design Your Package</a>
						</div>												
					</div>
					<h3 class="featured-title">Popular Events</h3>
					<div class="category-list row">
						<div class="block-category col-sm-4 col-xs-12">
							<div class="title">
								<h4><a href="#">FC Barcelona</a></h4>
							</div>
							<div class="image">
								<a href="#"><img src="{$base_dir_ssl}img/cms/category-image.jpg" /></a>
							</div>
							<a href="#" class="category-button">Design Your Package</a>
						</div>
						<div class="block-category col-sm-4 col-xs-12">
							<div class="title">
								<h4><a href="#">FC Barcelona</a></h4>
							</div>
							<div class="image">
								<a href="#"><img src="{$base_dir_ssl}img/cms/category-image.jpg" /></a>
							</div>
							<a href="#" class="category-button">Design Your Package</a>
						</div>
						<div class="block-category col-sm-4 col-xs-12">
							<div class="title">
								<h4><a href="#">FC Barcelona</a></h4>
							</div>
							<div class="image">
								<a href="#"><img src="{$base_dir_ssl}img/cms/category-image.jpg" /></a>
							</div>
							<a href="#" class="category-button">Design Your Package</a>
						</div>												
					</div>					
				</div>
			</div>


			<div class="home-about-container">
				<div class="container">
					<h2 class="about-title">How to use</h2>
					<h2 class="about-subtitle">Toureason.com</h2>
					<div id="info-block">
						<ul class="info-list clearfix">
							<li class="event active">
								<div class="image">
									<img src="{$img_dir}icon/event.png" alt="Choose your event" />
								</div>
								<span class="info-title">Choose your<strong>event</strong></span>
								<div class="description-mobile">
									<p>
									We offer huge variety of events – sports, concerts, theatre or festivals, all around the world and anytime during the year. Log in to our newsletter and receive regular information about your favourite events close to your city.</p>
								</div>
							</li>
							<li class="seats">
								<div class="image">
									<img src="{$img_dir}icon/seats.png" alt="Choose your event" />
								</div>
								<span class="info-title">Pick top<strong>seats</strong></span>								
								<div class="description-mobile">
									<p>
									Choose from hundreds of tickets for your desired event and find the best option which will match your budget and offer view worth of travelling! At Toureason.com we work only with trusted ticket sellers and providers. This is the reason why we can offer you full money back guarantee with compensation on top, in case your ticket would not be work.</p>
								</div>
							</li>
							<li class="flights">
								<div class="image">
									<img src="{$img_dir}icon/flights.png" alt="Choose your event" />
								</div>
								<span class="info-title">Find best<strong>flights</strong></span>
								<div class="description-mobile">
									<p>
									We work directly with airlines so we can guarantee same price that you can find on any other flight booking portals. The airtickets search is intuitive and offers best matching flights according to your preference (time en route, price or preferred airline). At Toureason.com do not expect any hidden fees or extra charges.</p>
								</div>										
							</li>
							<li class="hotel">
								<div class="image">
									<img src="{$img_dir}icon/hotel.png" alt="Choose your event" />
								</div>
								<span class="info-title">Get best hotel<strong>deals</strong></span>
								<div class="description-mobile">										
									<p>
									No matter whether you prefer a luxury hotel in the downtown or just a reasonable budget hotel to overnight after your event, you can find it at Toureason.com and always for the same price like booking directly with the hotel or via any other major booking portals.</p>
								</div>
							</li>
							<li class="guarantee">
								<div class="image">
									<img src="{$img_dir}icon/guarantee.png" alt="Choose your event" />
								</div>
								<span class="info-title">Check-out and get<strong>guarantee</strong></span>
								<div class="description-mobile">
									<p>
									Once you check-out our professional customer service team is ready for you to help with any needs regarding your reservation. We guarantee that your combination of service that you have designed and booked with us will work. You can read more about our Toureason.com guarantee here.</p>
								</div>										
							</li>
						</ul>
						<div class="description">
							<ul>
								<li class="event active">
									<p>
									We offer huge variety of events – sports, concerts, theatre or festivals, all around the world and anytime during the year. Log in to our newsletter and receive regular information about your favourite events close to your city.</p>
								</li>
								<li class="seats">
									<p>
									Choose from hundreds of tickets for your desired event and find the best option which will match your budget and offer view worth of travelling! At Toureason.com we work only with trusted ticket sellers and providers. This is the reason why we can offer you full money back guarantee with compensation on top, in case your ticket would not be work.</p>
								</li>
								<li class="flights">
									<p>
									We work directly with airlines so we can guarantee same price that you can find on any other flight booking portals. The airtickets search is intuitive and offers best matching flights according to your preference (time en route, price or preferred airline). At Toureason.com do not expect any hidden fees or extra charges.</p>
								</li>
								<li class="hotel">
									<p>
									No matter whether you prefer a luxury hotel in the downtown or just a reasonable budget hotel to overnight after your event, you can find it at Toureason.com and always for the same price like booking directly with the hotel or via any other major booking portals.</p>
								</li>
								<li class="guarantee">
									<p>
									Once you check-out our professional customer service team is ready for you to help with any needs regarding your reservation. We guarantee that your combination of service that you have designed and booked with us will work. You can read more about our Toureason.com guarantee here.</p>
								</li>
						</div>
					</div>
				</div>
			</div>

			<div class="home-news-container">
				<div class="container">
					<div class="news-content"> 
					<h2 class="news-title">Stay up to date</h2>
					<p>Follow us, and be the first to know special offers and never miss the favourite events.</p>
					<div id="mc_embed_signup">
						<form action="//toureason.us12.list-manage.com/subscribe/post?u=76a4e144cdb2abe3b7fe96cf1&amp;id=14eb097021" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    					<div id="mc_embed_signup_scroll">
							<div class="mc-field-group">
								<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Your e-mail">
							</div>
							<div id="mce-responses" class="clear">
								<div class="response" id="mce-error-response" style="display:none"></div>
								<div class="response" id="mce-success-response" style="display:none"></div>
							</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    						<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_76a4e144cdb2abe3b7fe96cf1_14eb097021" tabindex="-1" value=""></div>
    						<div class="clear"><input type="submit" value="Subscribe news" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    					</div>
						</form>
					</div>
					</div>
					{literal}
						<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
					{/literal}
					<div class="news-image"><h3>More fun with <span>toureason.com</span></h3>
						<img src="{$img_dir}more_fun.jpg" alt="More fun with Toureason.com" /></div>
				</div>
			</div>

			<div class="home-bottom-container">
				<div class="container">
					<h2 class="city-title">{l s='Where to go for events'}</h2>
					<div class="events_by_city clearfix">
						<div class="left">
							<div class="left_1">
								<a href="#">
									<img class="large" src="{$base_dir_ssl}img/cms/london.jpg" alt="London" />
									<img class="small" src="{$base_dir_ssl}img/cms/london_small.jpg" alt="London" />
								</a>
								<h3><a href="#">London <span>United Kingdom</span></a></h3>
							</div>
							<div class="left_2">
								<a href="#">
									<img class="large" src="{$base_dir_ssl}img/cms/amsterdam.jpg" alt="Amsterdam" />
									<img class="small" src="{$base_dir_ssl}img/cms/amsterdam_small.jpg" alt="Amsterdam" />
								</a>
								<h3><a href="#">Amsterdam <span>Netherland</span></a></h3>
							</div>
						</div>
						<div class="center">
							<div class="center_1">
								<a href="#">
									<img class="large" src="{$base_dir_ssl}img/cms/paris.jpg" alt="Paris" />
									<img class="small" src="{$base_dir_ssl}img/cms/paris_small.jpg" alt="Paris" />
								</a>
								<h3><a href="#">Paris <span>France</span></a></h3>
							</div>
							<div class="center_2">
								<a href="#">
									<img class="large" src="{$base_dir_ssl}img/cms/milano.jpg" alt="Milano" />
									<img class="small" src="{$base_dir_ssl}img/cms/milano_small.jpg" alt="Milano" />
								</a>
								<h3><a href="#">Milano <span>Italy</span></a></h3>
							</div>
							<div class="center_3">
								<a href="#">
									<img class="large" src="{$base_dir_ssl}img/cms/roma.jpg" alt="Roma" />
									<img class="small" src="{$base_dir_ssl}img/cms/roma_small.jpg" alt="Roma" />
								</a>
								<h3><a href="#">Roma <span>Italy</span></a></h3>
							</div>
						</div>
						<div class="right">
							<div class="right_1">
								<a href="#">
									<img class="large" src="{$base_dir_ssl}img/cms/barcelona.jpg" alt="Barcelona" />
									<img class="small" src="{$base_dir_ssl}img/cms/barcelona_small.jpg" alt="Barcelona" />
								<h3><a href="#">Barcelona <span>Spain</span></a></h3>
							</div>
							<div class="right_2">
								<a href="#"> 
									<img class="large" src="{$base_dir_ssl}img/cms/berlin.jpg" alt="Berlin" />
									<img class="small" src="{$base_dir_ssl}img/cms/berlin_small.jpg" alt="Berlin" />
								</a>
								<h3><a href="">Berlin <span>Germany</span></a></h3>
							</div>
						</div>
					</div>
					<div class="other_cities">
						<ul>
							<li><a>Bratislava</a></li>
							<li><a>Prague</a></li>
							<li><a>Budapest</a></li>
							<li><a>Munchen</a></li>
							<li><a>Monaco</a></li>
							<li><a>Tokyo</a></li>
							<li><a>New York</a></li>
							<li><a>Other destination</a></li>
						</ul>
					</div>
				</div>
			</div>
			{/if}

			{if isset($HOOK_FOOTER)}
				<!-- Footer -->
				<div class="footer-container">
					<footer id="footer"  class="container">
						<div class="row">{$HOOK_FOOTER}</div>
					</footer>
					<div id="copyright" class="container">
						<div class="row">
							<div class="copyright col-xs-7">
								&copy; {$smarty.now|date_format:'Y'} {$shop_name|escape:'html':'UTF-8'}
							</div>
							<div class="design col-xs-5">
								Design by <img src="{$img_dir}sdlogo.png" alt="Design by SuperDesigneri.cz" />
							</div>
						</div>
					</div>
				</div><!-- #footer -->
			{/if}
		</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
	</body>
</html>
