 $(document).ready(function() {
    $('#info-block .info-list li').on('click mouseover', function(e) {
        var item = '.' + this.className.replace(' active', '');
        $('#info-block .info-list li').removeClass('active');
        $(this).addClass('active');
        $('#info-block .description li').removeClass('active');
        $('#info-block .description ul').children(item).addClass('active');
    });
 });