$(function(){
    var cartBox = $('#cartBox');
    var mobileCartTrigger = $('#mobileCartTrigger');
    var mobileCart = $('#mobileCart');
    mobileCart.html(cartBox.html());

    mobileCartTrigger.click(function(){
        if($(this).css('position') == 'absolute'){
            mobileCart.toggle();
        }
    });
    $('.order-filter .btn-open-filter').click(function(){
        var form = $(this).parent().children('form');
        form.toggle();
    });
    $('.processFlights').on('click',function() {
        $("#flight_setpax_form").submit();
    });
});
setCartMobileWidth();
function setCartMobileWidth(){
    var box = $('#mobileCart');
    var boxW = 400;
    if(box.css('z-index') == '998'){
        boxW = $(window).width();
    }
    box.width(boxW);
}

$(window).resize(function(){
    setCartMobileWidth();
});
