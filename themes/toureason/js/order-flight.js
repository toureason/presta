var returnDateObj = null;

$(document).ready(function(){

    if($('#flight_search_btn').length > 0) {
        $('#flight_search_btn').html($('#flight_search_btn').attr('data-text-search'));
    }

    var dpParams = {
        dateFormat: 'dd.mm.yy',
        minDate: 0,
        firstDay: 1,
        changeYear: true,
        yearRange: "c:c+1"
    };
    $('#flight_search_form input[name="departure"]').datepicker(dpParams);
    $('#flight_search_form input[name="departure"]').datepicker('option', 'onClose', selectDate);
    $('#flight_search_form input[name="return_date"]').datepicker(dpParams);
    $('#flight_search_form input[name="return_date"]').datepicker('option', 'onClose', selectDateRet);

    dpParams = {
        dateFormat: 'dd.mm.yy',
        maxDate: 0,
        firstDay: 1,
        changeYear: true,
        yearRange: "c-13:c"
    };
    $('input.data-birth-CH').datepicker(dpParams);
    dpParams['yearRange'] = 'c-3:c';
    $('input.data-birth-INF').datepicker(dpParams);

    $.widget.bridge( "ui_autocomplete", $.ui.autocomplete );
    setAutocompleteLoc('from');
    setAutocompleteLoc('to');

});

function selectDate(date, obj) {
    if(date == '') $('#flight_search_form input[name="return_date"]').datepicker('option', 'minDate', 0);
    else $('#flight_search_form input[name="return_date"]').datepicker('option', 'minDate', new Date(obj.selectedYear, obj.selectedMonth, obj.selectedDay));
}

function selectDateRet(date, obj) {
    if(date == '') $('#flight_search_form input[name="departure"]').datepicker('option', 'maxDate', null);
    else $('#flight_search_form input[name="departure"]').datepicker('option', 'maxDate', new Date(obj.selectedYear, obj.selectedMonth, obj.selectedDay));
}

function setAutocompleteLoc(name) {
    $('#flight_search_form input[name="' + name + '_auto"]').ui_autocomplete({
        source: $('#flight_search_form').attr('action') + '&action=airportsload',
        minLength: 3,
        select: function(event, ui) {
            $('#flight_search_form input[name="' + name + '"]').val(ui.item.value);
            $('#flight_search_form input[name="' + name + '_auto"]').val(ui.item.label);
            return false;
        }
    });
}

function showFlightDetails(id) {
    $('#flight_details_' + id).show();
    $('#flight_details_btn_2_' + id).show();
    $('#flight_details_btn_' + id).hide();
    $('#row-bg-'+id).removeClass('border-bottom1');
    console.log(id)
}

function hideFlightDetails(id) {
    $('#flight_details_' + id).hide();
    $('#flight_details_btn_2_' + id).hide();
    $('#flight_details_btn_' + id).show();
    $('#row-bg-'+id).addClass('border-bottom1');
}

function setFlight(fgroup) {
    $('#flight_set input[name="fgroup"]').val(fgroup);
    $('#flight_set').submit();
}

function searchFlightsNew(sorting) {
    $('#flight_results_table').attr('data-next-index', 0);
    searchFlights(sorting, 1);
}

function searchFlightsSort(sorting) {
    $('#flight_results_table').attr('data-next-index', 0);
    searchFlights(sorting, 0);
}

var flightSortLast = 'price';
var flightSortLastDir = 'asc';

function searchFlights(sorting, reload) {
    if(! sorting) sorting = $('#flight_search_btn_next').attr('data-sorting');
    else $('#flight_search_btn_next').attr('data-sorting', sorting);
    if(! reload) reload = 0;
    if($('#loading_content').length > 0) {
        if (!!$.prototype.fancybox)
            $.fancybox.open([
            {
                type: 'inline',
                autoScale: true,
                minHeight: 30,
                content: $('#loading_content').html()
            }],
            {
                padding: 10
            });
    }
    var next = $('#flight_results_table').attr('data-next-index');
    if((flightSortLast == sorting) && (next == 0) && (reload == 0)) {
        if(flightSortLastDir == 'asc') {
            flightSortLastDir = 'desc';
        }
        else {
            flightSortLastDir = 'asc';
        }
    }
    flightSortLast = sorting;
    var url = $('#flight_search_form').attr('action') + '&action=flightsearch&reload=' + reload + '&sort=' + sorting + '&sortdir=' + flightSortLastDir + '&next=' + next;
    var params = {
        'adults': getInputVal('adults', 'select'),
        'children': getInputVal('children', 'select'),
        'infants': getInputVal('infants', 'select'),
        'from': getInputVal('from'),
        'to': getInputVal('to'),
        'from_auto': getInputVal('from_auto'),
        'to_auto': getInputVal('to_auto'),
        'departure': getInputVal('departure'),
        'da_time': getInputVal('da_time', 'select'),
        'pref_time': getInputVal('pref_time', 'select'),
        'direct': getInputVal('direct', 'check'),
        'return_date': getInputVal('return_date'),
        'return_da_time': getInputVal('return_da_time', 'select'),
        'return_pref_time': getInputVal('return_pref_time', 'select'),
        'class': getInputVal('class', 'select')
    };
    for (var key in params) {
        url += '&' + key + '=' + params[key];
    }
    if(next == 0) $('#flight_search_btn').html($('#flight_search_btn').attr('data-text-loading'));
    else $('#flight_search_btn_next').html($('#flight_search_btn_next').attr('data-text-loading'));
    $.get(url, function(data) {
        var flights = jQuery.parseJSON(data);
        if(flights['output'] != undefined) {
            if(next == 0) $('#flight_results_table').html(flights['output']);
            else $('#flight_results_table').append(flights['output']);
            if(flights['next'] && (flights['next'] != -1)) {
                $('#flight_results_table').attr('data-next-index', parseInt(flights['next']));
                $('#flight_search_btn_next').show();
                $('#flight_search_btn_next').html($('#flight_search_btn_next').attr('data-text-search'));
            }
            else $('#flight_search_btn_next').hide();
            if(flightSortLastDir == 'asc') {
                $('#sort-' + sorting).removeClass('down');
                $('#sort-' + sorting).addClass('up');
            }
            else {
                $('#sort-' + sorting).removeClass('up');
                $('#sort-' + sorting).addClass('down');
            }
        }
        else if(flights['error']) {
            var errText = flights['error'].join('\n');
            alert(errText);
        }
        if(next == 0) $('#flight_search_btn').html($('#flight_search_btn').attr('data-text-search'));
        if ($.prototype.fancybox) $.fancybox.close();
    });
}

function getInputVal(name, type) {
    if(! type) type = 'input';
    if(type == 'check') {
        if($('#flight_search_form input[name="'+name+'"]').is(':checked')) return 1;
        return 0;
    }
    return $('#flight_search_form ' + type + '[name="'+name+'"]').val();
}
