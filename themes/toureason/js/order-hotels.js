$(document).ready(function () {

    if($('#hotel_search_btn').length > 0) {
        $('#hotel_search_btn').html($('#hotel_search_btn').attr('data-text-search'));
    }

    var dpParams = {
        dateFormat: 'dd.mm.yy',
        minDate: 0,
        firstDay: 1
    };

    $('input[name="start_date"]').datepicker(dpParams);
    $('input[name="start_date"]').datepicker('option', 'onClose', selectDateStart);
    $('input[name="end_date"]').datepicker(dpParams);
    $('input[name="end_date"]').datepicker('option', 'onClose', selectDateEnd);

    $.widget.bridge( "ui_autocomplete", $.ui.autocomplete );
    setAutocompleteLoc('city');

    $('.data-hotel-set').click(function () {
        $('#hotel_set input[name="hotel_id"]').val($(this).attr('data-key'));
        $('#hotel_set').submit();
    });
});

// datepicker
function selectDateStart(date, obj) {
    if(date == '') $('input[name="end_date"]').datepicker('option', 'minDate', 0);
    else {
        var date = new Date(obj.selectedYear, obj.selectedMonth, obj.selectedDay);
        date.setTime(date.getTime() + 86400000);
        $('input[name="end_date"]').datepicker('option', 'minDate', date);
    }
}

function selectDateEnd(date, obj) {
    if(date == '') $('input[name="start_date"]').datepicker('option', 'maxDate', null);
    else {
        var date = new Date(obj.selectedYear, obj.selectedMonth, obj.selectedDay);
        date.setTime(date.getTime() - 86400000);
        $('input[name="start_date"]').datepicker('option', 'maxDate', date);
    }
}

// autocomplete
function setAutocompleteLoc(name) {
    $('#hotel_search_form input[name="' + name + '_auto"]').ui_autocomplete({
        source: $('#hotel_search_form').attr('action') + '&action=citiesload',
        minLength: 3,
        select: function(event, ui) {
            $('#hotel_search_form input[name="' + name + '"]').val(ui.item.value);
            $('#hotel_search_form input[name="' + name + '_auto"]').val(ui.item.label);
            return false;
        }
    });
}

// details show/hide
function showHotelDetails(id, hotelId) {
    $('#hotel_details_' + id).show();
    $('#hotel_details_btn_' + id).hide();
    $('#hotel_details_btn_2_' + id).show();
    $('#p-hotel-description_' + id).addClass('active');
}

function hideHotelDetails(id) {
    $('#hotel_details_' + id).hide();
    $('#hotel_details_btn_' + id).show();
    $('#hotel_details_btn_2_' + id).hide();
    $('#p-hotel-description_' + id).removeClass('active');
}

function checkRooms(hotel, roomRef) {
    var oneHotel = true;
    var roomSum = 0;
    $('.hotel_details select').each(function() {
        if(($(this).val() != 0) && ($(this).attr('data-hotel') != hotel)) {
            $(this).val(0);
            oneHotel = false;
        }
        roomSum = roomSum + parseInt($(this).val());
    });
    if(! oneHotel) alert($('#hotel_results_table').attr('data-deselect-msg'));
    if(roomSum > $('#count_rooms').val()) {
        alert($('#hotel_results_table').attr('data-count-msg'));
        $(roomRef).val(0);
    }
}

// searching
function searchHotelsNew(sorting) {
    $('#hotel_results_table').attr('data-next-index', 0);
    searchHotels(sorting, 1);
}

function searchHotelsSort(sorting) {
    $('#hotel_results_table').attr('data-next-index', 0);
    searchHotels(sorting, 0);
}

var hotelSortLast = 'price';
var hotelSortLastDir = 'asc';

function searchHotels(sorting, reload) {
    if(! sorting) sorting = $('#hotel_search_btn_next').attr('data-sorting');
    else $('#hotel_search_btn_next').attr('data-sorting', sorting);
    if(! reload) reload = 0;
    if($('#loading_content').length > 0) {
        if (!!$.prototype.fancybox)
            $.fancybox.open([
            {
                type: 'inline',
                autoScale: true,
                minHeight: 30,
                content: $('#loading_content').html()
            }],
            {
                padding: 10
            });
    }
    var next = $('#hotel_results_table').attr('data-next-index');
    if((hotelSortLast == sorting) && (next == 0) && (reload == 0)) {
        if(hotelSortLastDir == 'asc') {
            hotelSortLastDir = 'desc';
        }
        else {
            hotelSortLastDir = 'asc';
        }
    }
    hotelSortLast = sorting;
    var url = $('#hotel_search_form').attr('action') + '&action=hotelsearch&reload=' + reload + '&sort=' + sorting + '&sortdir=' + hotelSortLastDir + '&next=' + next;
    var params = {
        'adults': getInputVal('adults', 'select'),
        'count_rooms': getInputVal('count_rooms', 'select'),
        'start_date': getInputVal('start_date'),
        'end_date': getInputVal('end_date'),
        'city_auto': getInputVal('city_auto'),
        'city': getInputVal('city')
    };
    for (var key in params) {
        url += '&' + key + '=' + params[key];
    }
    if(next == 0) $('#hotel_search_btn').html($('#hotel_search_btn').attr('data-text-loading'));
    else $('#hotel_search_btn_next').html($('#hotel_search_btn_next').attr('data-text-loading'));
    $.get(url, function(data) {
        var hotels = jQuery.parseJSON(data);
        if(hotels['output'] != undefined) {
            if(next == 0) $('#hotel_results_table').html(hotels['output']);
            else $('#hotel_results_table').append(hotels['output']);
            if(hotels['next'] && (hotels['next'] != -1)) {
                $('#hotel_results_table').attr('data-next-index', parseInt(hotels['next']));
                $('#hotel_search_btn_next').show();
                $('#hotel_search_btn_next').html($('#hotel_search_btn_next').attr('data-text-search'));
            }
            else $('#hotel_search_btn_next').hide();
            if(hotelSortLastDir == 'asc') {
                $('#sort-' + sorting).removeClass('down');
                $('#sort-' + sorting).addClass('up');
            }
            else {
                $('#sort-' + sorting).removeClass('up');
                $('#sort-' + sorting).addClass('down');
            }
        }
        else if(hotels['error']) {
            var errText = hotels['error'].join('\n');
            alert(errText);
        }
        if(next == 0) $('#hotel_search_btn').html($('#hotel_search_btn').attr('data-text-search'));
        if ($.prototype.fancybox) $.fancybox.close();
        // show/hide btn Select rooms
        if($('#hotel_results_table tr').length > 0) $('#hotel_select_rooms_btn').show();
        else $('#hotel_select_rooms_btn').hide();
    });
}

function getInputVal(name, type) {
    if(! type) type = 'input';
    if(type == 'check') {
        if($('#hotel_search_form input[name="'+name+'"]').is(':checked')) return 1;
        return 0;
    }
    return $('#hotel_search_form ' + type + '[name="'+name+'"]').val();
}
