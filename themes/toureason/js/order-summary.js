$(document).ready(function () {

	if($('#summary-form').attr('data-validate-onload') == 1) {
        validateSummaryForm();
    }

});

function validateSummaryForm() {
    $('input.validate, textarea.validate').each(function() {
        validate_field(this);
    });
}

function summarySubmit() {
    if(! $('#cgv').is(':checked')) {
        alert($('#cgv').attr('data-text-required'));
        return;
    }
    validateSummaryForm();
    if($('.form-error').length == 0) $('#summary-form').submit();
    else console.log('form errors');
}
