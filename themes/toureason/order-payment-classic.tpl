{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $HOOK_PAYMENT}
    <div class="row row-sm">
        <div class="col-md-12">
            <div class="order-summary-content">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="order-summary-content-name">
                            {l s='Event'}
                        </div>
                    </div>
                    <div class="col-lg-10">

                        {foreach from=$products item=product name=productLoop}
                        <div class="row row-sm">
                            <div class="col-xs-8 col-sm-10">
                                <div class="name">{$product.name|escape:'htmlall':'UTF-8'}</div>
                                <!--div class="moreInfo">
                                    1.3.2016 | 19:00 | Madrid vs. Parsis (in Madrid)
                                </div -->
                                <div class="moreInfo">
                                    <!-- Category 5 - -->{$product.cart_quantity}&nbsp;{if $product.cart_quantity == 1}{l s='ticket'}{else}{l s='tickets'}{/if}
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-2 text-right">
                                <div class="price">{displayPrice price=$product.total_wt}</div>
                            </div>
                        </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="order-summary-content order-summary-content-flight">
                <div class="row">
                    <div class="col-lg-2">
                    <div class="row row-sm">
                    <div class="col-xs-8 col-sm-12">
                     <div class="order-summary-content-name">
                            {l s='Flight'}
                        </div>
</div>
<div class="col-xs-4 hidden-sm hidden-md hidden-lg text-right">
                    {if $flight_price}
                                <div class="price">{if $flight_price}{displayPrice price=$flight_price}{else}{l s='No flight selected'}{/if}</div>
                                 {else}
                                   <div class="price">{displayPrice price=0}</div>
                                  {/if}
                            </div>
</div>

                    </div>
                    <div class="col-lg-10">
                        <div class="row-sm">
                            {if $flight_price}
                            <div class="col-xs-12 col-sm-10">
                            {include file='./_flights_inc_selflight.tpl'}
                            </div>
                            <div class="col-xs-4 col-sm-2 text-right hidden-xs">
                                <div class="price">{if $flight_price}{displayPrice price=$flight_price}{else}{l s='No flight selected'}{/if}</div>
                            </div>
                            {else}
                            <div class="col-xs-12 col-sm-10">
                                {l s='No flight selected'}
                            </div>
                            <div class="col-xs-4 col-sm-2 text-right hidden-xs">
                                <div class="price">{displayPrice price=0}</div>
                            </div>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="order-summary-content">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="order-summary-content-name">
                            {l s='Hotel'}
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="row-sm">
                            {if $hotel_price}
                            <div class="col-xs-8 col-sm-10">
                                {include file='./_hotels_inc_selection.tpl'}
                            </div>
                            <div class="col-xs-4 col-sm-2 text-right">
                                <div class="price">{displayPrice price=$hotel_price}</div>
                            </div>
                            {else}
                            <div class="col-xs-8 col-sm-10">
                                {l s='No hotel selected'}
                            </div>
                            <div class="col-xs-4 col-sm-2 text-right">
                                <div class="price">{displayPrice price=0}</div>
                            </div>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="order-summary-content">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="order-summary-content-name">
                            {l s='Customer'}
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <form id="summary-form" data-validate-onload="{$address_validate}" class="summary-form"
                              action="{$link->getPageLink('order', true, NULL, "&step=103")|escape:'html':'UTF-8'}" method="post">
                            <input type="hidden" name="alias" value="delcart" />

                            {*<h3 class="page-smd-9 col-md{l s='Delivery address'}</h3>*}
                            <div class="row row-sm">
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="required form-group">
                                        <label class="hidden-md hidden-lg" for="email">{l s='E-mail'} <sup>*</sup></label>
                                        <input placeholder="{l s='E-mail'} *" class="is_required validate form-control" data-validate="isEmail" type="email" id="email" name="email" value="{if isset($smarty.post.email)}{$smarty.post.email}{else}{if isset($customer->email)}{$customer->email|escape:'html':'UTF-8'}{/if}{/if}" maxlength="128" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="required form-group">
                                        <label class="hidden-md hidden-lg" for="firstname">{l s='First name'} <sup>*</sup></label>
                                        <input placeholder="{l s='First name'} *" class="is_required validate form-control" data-validate="isName" type="text" name="firstname" id="firstname" value="{if isset($smarty.post.firstname)}{$smarty.post.firstname}{else}{if isset($address->firstname)}{$address->firstname|escape:'html':'UTF-8'}{/if}{/if}" maxlength="32" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="required form-group">
                                        <label class="hidden-md hidden-lg" for="lastname">{l s='Last name'} <sup>*</sup></label>
                                        <input placeholder="{l s='Last name'} *" class="is_required validate form-control" data-validate="isName" type="text" id="lastname" name="lastname" value="{if isset($smarty.post.lastname)}{$smarty.post.lastname}{else}{if isset($address->lastname)}{$address->lastname|escape:'html':'UTF-8'}{/if}{/if}" maxlength="32" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="required form-group">
                                        <label class="hidden-md hidden-lg" for="address1">{l s='Address'} <sup>*</sup></label>
                                        <input placeholder="{l s='Address'} *" class="is_required validate form-control" data-validate="isAddress" type="text" id="address1" name="address1" value="{if isset($smarty.post.address1)}{$smarty.post.address1}{else}{if isset($address->address1)}{$address->address1|escape:'html':'UTF-8'}{/if}{/if}" maxlength="128" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="required postcode form-group">
                                        <label class="hidden-md hidden-lg" for="postcode">{l s='Zip/Postal Code'} <sup>*</sup></label>
                                        <input placeholder="{l s='Zip/Postal Code'} *" class="is_required validate form-control" data-validate="isAddress" type="text" id="postcode" name="postcode" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{else}{if isset($address->postcode)}{$address->postcode|escape:'html':'UTF-8'}{/if}{/if}" maxlength="12" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="required form-group">
                                        <label class="hidden-md hidden-lg" for="city">{l s='City'} <sup>*</sup></label>
                                        <input placeholder="{l s='City'} *" class="is_required validate form-control" data-validate="isCityName" type="text" name="city" id="city" value="{if isset($smarty.post.city)}{$smarty.post.city}{else}{if isset($address->city)}{$address->city|escape:'html':'UTF-8'}{/if}{/if}" maxlength="64" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="required form-group">
                                        <label class="hidden-md hidden-lg" for="id_country">{l s='Country'} <sup>*</sup></label>
                                        <select data-sel="{$sl_country}" oncanplay="{l s='Country'} *" id="id_country" class="form-control" name="id_country">{$countries_list}</select>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="form-group">
                                        <label class="hidden-md hidden-lg" for="phone_mobile">{l s='Mobile phone'} <sup>*</sup></label>
                                        <input placeholder="{l s='Mobile phone'} *" class="is_required validate  form-control" data-validate="isPhoneNumber" type="tel" id="phone_mobile" name="phone_mobile" value="{if isset($smarty.post.phone_mobile)}{$smarty.post.phone_mobile}{else}{if isset($address->phone_mobile)}{$address->phone_mobile|escape:'html':'UTF-8'}{/if}{/if}" maxlength="32" />
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">

                                <div class="col-md-12">
                                    <div id="ordermsg" class="form-group">
                                        <label for="message">{l s='If you would like to add a comment about your order, please write it in the field below.'}</label>
                                        <textarea class="form-control form-control-msg" cols="60" rows="6" name="message">{if isset($oldMessage)}{$oldMessage}{/if}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-sm">
                                <div class="col-md-12">
                                    <p class="checkbox">
                                        <input type="checkbox" name="cgv" id="cgv" value="1" {if $cgv}checked="checked"{/if} data-text-required="{l s='Agreement with Terms of Service is required'}" />
                                        <label for="cgv">{l s='I agree to the terms of service and will adhere to them unconditionally.'}</label>
                                        <a href="{$link_conditions|escape:'html':'UTF-8'}" class="iframe" rel="nofollow">{l s='(Read the Terms of Service)'}</a>
                                    </p>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="order-summary-content order-summary-content-price">
                <div class="row">
                    <div class="col-xs-6 col-lg-2">
                        <div class="order-summary-content-name">
                            {l s='Payment'}
                        </div>
                    </div>
                    <div class="hidden-xs hidden-sm hidden-md col-lg-5">
                         <button class="btn btn-info btn-arrow btn-buy" onclick="summarySubmit();">
                            {l s='Buy now'}
                        </button>
                    </div>
                    <div class="col-xs-6 col-lg-5">
                        <div class="price-2"> {l s='Total'}: <em><strong>{displayPrice price=$orderTotal}</strong></em></div>
                    </div>
                     </div>
                </div>
                <div class="row hidden-lg">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-arrow btn-buy" onclick="summarySubmit();">
                            {l s='Buy now'}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {if !$opc}{/if}
    {if $opc}{/if}
{else}
    <!--p class="alert alert-warning">{l s='No payment modules have been installed.'}</p-->
{/if}
{if false}
<div class="paiement_block">
    <div id="HOOK_TOP_PAYMENT">{$HOOK_TOP_PAYMENT}</div>
    {if $HOOK_PAYMENT}
        {if !$opc}
            <div id="order-detail-content" class="table_block table-responsive">
                <table id="cart_summary" class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="cart_product first_item">{l s='Product'}</th>
                        <th class="cart_description item">{l s='Description'}</th>
                        {if $PS_STOCK_MANAGEMENT}
                            <th class="cart_availability item text-center">{l s='Availability'}</th>
                        {/if}
                        <th class="cart_unit item text-right">{l s='Unit price'}</th>
                        <th class="cart_quantity item text-center">{l s='Qty'}</th>
                        <th class="cart_total last_item text-right">{l s='Total'}</th>
                    </tr>
                    </thead>
                    <tfoot>
                    {if $use_taxes}
                        {if $priceDisplay}
                            <tr class="cart_total_price">
                                <td colspan="4" class="text-right">{if $display_tax_label}{l s='Total products (tax excl.)'}{else}{l s='Total products'}{/if}</td>
                                <td colspan="2" class="price" id="total_product">{displayPrice price=$total_products}</td>
                            </tr>
                        {else}
                            <tr class="cart_total_price">
                                <td colspan="4" class="text-right">{if $display_tax_label}{l s='Total products (tax incl.)'}{else}{l s='Total products'}{/if}</td>
                                <td colspan="2" class="price" id="total_product">{displayPrice price=$total_products_wt}</td>
                            </tr>
                        {/if}
                    {else}
                        <tr class="cart_total_price">
                            <td colspan="4" class="text-right">{l s='Total products'}</td>
                            <td colspan="2" class="price" id="total_product">{displayPrice price=$total_products}</td>
                        </tr>
                    {/if}
                    <tr class="cart_total_voucher" {if $total_wrapping == 0}style="display:none"{/if}>
                        <td colspan="4" class="text-right">
                            {if $use_taxes}
                                {if $priceDisplay}
                                    {if $display_tax_label}{l s='Total gift wrapping (tax excl.):'}{else}{l s='Total gift wrapping cost:'}{/if}
                                {else}
                                    {if $display_tax_label}{l s='Total gift wrapping (tax incl.)'}{else}{l s='Total gift wrapping cost:'}{/if}
                                {/if}
                            {else}
                                {l s='Total gift wrapping cost:'}
                            {/if}
                        </td>
                        <td colspan="2" class="price-discount price" id="total_wrapping">
                            {if $use_taxes}
                                {if $priceDisplay}
                                    {displayPrice price=$total_wrapping_tax_exc}
                                {else}
                                    {displayPrice price=$total_wrapping}
                                {/if}
                            {else}
                                {displayPrice price=$total_wrapping_tax_exc}
                            {/if}
                        </td>
                    </tr>
                    <tr class="cart_total_flight">
                        <td colspan="4" class="text-right">
                            {l s='Flight'}<br />
                            {include file='./_flights_inc_selflight.tpl'}
                        </td>
                        <td colspan="2" class="price" id="total_flight">{if $flight_price}{displayPrice price=$flight_price}{else}{l s='No flight selected'}{/if}</td>
                    </tr>
                    <tr class="cart_total_hotel">
                        <td colspan="4" class="text-right">{l s='Hotel'}</td>
                        <td colspan="2" class="price" id="total_hotel">{if $hotel_price}{displayPrice price=$hotel_price}{else}{l s='No hotel selected'}{/if}</td>
                    </tr>

                    {if $total_shipping_tax_exc <= 0 && (!isset($isVirtualCart) || !$isVirtualCart) && $free_ship}
                        <tr class="cart_total_delivery">
                            <td colspan="4" class="text-right">{l s='Total shipping'}</td>
                            <td colspan="2" class="price" id="total_shipping">{l s='Free Shipping!'}</td>
                        </tr>
                    {else}
                        {if $use_taxes && $total_shipping_tax_exc != $total_shipping}
                            {if $priceDisplay}
                                <tr class="cart_total_delivery" {if $shippingCost <= 0} style="display:none"{/if}>
                                    <td colspan="4" class="text-right">{if $display_tax_label}{l s='Total shipping (tax excl.)'}{else}{l s='Total shipping'}{/if}</td>
                                    <td colspan="2" class="price" id="total_shipping">{displayPrice price=$shippingCostTaxExc}</td>
                                </tr>
                            {else}
                                <tr class="cart_total_delivery"{if $shippingCost <= 0} style="display:none"{/if}>
                                    <td colspan="4" class="text-right">{if $display_tax_label}{l s='Total shipping (tax incl.)'}{else}{l s='Total shipping'}{/if}</td>
                                    <td colspan="2" class="price" id="total_shipping" >{displayPrice price=$shippingCost}</td>
                                </tr>
                            {/if}
                        {else}
                            <tr class="cart_total_delivery"{if $shippingCost <= 0} style="display:none"{/if}>
                                <td colspan="4" class="text-right">{l s='Total shipping'}</td>
                                <td colspan="2" class="price" id="total_shipping" >{displayPrice price=$shippingCostTaxExc}</td>
                            </tr>
                        {/if}
                    {/if}
                    <tr class="cart_total_voucher" {if $total_discounts == 0}style="display:none"{/if}>
                        <td colspan="4" class="text-right">
                            {if $use_taxes}
                                {if $priceDisplay}
                                    {if $display_tax_label && $show_taxes}{l s='Total vouchers (tax excl.)'}{else}{l s='Total vouchers'}{/if}
                                {else}
                                    {if $display_tax_label && $show_taxes}{l s='Total vouchers (tax incl.)'}{else}{l s='Total vouchers'}{/if}
                                {/if}
                            {else}
                                {l s='Total vouchers'}
                            {/if}
                        </td>
                        <td colspan="2" class="price-discount price" id="total_discount">
                            {if $use_taxes}
                                {if $priceDisplay}
                                    {displayPrice price=$total_discounts_tax_exc*-1}
                                {else}
                                    {displayPrice price=$total_discounts*-1}
                                {/if}
                            {else}
                                {displayPrice price=$total_discounts_tax_exc*-1}
                            {/if}
                        </td>
                    </tr>
                    {if $use_taxes}
                        {if $total_tax != 0 && $show_taxes}
                            {if $priceDisplay != 0}
                                <tr class="cart_total_price">
                                    <td colspan="4" class="text-right">{if $display_tax_label}{l s='Total (tax excl.)'}{else}{l s='Total'}{/if}</td>
                                    <td colspan="2" class="price" id="total_price_without_tax">{displayPrice price=$total_price_without_tax}</td>
                                </tr>
                            {/if}
                            <tr class="cart_total_tax">
                                <td colspan="4" class="text-right">{l s='Tax'}</td>
                                <td colspan="2" class="price" id="total_tax" >{displayPrice price=$total_tax}</td>
                            </tr>
                        {/if}
                        <tr class="cart_total_price">
                            <td colspan="4" class="total_price_container text-right"><span>{l s='Total'}</span></td>
                            <td colspan="2" class="price" id="total_price_container">
                                <span id="total_price" data-selenium-total-price="{$total_price}">{displayPrice price=$total_price+$flight_price+$hotel_price}</span>
                            </td>
                        </tr>
                    {else}
                        <tr class="cart_total_price">
                            {if $voucherAllowed}
                                <td colspan="2" id="cart_voucher" class="cart_voucher">
                                    <div id="cart_voucher" class="table_block">
                                        {if $voucherAllowed}
                                            <form action="{if $opc}{$link->getPageLink('order-opc', true)}{else}{$link->getPageLink('order', true)}{/if}" method="post" id="voucher">
                                                <fieldset>
                                                    <h4>{l s='Vouchers'}</h4>
                                                    <input type="text" id="discount_name" class="form-control" name="discount_name" value="{if isset($discount_name) && $discount_name}{$discount_name}{/if}" />
                                                    <input type="hidden" name="submitDiscount" />
                                                    <button type="submit" name="submitAddDiscount" class="button btn btn-default button-small"><span>{l s='ok'}</span></button>
                                                    {if $displayVouchers}
                                                        <p id="title" class="title_offers">{l s='Take advantage of our offers:'}</p>
                                                        <div id="display_cart_vouchers">
                                                            {foreach from=$displayVouchers item=voucher}
                                                                <span onclick="$('#discount_name').val('{$voucher.name}');return false;" class="voucher_name">{$voucher.name}</span> - {$voucher.description} <br />
                                                            {/foreach}
                                                        </div>
                                                    {/if}
                                                </fieldset>
                                            </form>
                                        {/if}
                                    </div>
                                </td>
                            {/if}
                            <td colspan="{if !$voucherAllowed}4{else}2{/if}" class="text-right total_price_container">
                                <span>{l s='Total'}</span>
                            </td>
                            <td colspan="2" class="price total_price_container" id="total_price_container">
                                <span id="total_price" data-selenium-total-price="{$total_price_without_tax}">{displayPrice price=$total_price_without_tax}</span>
                            </td>
                        </tr>
                    {/if}
                    </tfoot>

                    <tbody>
                    {foreach from=$products item=product name=productLoop}
                        {assign var='productId' value=$product.id_product}
                        {assign var='productAttributeId' value=$product.id_product_attribute}
                        {assign var='quantityDisplayed' value=0}
                        {assign var='cannotModify' value=1}
                        {assign var='odd' value=$product@iteration%2}
                        {assign var='noDeleteButton' value=1}

                        {* Display the product line *}
                        {include file="$tpl_dir./shopping-cart-product-line.tpl"}

                        {* Then the customized datas ones*}
                        {if isset($customizedDatas.$productId.$productAttributeId)}
                            {foreach from=$customizedDatas.$productId.$productAttributeId[$product.id_address_delivery] key='id_customization' item='customization'}
                                <tr id="product_{$product.id_product}_{$product.id_product_attribute}_{$id_customization}" class="alternate_item cart_item">
                                    <td colspan="4">
                                        {foreach from=$customization.datas key='type' item='datas'}
                                            {if $type == $CUSTOMIZE_FILE}
                                                <div class="customizationUploaded">
                                                    <ul class="customizationUploaded">
                                                        {foreach from=$datas item='picture'}
                                                            <li>
                                                                <img src="{$pic_dir}{$picture.value}_small" alt="" class="customizationUploaded" />
                                                            </li>
                                                        {/foreach}
                                                    </ul>
                                                </div>
                                            {elseif $type == $CUSTOMIZE_TEXTFIELD}
                                                <ul class="typedText">
                                                    {foreach from=$datas item='textField' name='typedText'}
                                                        <li>
                                                            {if $textField.name}
                                                                {l s='%s:' sprintf=$textField.name}
                                                            {else}
                                                                {l s='Text #%s:' sprintf=$smarty.foreach.typedText.index+1}
                                                            {/if}
                                                            {$textField.value}
                                                        </li>
                                                    {/foreach}
                                                </ul>
                                            {/if}
                                        {/foreach}
                                    </td>
                                    <td class="cart_quantity text-center">
                                        {$customization.quantity}
                                    </td>
                                    <td class="cart_total"></td>
                                </tr>
                                {assign var='quantityDisplayed' value=$quantityDisplayed+$customization.quantity}
                            {/foreach}
                            {* If it exists also some uncustomized products *}
                            {if $product.quantity-$quantityDisplayed > 0}{include file="$tpl_dir./shopping-cart-product-line.tpl"}{/if}
                        {/if}
                    {/foreach}
                    {assign var='last_was_odd' value=$product@iteration%2}
                    {foreach $gift_products as $product}
                        {assign var='productId' value=$product.id_product}
                        {assign var='productAttributeId' value=$product.id_product_attribute}
                        {assign var='quantityDisplayed' value=0}
                        {assign var='odd' value=($product@iteration+$last_was_odd)%2}
                        {assign var='ignoreProductLast' value=isset($customizedDatas.$productId.$productAttributeId)}
                        {assign var='cannotModify' value=1}
                        {* Display the gift product line *}
                        {include file="./shopping-cart-product-line.tpl" productLast=$product@last productFirst=$product@first}
                    {/foreach}
                    </tbody>

                    {if count($discounts)}
                        <tbody>
                        {foreach from=$discounts item=discount name=discountLoop}
                            {if (float)$discount.value_real == 0}
                                {continue}
                            {/if}
                            <tr class="cart_discount {if $smarty.foreach.discountLoop.last}last_item{elseif $smarty.foreach.discountLoop.first}first_item{else}item{/if}" id="cart_discount_{$discount.id_discount}">
                                <td class="cart_discount_name" colspan="{if $PS_STOCK_MANAGEMENT}3{else}2{/if}">{$discount.name}</td>
                                <td class="cart_discount_price">
                                                    <span class="price-discount">
                                                        {if $discount.value_real > 0}
                                                            {if !$priceDisplay}
                                                                {displayPrice price=$discount.value_real*-1}
                                                            {else}
                                                                {displayPrice price=$discount.value_tax_exc*-1}
                                                            {/if}
                                                        {/if}
                                                    </span>
                                </td>
                                <td class="cart_discount_delete">1</td>
                                <td class="cart_discount_price">
                                                    <span class="price-discount">
                                                        {if $discount.value_real > 0}
                                                            {if !$priceDisplay}
                                                                {displayPrice price=$discount.value_real*-1}
                                                            {else}
                                                                {displayPrice price=$discount.value_tax_exc*-1}
                                                            {/if}
                                                        {/if}
                                                    </span>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    {/if}
                </table>
            </div> <!-- end order-detail-content -->
        {/if}
        {if $opc}
            <div id="opc_payment_methods-content">
        {/if}
        <!--div id="HOOK_PAYMENT">
            {$HOOK_PAYMENT}
        </div -->
        {if $opc}
            </div> <!-- end opc_payment_methods-content -->
        {/if}
    {else}
        <p class="alert alert-warning">{l s='No payment modules have been installed.'}</p>
    {/if}
    {if !$opc}
    <!--p class="cart_navigation clearfix">
        <a href="{$link->getPageLink('order', true, NULL, "step=2")|escape:'html':'UTF-8'}" title="{l s='Previous'}" class="button-exclusive btn btn-default">
            <i class="icon-chevron-left"></i>
            {l s='Continue shopping'}
        </a>
    </p-->
    {else}
</div> <!-- end opc_payment_methods -->
{/if}
</div> <!-- end HOOK_TOP_PAYMENT -->
{/if}
