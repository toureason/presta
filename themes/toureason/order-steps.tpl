{* Assign a value to 'current_step' to display current style *}
{capture name="url_back"}
    {if isset($back) && $back}back={$back}{/if}
{/capture}

{if !isset($multi_shipping)}
    {assign var='multi_shipping' value='0'}
{/if}

{if !$opc && ((!isset($back) || empty($back)) || (isset($back) && preg_match("/[&?]step=/", $back)))}
    <!-- Steps -->
    <div class="order-steps {if $current_step=='payment'}hidden-mobile{/if}">
        <ul>

            <li class="first step_done">
                <div class="order-steps-icon">Design</div>
                <div class="order-steps-text">your package</div>
            </li>

            {*<li class="{if $current_step=='summary'}step_current {elseif $current_step=='login'}step_done_last step_done{else}{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address' || $current_step=='login' || $current_step=='hotels' || $current_step=='flights'}step_done{else}step_todo{/if}{/if}">*}
                {*{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address' || $current_step=='login' || $current_step=='hotels' || $current_step=='flights'}*}
                    {*<a href="{$link->getPageLink('order', true)}">*}
                        {*<div class="order-steps-icon"></div>*}
                        {*<div class="order-steps-text">*}
                            {*1. {l s='Summary'}*}
                        {*</div>*}
                    {*</a>*}
                {*{else}*}
                    {*<div>*}
                        {*<div class="order-steps-icon"></div>*}
                        {*<div class="order-steps-text">*}
                            {*1. {l s='Summary'}*}
                        {*</div>*}
                    {*</div>*}
                {*{/if}*}

            {*</li>*}


            {*<li class="{if $current_step=='login'}step_current{elseif $current_step=='flights'}step_done step_done_last{else}{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address' || $current_step=='hotels'}step_done{else}step_todo{/if}{/if} second">*}

                {*{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address' || $current_step=='hotels' || $current_step=='flights'}*}
                    {*<a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=101{if $multi_shipping}&multi-shipping={$multi_shipping}{/if}")|escape:'html':'UTF-8'}">*}
                        {*<div class="order-steps-icon"></div>*}
                        {*<div class="order-steps-text">*}

                            {*2. {l s='Sign in'}*}
                        {*</div>*}
                    {*</a>*}
                {*{else}*}
                    {*<div>*}
                        {*<div class="order-steps-icon"></div>*}
                        {*<div class="order-steps-text">*}
                            {*2. {l s='Sign in'}*}
                        {*</div>*}
                    {*</div>*}
                {*{/if}*}

            {*</li>*}

            <li class="{if $current_step=='summary'}step_current{elseif $current_step=='flights'}step_done {elseif $current_step=='hotels'}step_done step_done_last{else}{if $current_step=='payment' || $current_step=='shipping' || $current_step=='address' }step_done{else}step_todo{/if}{/if} second">

                {if $current_step=='payment' || $current_step=='shipping' || $current_step=='address' || $current_step=='hotels' || $current_step=='flights'}
                    <a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}{if $multi_shipping}&multi-shipping={$multi_shipping}{/if}")|escape:'html':'UTF-8'}">
                        <div class="order-steps-icon t-icon t-icon-ticket"></div>
                        <div class="order-steps-text">

                            1. {l s='Event tickets'}
                        </div>
                    </a>
                {else}
                    <div>
                        <div class="order-steps-icon t-icon t-icon-ticket"></div>
                        <div class="order-steps-text">
                            1. {l s='Event tickets'}
                        </div>
                    </div>
                {/if}

            </li>


            <li class="{if $current_step=='flights'}step_current{elseif $current_step=='hotels'}step_done step_done_last{else}{if $current_step=='payment' || $current_step=='shipping'}step_done{else}step_todo{/if}{/if} second">

                {if $current_step=='payment' || $current_step=='shipping' || $current_step=='address' || $current_step=='hotels'}
                    <a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=101{if $multi_shipping}&multi-shipping={$multi_shipping}{/if}")|escape:'html':'UTF-8'}">
                        <div class="order-steps-icon t-icon t-icon-plane"></div>
                        <div class="order-steps-text">
                            2. {l s='Flight Tickets'}
                        </div>
                    </a>
                {else}
                    <div>
                        <div class="order-steps-icon t-icon t-icon-plane"></div>
                        <div class="order-steps-text">
                            2. {l s='Flight Tickets'}
                        </div>
                    </div>
                {/if}

            </li>


            <!--li class="{if $current_step=='address'}step_current{elseif $current_step=='shipping'}step_done step_done_last{else}{if $current_step=='payment'}step_done{else}step_todo{/if}{/if} third">
            {if $current_step=='payment' || $current_step=='shipping'}
                <a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=1{if $multi_shipping}&multi-shipping={$multi_shipping}{/if}")|escape:'html':'UTF-8'}">
                    05. {l s='Address'}
                </a>
            {else}
                <div>05. {l s='Address'}</div>
            {/if}
        </li>


        <li class="{if $current_step=='shipping'}step_current{else}{if $current_step=='payment'}step_done step_done_last{else}step_todo{/if}{/if} four">
            {if $current_step=='payment'}
                <a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=2{if $multi_shipping}&multi-shipping={$multi_shipping}{/if}")|escape:'html':'UTF-8'}">
                    06. {l s='Shipping'}
                </a>
            {else}
                <div>06. {l s='Shipping'}</div>
            {/if}
        </li-->


            <li class="{if $current_step=='hotels'}step_current{elseif $current_step=='address'}step_done step_done_last{else}{if $current_step=='payment' || $current_step=='shipping'}step_done{else}step_todo{/if}{/if}">
                {if $current_step=='payment'}
                <a href="{$link->getPageLink('order', true, NULL, "{$smarty.capture.url_back}&step=102{if $multi_shipping}&multi-shipping={$multi_shipping}{/if}")|escape:'html':'UTF-8'}">
                    <div>
                        <div class="order-steps-icon t-icon t-icon-hotel"></div>
                        <div class="order-steps-text">
                            3. {l s='Hotels'}
                        </div>
                    </div>
                </a>
                {else}
                    <div>
                        <div class="order-steps-icon t-icon t-icon-hotel"></div>
                        <div class="order-steps-text">
                            3. {l s='Hotels'}
                        </div>
                    </div>
                {/if}

            </li>

            <li class="last {if $current_step=='payment'}step_current{else}step_todo{/if}">

                <div>
                    <div class="order-steps-text">
                        {l s="DONE!"}
                    </div>
                </div>

            </li>
        </ul>

        {if $orderTotal}
        <div class="mobile-cart" id="mobileCartTrigger">
            <div class="cart-box-icon"></div>
            <div class="cart-box-price">{l s='Total'}:<span>{displayPrice price=$orderTotal}</span>
            </div>
            <div class="mobileCart cart-box" id="mobileCart"></div>
        </div>
        {/if}

    </div>
    <!-- /Steps -->
{/if}
